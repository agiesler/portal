Helmholtz Cloud Agent Services
==============================

This service runs in the background and handles the communication with the Helmholtz Cloud Agent (HCA). It picks up requests created by the backend server and sends them to the configured RabbitMQ queue. It also receives the responses from the HCA and updates the requests accordingly in the database.

### Configuration

This service needs a couple of configuration paramaters in order to run:

* `hca.service`: Name of the service this instance should work on
* `spring.data.mongodb.database`: The hostname of the MongoDB server
* `spring.rabbitmq.host`: The hostname of the RabbitMQ server
* `spring.rabbitmq.username`: Username to authenticate to RabbitMQ
* `spring.rabbitmq.password`: Password to authenticate to RabbitMQ
* `spring.rabbitmq.virtual-host`: Virtual host used for this service

Optional parameters:

* `hca.sender.queue`: Name of the queue to send messages to (default: `to_hca`)
* `hca.sender.delay`: Delay between checks for new requests in ms (default: `5000`)
* `hca.receiver.queue`: Name of the queue to receive messages from (default: `to_portal`)