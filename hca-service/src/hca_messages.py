from typing import List, Union
from dataclasses import dataclass


@dataclass
class QuotaV1:
    unit: str = None
    value: int = None


@dataclass
class GroupStorageResourceSpecV1:
    desiredName: str = None
    quota: QuotaV1 = None


@dataclass
class ComputeResourceSpecV1:
    cpu: QuotaV1 = None
    ram: QuotaV1 = None
    storage: QuotaV1 = None


@dataclass
class UserSpecV1:
    userId: str = None


@dataclass
class VOListSpecV1:
    vos: List[str] = None


@dataclass
class TargetEntityV1:
    type: str = None
    specification: Union[UserSpecV1, VOListSpecV1] = None


@dataclass
class ResourceDeallocateV1:
    id: str = None


@dataclass
class ResourceAllocateV1:
    type: str = None
    specification: Union[GroupStorageResourceSpecV1, ComputeResourceSpecV1] = None
    targetEntity: TargetEntityV1 = None


@dataclass
class ResourceUpdateV1:
    id: str = None
    type: str = None
    specification: Union[GroupStorageResourceSpecV1, ComputeResourceSpecV1] = None
    targetEntity: TargetEntityV1 = None


@dataclass
class ResourceCreatedV1:
    __handler__ = 'handle_resource_created'
    id: str = None


@dataclass
class ResourceUpdatedV1:
    __handler__ = 'handle_resource_updated'
    id: str = None


@dataclass
class ResourceDeallocatedV1:
    __handler__ = 'handle_resource_deallocated'
    id: str = None


@dataclass
class ErrorV1:
    __handler__ = 'handle_error'
    type: str = None
    message: str = None


@dataclass
class GenericResponseV1:
    __handler__ = 'handle_generic_response'
    successful: str = None
    message: str = None


@dataclass
class PingV1:
    message: str = None