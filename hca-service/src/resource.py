from typing import List, Union
from dataclasses import dataclass
from hca_messages import ResourceAllocateV1, ResourceUpdateV1, TargetEntityV1
import json

@dataclass
class Resource:
    uuid: str = None
    targetEntity: TargetEntityV1 = None
    spec: Union[ResourceAllocateV1, ResourceUpdateV1] = None
    requesterId: str = None
    resourceType: str = None
    serviceId: str = None
    resourceId: str = None
    status: str = None
    errorMessage: str = None
    createdDate: int = None
    lastModifiedDate: int = None

    def to_json(self):
        return json.dumps(self.__dict__, default=lambda x: x.__dict__, indent=4)