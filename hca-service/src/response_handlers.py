from json import loads
from time import time
from pydantic import parse_obj_as

from hca_messages import ResourceCreatedV1, ResourceDeallocatedV1, ResourceUpdatedV1, GenericResponseV1, ErrorV1

def handle_resource_created(props, message, db, logger):
    logger.debug("handle ResourceCreatedV1 message")

    response = parse_obj_as(ResourceCreatedV1, loads(message))
    
    request = db.hcaRequest.find_one({'_id': props.message_id})
    if not request:
        logger.error("cannot find request for id %s. cannot update resource" % props.message_id)
        return

    resourceId = request.get('resourceId')

    filter = {'_id': resourceId}
    newvalues = {'$set': {'status': 'created', 'resourceId': response.id, 'lastModifiedDate': int(time())*1000}}
    db.hcaResource.update_one(filter, newvalues)

    filter = {'_id': props.message_id}
    newvalue = {'$set': {'status': 'finished', 'lastModifiedDate': int(time())*1000}}
    db.hcaRequest.update_one(filter, newvalue)

    logger.debug(request)

def handle_error(props, message, db, logger):
    logger.debug("handle ErrorV1 message")
    response = parse_obj_as(ErrorV1, loads(message))
    request = db.hcaRequest.find_one({'_id': props.message_id})

    if not request:
        logger.error("cannot find request for id %s. cannot update resource" % props.message_id)
        return

    resourceId = request.get('resourceId')

    filter = {'_id': resourceId}
    newvalues = {'$set': {'status': 'failed', 'errorMessage': response.message, 'lastModifiedDate': int(time())*1000}}

    db.hcaResource.update_one(filter, newvalues)

    filter = {'_id': props.message_id}
    newvalue = {'$set': {'status': 'finished'}}
    db.hcaRequest.update_one(filter, newvalue)

def handle_generic_response(props, message, db, logger):
    logger.debug("handle GenericResponseV1 message")
    response = parse_obj_as(GenericResponseV1, loads(message))
    if response.message == 'group storage request created':
        request = db.hcaRequest.find_one({'_id': props.message_id})
        if not request:
            logger.error("cannot find request for id %s. cannot update resource" % props.message_id)
            return
        resourceId = request.get('resourceId')
        filter = {'_id': resourceId}
        newvalues = {'$set': {'status': 'inprogress', 'lastModifiedDate': int(time())*1000}}
        db.hcaResource.update_one(filter, newvalues)