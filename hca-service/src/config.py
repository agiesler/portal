import json
from dataclasses import dataclass
import os


@dataclass
class Config:
    rabbitmq_username: str
    rabbitmq_password: str
    dev_mode: bool = False
    mongodb_hostname: str = 'localhost'
    mongodb_port: int = 27017
    mongodb_database: str = 'local'
    mongodb_authdb: str = 'local'
    mongodb_username: str = None
    mongodb_password: str = None
    rabbitmq_use_ssl: bool = True
    rabbitmq_cafile: str = '/etc/ssl/certs/ca-certificates.crt'
    rabbitmq_hostname: str = 'hifis-rabbitmq.desy.de'
    rabbitmq_port: int = 5671
    rabbitmq_retry_delay: int = 20
    rabbitmq_connection_attempts: int = 3
    rabbitmq_heartbeat: int = None
    log_level: str = 'INFO'


def from_json_file(path) -> Config:
    with open(path, 'r') as f:
        return json.loads(f.read(), object_hook=lambda d: Config(**d))


def from_env() -> Config:
    rabbitmq_username = os.getenv('HCA_RABBITMQ_USERNAME')
    rabbitmq_password = os.getenv('HCA_RABBITMQ_PASSWORD')
    if rabbitmq_username is None or rabbitmq_password is None:
        raise Exception('Missing HCA_RABBITMQ_USERNAME or HCA_RABBITMQ_PASSWORD env variable')
    c = Config(rabbitmq_username=rabbitmq_username, rabbitmq_password=rabbitmq_password)
    c.dev_mode = os.getenv("HCA_DEV_MODE", str(c.dev_mode)).lower() in ['true', '1']
    c.mongodb_hostname = os.getenv("HCA_MONGODB_HOSTNAME", c.mongodb_hostname)
    c.mongodb_port = int(os.getenv("HCA_MONGODB_PORT", c.mongodb_port))
    c.mongodb_database = os.getenv("HCA_MONGODB_DATABASE", c.mongodb_database)
    c.mongodb_authdb = os.getenv("HCA_MONGODB_AUTHDB", c.mongodb_authdb)
    c.mongodb_username = os.getenv("HCA_MONGODB_USERNAME", c.mongodb_username)
    c.mongodb_password = os.getenv("HCA_MONGODB_PASSWORD", c.mongodb_password)
    c.rabbitmq_hostname = os.getenv("HCA_RABBITMQ_HOSTNAME", c.rabbitmq_hostname)
    c.rabbitmq_port = int(os.getenv("HCA_RABBITMQ_PORT", c.rabbitmq_port))
    c.rabbitmq_cafile = os.getenv("HCA_RABBITMQ_CAFILE", c.rabbitmq_cafile)
    c.rabbitmq_use_ssl = os.getenv("HCA_RABBITMQ_USE_SSL", c.rabbitmq_use_ssl)
    c.rabbitmq_retry_delay = os.getenv("HCA_RABBITMQ_RETRY_DELAY", c.rabbitmq_retry_delay)
    c.rabbitmq_connection_attempts = os.getenv("HCA_RABBITMQ_CONNECTION_ATTEMPTS", c.rabbitmq_connection_attempts)
    c.rabbitmq_heartbeat = os.getenv("HCA_RABBITMQ_HEARTBEAT", c.rabbitmq_heartbeat)
    c.log_level = os.getenv("HCA_LOG_LEVEL", c.log_level)
    return c
