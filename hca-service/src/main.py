import dataclasses
from importlib.resources import Resource
import json
import logging

import ssl
import pika
import config
import time
import hca_messages
from pika.exceptions import AMQPConnectionError, ConnectionClosedByBroker
from pathlib import Path
from pymongo import MongoClient
from urllib.parse import quote_plus
from threading import Thread, Lock
from pydantic import ValidationError, parse_obj_as
from json import dumps
from request import Request
from resource import Resource
from uuid import uuid1
import response_handlers

# Name of client and server queue
receiver_queue, sender_queue = 'to_portal', 'to_hca'

# Global logger
logging.basicConfig(format='%(asctime)s\t%(levelname)s\t%(filename)s\t%(threadName)s\t%(funcName)s\t%(message)s')

logger = logging.getLogger('hca-service')

threadLock = Lock()

def get_database(host, port, database, authdb, username=None, password=None):
    if username:
        uri = "mongodb://%s:%s@%s:%d/%s" % (quote_plus(username), quote_plus(password), host, port, authdb)
    else:
        uri = "mongodb://%s:%d" % (host, port)
    client = MongoClient(uri)
    return client[database]


def get_pika_params(services, conf):
    pika_params = []
    for service in services:
        credentials = pika.PlainCredentials(conf.rabbitmq_username, conf.rabbitmq_password)
        if conf.rabbitmq_use_ssl:
            context = ssl.create_default_context(cafile=conf.rabbitmq_cafile)
            ssl_options = pika.SSLOptions(context, conf.rabbitmq_hostname)
            pika_param = pika.ConnectionParameters(
                host=conf.rabbitmq_hostname,
                port=conf.rabbitmq_port,
                credentials=credentials,
                virtual_host=service.get('vhost'),
                ssl_options=ssl_options,
                retry_delay=20,
                connection_attempts=3,
                heartbeat=None)
        else:
            pika_param = pika.ConnectionParameters(
                host=conf.rabbitmq_hostname,
                port=conf.rabbitmq_port,
                credentials=credentials,
                virtual_host=service.get('vhost'),
                retry_delay=20,
                connection_attempts=3,
                heartbeat=None)
        pika_params.append(pika_param)
    return pika_params


def main():
    # First try to load config from file, else from env vars
    if Path('config.json').is_file():
        logger.info("Trying to load config.json")
        conf = config.from_json_file('config.json')
    else:
        logger.info("No config.json found loading config from env")
        conf = config.from_env()
    if conf.dev_mode:
        for key, value in conf.__dict__.items():
            print("%s: %s" % (key, value))
    logger.setLevel(conf.log_level)

    db = get_database(conf.mongodb_hostname, conf.mongodb_port, conf.mongodb_database, conf.mongodb_authdb, conf.mongodb_username, conf.mongodb_password)

    services = list(db.hcaMapping.find())

    logger.debug(services)
    pika_params = get_pika_params(services, conf)

    threads = []

    for pika_param in pika_params:
        vhost = pika_param._virtual_host
        serviceName = db.hcaMapping.find({"vhost": vhost}).next().get('serviceName')
        t = Thread(target=start_receiver, name="Receiver %s" % serviceName, args=(pika_param, db))
        threads.append(t)


    t = Thread(target=start_sender, name="Sender", args=(pika_params, db))
    threads.append(t)

    [t.start() for t in threads]

    for t in threads:
        t.join()


def on_message(channel, method, props, body, db):
    logger.debug("got message: %s", body)
    dataclass_type = getattr(hca_messages, props.type)

    if not hasattr(response_handlers, dataclass_type.__handler__):
        logger.error("Cannot load handler for message type %s\nMessage: %s" % (props.type, body))
    handler = getattr(response_handlers, dataclass_type.__handler__)
    handler(props, body, db, logger)


def start_receiver(pika_param, db):
    logger.info("starting receiver...")
    while True:
        try:
            connection = pika.BlockingConnection(parameters=pika_param)
            channel = connection.channel()

            # Declare queue for receiving messages
            channel.queue_declare(queue=receiver_queue, durable=True)

            # Start consuming messages from RabbitMQ
            channel.basic_consume(queue=receiver_queue,
                                on_message_callback=lambda c, m, p, b: on_message(c, m, p, b, db),
                                auto_ack=True)
            logger.info('Start receiving messages...')
            channel.start_consuming()
        except ConnectionClosedByBroker as ex:
            logger.warning("Connection was closed by broker. Retry connection.")
            time.sleep(30)
        except AMQPConnectionError as ex:
            logger.error(str(ex))
            logger.error("Connection error. Retry connection.")
            time.sleep(30)
        except Exception as ex:
            logger.error("unknown exception: %s" % str(ex))
            time.sleep(30)
        finally:
            channel.close()
            connection.close()


def get_new_requests_from_db(db):
    requests = []
    for request in db.hcaRequest.find({'status': 'new'}):
        request['uuid'] = request.get('_id')
        request.pop('_id')
        request.pop('_class')
        request.pop('foreignKeys')
        request.get('spec').pop('_class')
        requests.append(request)

    return requests

def send_message(pika_params, props, message):
    while True:
        try:
            logger.debug('try sending message...')
            # Initialize connection to RabbitMQ
            connection = pika.BlockingConnection(parameters=pika_params)
            channel = connection.channel()

            # Declare queue for sending messages
            channel.queue_declare(queue=sender_queue, durable=True)
            channel.basic_publish(exchange='',
                                routing_key=sender_queue,
                                body=str.encode(json.dumps(dataclasses.asdict(message))),
                                properties=props)
            break
        except pika.exceptions.AMQPConnectionError as ex:
            logger.error(str(ex))
            logger.error("Connection error. Retry connection.")
            time.sleep(30)
        except pika.exceptions.Exception as ex:
            logger.error("Unknown error: %s" % str(ex))
        finally:
            channel.close()
            connection.close()

def start_sender(pika_params, db):
    logger.info("Starting sender...")
    vhost_mapping = {}
    for pika_param in pika_params:
        vhost = pika_param._virtual_host
        hcaMapping = db.hcaMapping.find({"vhost": vhost}).next()
        logger.debug("adding connection for %s (vhost: %s, id: %s)" % (hcaMapping['serviceName'], vhost, hcaMapping['serviceId']))
        vhost_mapping[hcaMapping.get('serviceId')] = {'pika_param': pika_param, 'serviceName': hcaMapping.get('serviceName')}

    logger.debug(vhost_mapping)
    while True:
        new_requests = get_new_requests_from_db(db)

        for request in new_requests:
            try:
                logger.debug(request)
                request = parse_obj_as(Request, request)

                uuid = 'res-' + str(uuid1())

                logger.debug(request)

                resource = Resource(uuid=uuid, targetEntity=request.spec.targetEntity, serviceId=request.serviceId, createdDate=int(time.time())*1000, lastModifiedDate=int(time.time())*1000, spec=request.spec.specification, status="requested", resourceId="", resourceType=request.spec.type, requesterId=request.requesterId)

                logger.debug(resource)
                
                props = pika.BasicProperties(type=request.spec.__class__.__name__, correlation_id=request.uuid, app_id=vhost_mapping[request.serviceId]['serviceName'])

                send_message(vhost_mapping[request.serviceId]['pika_param'], props, request.spec)
                resource = json.loads(resource.to_json())
                resource['_id'] = resource.get('uuid')
                db.hcaResource.insert_one(resource)
                filter = {'_id': request.uuid}

                newvalue = {'$set': {'status': 'sent', 'resourceId': resource.get('uuid')}}
                db.hcaRequest.update_one(filter, newvalue)

            except KeyError as ex:
                logger.error("cannot find RabbitMQ connection for vhost '%s' %s" % (request.serviceId, str(ex)))
                continue
            except ValidationError as ex:
                logger.error("cannot decode request %s" % str(ex))
                continue

        time.sleep(30)

if __name__ == "__main__":
    # TODO: Properly handle sigint
    main()
