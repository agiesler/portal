from typing import List, Union
from dataclasses import dataclass
from hca_messages import ResourceAllocateV1, ResourceUpdateV1

@dataclass
class Request:
    uuid: str = None
    version: str = None
    spec: Union[ResourceAllocateV1, ResourceUpdateV1] = None
    requesterId: str = None
    serviceId: str = None
    status: str = None
    createdDate: int = None
    lastModifiedDate: int = None
    resourceId: str = None
