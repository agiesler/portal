
package de.helmholtz.cloud.hca.message.resources;

import javax.annotation.processing.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import de.helmholtz.cloud.hca.message.AllocateResourceSpecification;


/**
 * GroupStorageResourceSpecV1
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "desiredName",
    "quota"
})
@Generated("jsonschema2pojo")
public class GroupStorageResourceSpecV1 extends AllocateResourceSpecification{

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("desiredName")
    private String desiredName;
    /**
     * QuotaV1
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("quota")
    private QuotaSpecV1 quota;

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("desiredName")
    public String getDesiredName() {
        return desiredName;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("desiredName")
    public void setDesiredName(String desiredName) {
        this.desiredName = desiredName;
    }

    /**
     * QuotaV1
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("quota")
    public QuotaSpecV1 getQuota() {
        return quota;
    }

    /**
     * QuotaV1
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("quota")
    public void setQuota(QuotaSpecV1 quota) {
        this.quota = quota;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(GroupStorageResourceSpecV1.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("desiredName");
        sb.append('=');
        sb.append(((this.desiredName == null)?"<null>":this.desiredName));
        sb.append(',');
        sb.append("quota");
        sb.append('=');
        sb.append(((this.quota == null)?"<null>":this.quota));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.desiredName == null)? 0 :this.desiredName.hashCode()));
        result = ((result* 31)+((this.quota == null)? 0 :this.quota.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof GroupStorageResourceSpecV1) == false) {
            return false;
        }
        GroupStorageResourceSpecV1 rhs = ((GroupStorageResourceSpecV1) other);
        return (((this.desiredName == rhs.desiredName)||((this.desiredName!= null)&&this.desiredName.equals(rhs.desiredName)))&&((this.quota == rhs.quota)||((this.quota!= null)&&this.quota.equals(rhs.quota))));
    }

}
