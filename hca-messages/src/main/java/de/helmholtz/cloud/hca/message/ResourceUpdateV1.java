package de.helmholtz.cloud.hca.message;

import javax.annotation.processing.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * ResourceUpdateV1
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "targetEntity",
    "type",
    "specification"
})
@Generated("jsonschema2pojo")
public class ResourceUpdateV1 extends RequestV1{

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("id")
    private String id;
    /**
     * TargetEntityV1
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("targetEntity")
    private TargetEntityV1 targetEntity;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    private String type;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("specification")
    private UpdateResourceSpecification specification;

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("id")
    public String getId() {
        return id;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    /**
     * TargetEntityV1
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("targetEntity")
    public TargetEntityV1 getTargetEntity() {
        return targetEntity;
    }

    /**
     * TargetEntityV1
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("targetEntity")
    public void setTargetEntity(TargetEntityV1 targetEntity) {
        this.targetEntity = targetEntity;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("specification")
    public UpdateResourceSpecification getSpecification() {
        return specification;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("specification")
    public void setSpecification(UpdateResourceSpecification specification) {
        this.specification = specification;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(ResourceUpdateV1.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("id");
        sb.append('=');
        sb.append(((this.id == null)?"<null>":this.id));
        sb.append(',');
        sb.append("targetEntity");
        sb.append('=');
        sb.append(((this.targetEntity == null)?"<null>":this.targetEntity));
        sb.append(',');
        sb.append("type");
        sb.append('=');
        sb.append(((this.type == null)?"<null>":this.type));
        sb.append(',');
        sb.append("specification");
        sb.append('=');
        sb.append(((this.specification == null)?"<null>":this.specification));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.specification == null)? 0 :this.specification.hashCode()));
        result = ((result* 31)+((this.id == null)? 0 :this.id.hashCode()));
        result = ((result* 31)+((this.targetEntity == null)? 0 :this.targetEntity.hashCode()));
        result = ((result* 31)+((this.type == null)? 0 :this.type.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ResourceUpdateV1) == false) {
            return false;
        }
        ResourceUpdateV1 rhs = ((ResourceUpdateV1) other);
        return (((((this.specification == rhs.specification)||((this.specification!= null)&&this.specification.equals(rhs.specification)))&&((this.id == rhs.id)||((this.id!= null)&&this.id.equals(rhs.id))))&&((this.targetEntity == rhs.targetEntity)||((this.targetEntity!= null)&&this.targetEntity.equals(rhs.targetEntity))))&&((this.type == rhs.type)||((this.type!= null)&&this.type.equals(rhs.type))));
    }

}
