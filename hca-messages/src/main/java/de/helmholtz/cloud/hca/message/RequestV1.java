package de.helmholtz.cloud.hca.message;

public abstract class RequestV1 {
    public RequestV1() { }

    @Override
    public abstract boolean equals(Object obj);
}
