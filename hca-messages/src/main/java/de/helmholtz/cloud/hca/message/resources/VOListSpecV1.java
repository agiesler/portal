
package de.helmholtz.cloud.hca.message.resources;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import de.helmholtz.cloud.hca.message.TargetEntitySpecification;


/**
 * VOListSpecV1
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "vos"
})
@Generated("jsonschema2pojo")
public class VOListSpecV1 extends TargetEntitySpecification{

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("vos")
    private List<Object> vos = new ArrayList<Object>();

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("vos")
    public List<Object> getVos() {
        return vos;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("vos")
    public void setVos(List<Object> vos) {
        this.vos = vos;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(VOListSpecV1.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("vos");
        sb.append('=');
        sb.append(((this.vos == null)?"<null>":this.vos));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.vos == null)? 0 :this.vos.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof VOListSpecV1) == false) {
            return false;
        }
        VOListSpecV1 rhs = ((VOListSpecV1) other);
        return ((this.vos == rhs.vos)||((this.vos!= null)&&this.vos.equals(rhs.vos)));
    }

}
