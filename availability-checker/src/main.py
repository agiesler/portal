from pymongo import MongoClient, ReplaceOne
from requests import get, ConnectionError, Timeout
from datetime import datetime
from elasticsearch import Elasticsearch
from os import environ
from time import sleep

def checkAvailability(entrypoint, retries=3, delay=10):
    for i in range(retries):
        lastResp = None
        try:
            r = get(entrypoint, timeout=5)
            if r.status_code == 200:
                return r.status_code, ""
            lastResp = r.status_code, ""
        except ConnectionError as ex:
            lastResp = -1, str(ex)
        except Timeout as ex:
            lastResp = -2, str(ex)
        sleep(delay)
    return lastResp

def getServices(collection):
    services = []
    for service in collection.find():
        services.append({key: service[key] for key in service.keys() & {'_id', 'availabilityUrl', 'name'}})
    
    return services

def updateDb(collection, services):
    updates = []
    for service in services:
        updates.append(ReplaceOne({"_id": service["_id"]}, service, upsert=True))
    collection.bulk_write(updates)

def writeToES(services):
    host = environ.get("ES_HOST")
    if not host:
        print("Elasticsearch host not set. Will not write to ES.")
        return

    port = int(environ.get("ES_PORT"))
    indexpattern = environ.get("ES_INDEXPATTERN")
    use_ssl = bool(environ.get("ES_USE_SSL"))
    release = environ.get("ES_RELEASE")

    username = environ.get("ES_USERNAME")
    password = environ.get("ES_PASSWORD")

    currentMonth = datetime.now().month
    currentYear = datetime.now().year

    index = f"{indexpattern}-{currentYear}.{currentMonth}"

    if not username:
        es = Elasticsearch(
            [host],
            port=port,
            use_ssl=use_ssl,
            verify_certs=False,
            ssl_show_warn=False
            )
    else:
        es = Elasticsearch(
            [host],
            http_auth=(username, password),
            port=port,
            use_ssl=use_ssl,
            verify_certs=False,
            ssl_show_warn=False
            )

    try:
        for service in services:
            service['service_id'] = service.pop('_id')
            if release:
                service['release'] = release
            res = es.index(index=index, document=service)
            print(res)
    except Exception as ex:
        print(ex)

if __name__ == "__main__":
    host = environ.get("MONGODB_HOST", 'localhost')
    port = int(environ.get("MONGODB_PORT", "27017"))

    database = environ.get("MONGODB_DATABASE", "local")

    username = environ.get("MONGODB_USERNAME")
    password = environ.get("MONGODB_PASSWORD")
    authSource = environ.get("MONGODB_AUTHDB")

    if username:
        client = MongoClient(host=host, port=port, username=username, password=password, authSource=authSource)
    else:
        client = MongoClient(host=host, port=port)

    db = client.get_database(database)

    services = getServices(db.marketService)

    print(services)

    for service in services:
        status, errorMsg = checkAvailability(service['availabilityUrl'])
        service['status'] = status
        service['errorMsg'] = errorMsg
        service['lastUpdated'] = datetime.now()
        del service['availabilityUrl']
        print(service)
    
    updateDb(db.availability, services)

    writeToES(services)