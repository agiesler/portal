#!/usr/bin/env python3
from sh import mongodump
from datetime import datetime
import boto3
from botocore.client import Config
import os

now = datetime.now()
now_string = now.strftime('%Y%m%d-%H%M')
release = os.environ['RELEASE']
host = os.environ['MONGODB_HOST']
port = os.environ['MONGODB_PORT']
auth_db = os.environ['MONGODB_AUTHDB']
user = os.environ['MONGODB_USERNAME']
password = os.environ['MONGODB_PASSWORD']
database = os.environ['MONGODB_DATABASE']

backupfile = f'db-backup-{release}-{now_string}.gz'

mongodump('--host', host, '--port', port, '--authenticationDatabase', auth_db, '--username', user, '--password', password, '--db', database, '--forceTableScan', '--gzip', f'--archive={backupfile}')

s3 = boto3.resource('s3',
        endpoint_url=os.environ['S3_ENDPOINT_URL'],
        aws_access_key_id=os.environ['AWS_ACCESS_KEY_ID'],
        aws_secret_access_key=os.environ['AWS_SECRET_ACCESS_KEY'],
        config=Config(signature_version='s3v4'),
        region_name=os.environ['AWS_DEFAULT_REGION'])

s3.Bucket("cloud-portal").upload_file(backupfile, os.path.join(release, backupfile))
