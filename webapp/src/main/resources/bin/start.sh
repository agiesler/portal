#!/bin/bash

port=8080
cerebrumEndpoint=http://localhost:8090/api/v0/
currentDir=$(pwd)
parentDir="$(dirname "$currentDir")"

while getopts "a:d:f:p:s:l:c:" flag;
do
    case "${flag}" in
        a) profile=${OPTARG};;
        d) parentDir=${OPTARG};;
        f) filePath=${OPTARG};;
        p) port=${OPTARG};;
        s) secret=${OPTARG};;
        l) logging=${OPTARG};;
        c) cerebrumEndpoint=${OPTARG};;
        *) echo 'error' >&2
           exit 1
    esac
done

if [ ! "$secret" ]; then
    echo 'Option -s missing' >&2
    exit 1
fi

if [ ! "$filePath" ]; then
    filePath=file:$parentDir/webapp/
fi

echo $cerebrumEndpoint

if [ ! "$profile" ]; then
    java -Xms256m -Xmx512m -server -jar "$parentDir"/lib/webapp-0.0.2-SNAPSHOT.jar --spring.security.oauth2.client.registration.unity.client-secret=$secret --server.port=$port --spring.web.resources.static-locations="$filePath" --server.servlet.context-path="$CONTEXT_PATH" --server.jetty.accesslog.enabled=$logging --cerebrum.endpoint=$cerebrumEndpoint
    exit 1
fi

java -Xms256m -Xmx512m -Dspring.profiles.active="$profile" -server -jar "$parentDir"/lib/webapp-0.0.2-SNAPSHOT.jar --spring.security.oauth2.client.registration.unity.client-secret=$secret --server.port=$port --spring.web.resources.static-locations="$filePath" --server.servlet.context-path="$CONTEXT_PATH" --server.jetty.accesslog.enabled=$logging --cerebrum.endpoint=$cerebrumEndpoint
