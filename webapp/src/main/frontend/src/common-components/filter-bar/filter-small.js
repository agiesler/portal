import { LitElement, html, css } from 'lit';

class FilterSmall extends LitElement {
    constructor() {
        super();
    }
    static get properties() {
        return {
            keyworldList: {
                type: Array
            },
            selectedKeywords: {
                type: Array
            },
            providerList: {
                type: Array
            },
            selectedProviders: {
                type: Array
            },
            softwareList: {
                type: Array
            },
            selectedSoftware: {
                type: Array
            },
            serviceSort: {
                type: String
            },
            searchInput: {
                type: String
            }
        };
    }
    static get styles() {
        return css`
            :host {
                width: 100%;
                height: 100%;
                display: block;
                box-sizing: border-box;
            }
            * {
                box-sizing: border-box;
            }
            .container {
                display: flex;
                flex-direction: row;
                width: 100%;
                -ms-overflow-style: none;  /* IE and Edge */
                scrollbar-width: none;  /* Firefox */
            }
            .search {
                padding-top: 18px;
                padding-left: 20px;
                padding-right: 20px;
                padding-bottom: 5px;
                width: 100%;
            }
            #servicesearch {
                background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSIzMnB4IiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAzMiAzMiIgd2lkdGg9IjMycHgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6c2tldGNoPSJodHRwOi8vd3d3LmJvaGVtaWFuY29kaW5nLmNvbS9za2V0Y2gvbnMiIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48dGl0bGUvPjxkZXNjLz48ZGVmcy8+PGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIiBpZD0iUGFnZS0xIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSI+PGcgZmlsbD0iIzkyOTI5MiIgaWQ9Imljb24tMTExLXNlYXJjaCI+PHBhdGggZD0iTTE5LjQyNzExNjQsMjAuNDI3MTE2NCBDMTguMDM3MjQ5NSwyMS40MTc0ODAzIDE2LjMzNjY1MjIsMjIgMTQuNSwyMiBDOS44MDU1NzkzOSwyMiA2LDE4LjE5NDQyMDYgNiwxMy41IEM2LDguODA1NTc5MzkgOS44MDU1NzkzOSw1IDE0LjUsNSBDMTkuMTk0NDIwNiw1IDIzLDguODA1NTc5MzkgMjMsMTMuNSBDMjMsMTUuODQ3MjEwMyAyMi4wNDg2MDUyLDE3Ljk3MjIxMDMgMjAuNTEwNDA3NywxOS41MTA0MDc3IEwyNi41MDc3NzM2LDI1LjUwNzc3MzYgQzI2Ljc4MjgyOCwyNS43ODI4MjggMjYuNzc2MTQyNCwyNi4yMjM4NTc2IDI2LjUsMjYuNSBDMjYuMjIxOTMyNCwyNi43NzgwNjc2IDI1Ljc3OTYyMjcsMjYuNzc5NjIyNyAyNS41MDc3NzM2LDI2LjUwNzc3MzYgTDE5LjQyNzExNjQsMjAuNDI3MTE2NCBMMTkuNDI3MTE2NCwyMC40MjcxMTY0IFogTTE0LjUsMjEgQzE4LjY0MjEzNTgsMjEgMjIsMTcuNjQyMTM1OCAyMiwxMy41IEMyMiw5LjM1Nzg2NDE3IDE4LjY0MjEzNTgsNiAxNC41LDYgQzEwLjM1Nzg2NDIsNiA3LDkuMzU3ODY0MTcgNywxMy41IEM3LDE3LjY0MjEzNTggMTAuMzU3ODY0MiwyMSAxNC41LDIxIEwxNC41LDIxIFoiIGlkPSJzZWFyY2giLz48L2c+PC9nPjwvc3ZnPg==);
                background-position: right 10px bottom 5px;
                background-repeat: no-repeat;
                background-size: 25px;
                border: 1px solid #ccc;
                -webkit-border-radius: 4px;
                -moz-border-radius: 4px;
                -ms-border-radius: 4px;
                -o-border-radius: 4px;
                border-radius: 4px;
                padding: 10px 40px 10px 20px;
                text-align: right;
                -webkit-transition: all 0.2s;
                -moz-transition: all 2s;
                transition: all 0.2s;
                width: 100%;
            }
            #servicesearch::placeholder{
                text-align: right;
            }
            #filtershow {
                padding-top: 10px;
                display: inline-grid;
                text-align: right;
                width: 100%;
                justify-content: right;
                align-items: right;
            }
            #filtershow a {
                text-decoration: none;
                color: #808080;
                font-family: inherit;
                font-size: 0.84em;
            }
            #filtershow mwc-icon {
                vertical-align: middle;
                padding-right: 10px;
                padding-left: 6px;
                cursor: pointer;
            }
            form[role=search] {
                display: grid;
                width: 100%;
            }
            form[role=search] label {
                margin-left: 5px;
            }
            form[role=search] input {
                margin-left: 10px;
            }
            form[role=search] select {
                margin-left: 10px;
            }
            form[role=search] button {
                border: none;
                color: white;
                background-color: #005aa0;
                padding: 15px 32px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                cursor: pointer;
            }
            .filters {
                position: fixed; /* Sit on top of the page content */
                display: none; /* Hidden by default */
                width: 100%; /* Full width (cover the whole page) */
                height: calc(100% - 51px);%; /* Full height - the header height */
                background-color: white;
                z-index: 2; /* Specify a stack order in case you're using a different order for other elements */
                overflow-y: scroll;
                -ms-overflow-style: none;  /* IE and Edge */
                scrollbar-width: none;  /* Firefox */
              }
            .dropdown-check-list {
                display: inline-block;
            }
            .dropdown-check-list .anchor {
                position: relative;
                cursor: pointer;
                display: inline-block;
                padding: 5px 50px 15px 10px;
                width: 100%;
            }
            .dropdown-check-list .anchor:after {
                position: absolute;
                content: "";
                border-left: 2px solid black;
                border-top: 2px solid black;
                padding: 5px;
                right: 20px;
                top: 20%;
                -moz-transform: rotate(-135deg);
                -ms-transform: rotate(-135deg);
                -o-transform: rotate(-135deg);
                -webkit-transform: rotate(-135deg);
                transform: rotate(-135deg);
            }
            .dropdown-check-list .anchor:active:after {
                right: 8px;
                top: 21%;
            }
            .dropdown-check-list ul.items {
                padding: 2px;
                display: none;
                margin: 0;
                border-top: none;
            }
            .dropdown-check-list ul.items li {
                list-style: none;
            }
            .dropdown-check-list.visible .anchor {
                color: #0094ff;
            }
            .dropdown-check-list.visible .items {
                display: block;
            }
        `;
    }
    render() {
        return html`
        <div class="container">
            <div class="search">
                <input type="search" id="servicesearch" name="servicesearch" placeholder="Search services">
                <div id="filtershow">
                    <a @click=${this._showFilters}>All categories<mwc-icon>filter_list</mwc-icon></a>
                </div>
            </div>
            <div class="filters" id="overlay">
                <form role="search">
                    <div id="keywordList" class="dropdown-check-list" tabindex="100">
                        <span class="anchor">Select Keywords</span>
                        <ul class="items"></ul>
                    </div>
                    <div id="providerList" class="dropdown-check-list" tabindex="100">
                        <span class="anchor">Select Providers</span>
                        <ul class="items"></ul>
                    </div>
                    <div id="softwareList" class="dropdown-check-list" tabindex="100">
                        <span class="anchor">Select Software</span>
                        <ul class="items"></ul>
                    </div>
                    <div id="serviceSort" class="dropdown-check-list" tabindex="100">
                        <span class="anchor">Sort Services</span>
                        <ul class="items">
                            <li>
                                <input id="sortSoftware" type="radio" value="software" name="serviceSort" checked>
                                <label for="sortSoftware">Service Software</label>
                            </li>
                            <li>
                                <input id="sortName" type="radio" value="name" name="serviceSort">
                                <label for="sortName">Service Name</label>
                            </li>
                            <li>
                                <input id="sortProvider" type="radio" value="providerName" name="serviceSort">
                                <label for="sortProvider">Service Provider</label>
                            </li>
                        </ul>
                    </div>
                    <button @click="${this._applyFilters}" type="button" id="applybutton">Apply filters</button>
                    <button @click="${this._resetFilters}" type="button" id="clearbutton">Reset all filters</button>
                    <button @click="${this._closeFilters}" type="button" id="closebutton">Close</button>
                </form>
            </div>
        </div>
        `;
    }
    firstUpdated() {
        var servicesearch = this.shadowRoot.getElementById("servicesearch");
        servicesearch.addEventListener("keyup", this._applyFilters.bind(this));
        /* add handlers to the dropdown menus that handle the actual drop down of options */
        var keywordList = this.shadowRoot.getElementById('keywordList');
        var providerList = this.shadowRoot.getElementById('providerList');
        var softwareList = this.shadowRoot.getElementById('softwareList');
        var serviceSort = this.shadowRoot.getElementById('serviceSort');
        keywordList.getElementsByClassName('anchor')[0].onclick = function (evt) {
            if (keywordList.classList.contains('visible')) {
                keywordList.classList.remove('visible');
            } else {
                keywordList.classList.add('visible');
                providerList.classList.remove('visible');
                softwareList.classList.remove('visible');
                serviceSort.classList.remove('visible');
            }
        }
        providerList.getElementsByClassName('anchor')[0].onclick = function (evt) {
            if (providerList.classList.contains('visible')) {
                providerList.classList.remove('visible');
            } else {
                keywordList.classList.remove('visible');
                providerList.classList.add('visible');
                softwareList.classList.remove('visible');
                serviceSort.classList.remove('visible');
            }
        }
        softwareList.getElementsByClassName('anchor')[0].onclick = function (evt) {
            if (softwareList.classList.contains('visible')) {
                softwareList.classList.remove('visible');
            } else {
                keywordList.classList.remove('visible');
                providerList.classList.remove('visible');
                softwareList.classList.add('visible');
                serviceSort.classList.remove('visible');
            }
        }
        serviceSort.getElementsByClassName('anchor')[0].onclick = function (evt) {
            if (serviceSort.classList.contains('visible')) {
                serviceSort.classList.remove('visible');
            } else {
                keywordList.classList.remove('visible');
                providerList.classList.remove('visible');
                softwareList.classList.remove('visible');
                serviceSort.classList.add('visible');
            }
        }
        this._fillOptions();
    }
    /* needed to generate unique identifier to match labels to the input checkboxes */
    guidGenerator() {
        var S4 = function () {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    }
    /* fills the dropdown menus with the options provided by the services-view */
    _fillOptions() {
        var keywordItems = this.shadowRoot.getElementById('keywordList').getElementsByClassName('items')[0];
        this.keywordList.forEach(keyword => {
            var id = this.guidGenerator();
            var li = document.createElement('li');
            var input = document.createElement('input');
            input.type = 'checkbox';
            input.value = keyword;
            input.id = id;
            var label = document.createElement('label');
            label.setAttribute('for', id);
            label.innerHTML = keyword;
            li.appendChild(input);
            li.appendChild(label);
            if (this.selectedKeywords.includes(keyword)) {
                input.setAttribute("checked", "true");
            }
            keywordItems.appendChild(li);
        });
        var softwareItems = this.shadowRoot.getElementById('softwareList').getElementsByClassName('items')[0];
        this.softwareList.forEach(keyword => {
            var id = this.guidGenerator();
            var li = document.createElement('li');
            var input = document.createElement('input');
            input.type = 'checkbox';
            input.value = keyword;
            input.id = id;
            var label = document.createElement('label');
            label.setAttribute('for', id);
            label.innerHTML = keyword;
            li.appendChild(input);
            li.appendChild(label);
            if (this.selectedSoftware.includes(keyword)) {
                input.setAttribute("checked", "true");
            }
            softwareItems.appendChild(li);
        });
        var providerItems = this.shadowRoot.getElementById('providerList').getElementsByClassName('items')[0];
        this.providerList.forEach(keyword => {
            var id = this.guidGenerator();
            var li = document.createElement('li');
            var input = document.createElement('input');
            input.type = 'checkbox';
            input.value = keyword;
            input.id = id;
            var label = document.createElement('label');
            label.setAttribute('for', id);
            label.innerHTML = keyword;
            li.appendChild(input);
            li.appendChild(label);
            if (this.selectedProviders.includes(keyword)) {
                input.setAttribute("checked", "true");
            }
            providerItems.appendChild(li);
        });
        var serviceSort = this.shadowRoot.getElementById('serviceSort').getElementsByTagName('li');
        for (let i = 0; i < serviceSort.length; i++) {
            var input = serviceSort[i].getElementsByTagName('input')[0];
            if (input.value == this.serviceSort) {
                input.setAttribute("checked", "true");
            }
            input.onclick = function (e) {
                var originalElement = e.srcElement || e.originalTarget;
                this.serviceSort = originalElement.value;
            }.bind(this);
        }
        this.shadowRoot.getElementById('servicesearch').value = this.searchInput;
    }
    /* collects all selected filter options and sends an event up to services-view where the filtering is applied */
    _applyFilters() {
        this._collapseAllDropdown();
        var searchInput = this.shadowRoot.getElementById("servicesearch").value;
        var selectedProviders = [];
        var selectedKeywords = [];
        var selectedSoftware = [];
        var providerItems = this.shadowRoot.getElementById('providerList').getElementsByClassName('items')[0].getElementsByTagName('li');
        for (let i = 0; i < providerItems.length; i++) {
            if (providerItems[i].childNodes[0].checked) {
                selectedProviders.push(providerItems[i].childNodes[0].value);
            }
        }
        var keywordItems = this.shadowRoot.getElementById('keywordList').getElementsByClassName('items')[0].getElementsByTagName('li');
        for (let i = 0; i < keywordItems.length; i++) {
            if (keywordItems[i].childNodes[0].checked) {
                selectedKeywords.push(keywordItems[i].childNodes[0].value);
            }
        }
        var softwareItems = this.shadowRoot.getElementById('softwareList').getElementsByClassName('items')[0].getElementsByTagName('li');
        for (let i = 0; i < softwareItems.length; i++) {
            if (softwareItems[i].childNodes[0].checked) {
                selectedSoftware.push(softwareItems[i].childNodes[0].value);
            }
        }
        var result = {
            'searchInput': searchInput,
            'selectedProviders': selectedProviders,
            'selectedKeywords': selectedKeywords,
            'selectedSoftware': selectedSoftware,
            'serviceSort': this.serviceSort
        }
        this.dispatchEvent(new CustomEvent('helmholtz-cloud-apply-filters', {
            detail: { filters: result }, bubbles: true, composed: true
        }));
        this.shadowRoot.getElementById("overlay").style.display = 'none';
    }
    /* display the overlay with the filtering options */
    _showFilters(e) {
        this.shadowRoot.getElementById("overlay").style.display = 'block';
    }
    /* hides the filter overlay */
    _closeFilters() {
        this._collapseAllDropdown();
        this.shadowRoot.getElementById("overlay").style.display = "none";
    }
    /* collapse all dropdown menus at once */
    _collapseAllDropdown() {
        this.shadowRoot.getElementById('keywordList').classList.remove('visible');
        this.shadowRoot.getElementById('providerList').classList.remove('visible');
        this.shadowRoot.getElementById('softwareList').classList.remove('visible');
        this.shadowRoot.getElementById('serviceSort').classList.remove('visible');
    }
    /* send an event to services-view to reset all filters */
    _resetFilters() {
        this._collapseAllDropdown();
        this.dispatchEvent(new CustomEvent('helmholtz-cloud-reset-filters', {
            detail: { reset: true }, bubbles: true, composed: true
        }));
    }
}
customElements.define('filter-small', FilterSmall);