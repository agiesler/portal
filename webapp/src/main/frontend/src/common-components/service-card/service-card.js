import { LitElement, html, css } from 'lit';

import '@material/mwc-icon';
import '@material/mwc-icon-button';

class ServiceCard extends LitElement {
    constructor() {
        super();
        this.test = "A";
        this.abbreviation = "";
        this.img = "";
        this.software = "";
    }
    static get properties() {
        return {
            service: {
                type: Object
            },
            img: {
                type: String
            },
            providerImg: {
                type: String
            },
            software: {
                type: String
            }
        };
    }
    static get styles() {
        return css`
            :host {
                display: inline-block;
            }
            * {
                box-sizing: border-box;
            }
            .card {
                max-width: 550px;
                height: 229px;
                box-shadow: none;
                transition: .3s transform cubic-bezier(.155,1.105,.295,1.12),
                            .3s box-shadow,
                            .3s -webkit-transform cubic-bezier(.155,1.105,.295,1.12);
                border-radius: 4px;
                border: 1px solid #ccc;
                background-color: #fff;
                display: flex;
                position: relative;
            }
            :host([highlighted]) {
                box-shadow: 10px 5px 5px rgba(140,180,35,.5);
            }
            .card .left {
                width: 100%;
                position: relative;
            }
            .card .right {
                flex: 0 1 160px;
                align: right;
                border-left: 1px solid #ccc;
                margin-left: auto;
            }
            .card .top {
                display: flex;
                width: 100%;
                // height: 45px;
                padding: 14px 0px 0px 20px;
            }
            .top span {
                margin-left: auto;
                float: right;
                margin-right: 18px;
                margin-top: 8px;
                height: 12px;
            }
            .card .center {
                width: 100%;
                height: 135px;
                padding: 5px 36px 10px 20px;
                background-repeat: no-repeat;
                background-position: right;
                background-size: 80px;
            }
            .card:hover {
                box-shadow: 0 10px 20px rgba(0,0,0,.12), 0 4px 8px rgba(0,0,0,.06);
            }
            .center img {
              top: 20px;
              right: 15px;
              max-height: 120px;
            }
            .top .title {
            }
            h2 {
                float: left;
                font-weight: 600;
                margin: 0;
                color: #035ba0;
                cursor: pointer;
            }
            .software {
                display: flex;
                align-items: center;
                margin-top: 5px;
                cursor: pointer;
            }
            .software img {
                max-width: 40px;
                max-height: 40px;
            }
            .software span {
                padding-left: 10px;
            }
            .description {
                display: -webkit-box;
                max-height: 70px;
                max-width: 330px;
                width: auto;
                overflow: hidden;
                text-overflow: ellipsis;
                font-size: 0.8em;
                padding: 8px 5px 0px 0px;
                -webkit-line-clamp: 4;
                -webkit-box-orient: vertical;
            }
            .card .bottom {
                display: flex;
                position: absolute;
                top: 180px;
                align-items: center;
                padding-left: 20px;
                padding-right: 0;
                height: 44px;
                width: 100%;
                border-top: 1px solid #ccc;
            }
            .right span {
                font-weight: 400;
                margin-right: 4px;
                font-size: 0.8em;
                font-style: italic;
            }
            .bottom img {
                max-width: 180px;
                max-height: 30px;
            }
            .bottom .use-button {
                flex: 1 1 auto;
                text-align: end;
            }
            .bottom a {
                text-decoration: none;
                color: #8CB423;
                padding: 5px;
                border-radius: 4px;
                font-size: 0.82em;
                font-weight: 200;
            }
            a mwc-icon {
                --mdc-icon-size: 0.82em;
            }
            .bottom a:hover {
                background-color: rgba(140,180,35,.2);
            }
            .bottom mwc-icon-button {
                color: #333;
            }
            .bottom mwc-icon-button.pulsating {
                animation: msPulseEffect 1s 1s 3;
            }
            .bottom img {
                padding-left: 5px;
                cursor: pointer;
            }
            .right {
                display: none;
            }
            .right .use-button {
                flex: 1 1 auto;
                cursor: pointer;
            }
            .right a {
                text-decoration: none;
                color: #8CB423;
                padding: 5px;
                border-radius: 4px;
                font-size: 0.82em;
                font-weight: 200;
            }
            .right .tags {
                padding: 10px 10px 10px 10px;
                height: 180px;
            }
            .right .buttons {
                height: 44px;
            }
            a mwc-icon {
                --mdc-icon-size: 0.82em;
            }
            .right a:hover {
                background-color: rgba(140,180,35,.2);
            }
            .right mwc-icon-button {
                color: #333;
            }
            a.pulsating {
                animation: msPulseEffect 1s 1s 3;
            }
            @keyframes msPulseEffect {
              0% {
                  box-shadow: 0 0 0 0px rgba(0, 0, 0, 0.5);
                }
              100% {
                  box-shadow: 0 0 0 15px rgba(0, 0, 0, 0);
                }
            }
            .greendot {
                height: 12px;
                width: 12px;
                background-color: #8CB423;
                border-radius: 50%;
              }
            .greydot {
                height: 12px;
                width: 12px;
                background-color: #D3D3D3;
                border-radius: 50%;
            }
            .bluebox {
                margin: 2px 2px 2px 2px;
                padding-left: 5px;
                padding-right: 5px;
                background: #035ba0;
                border: 2px solid #035ba0;
                font-size: 0.82em;
                float: left;
                color: white;
                border-radius: 10px;
                height: 20px;
                cursor: pointer;
               }
            .whitebox {
                margin: 2px 2px 2px 2px;
                padding-left: 5px;
                padding-right: 5px;
                background: white;
                border: 1px solid #035ba0;
                font-size: 0.82em;
                float: left;
                color: #035ba0;
                border-radius: 10px;
                #height: 20px;
                max-width:100px;
                overflow-wrap: break-word;
                cursor: pointer;
                }
            @media screen and (min-width: 600px) {
                .right {
                    display: inline;
                }
                .bottom .use-button, .bottom mwc-icon-button {
                    visibility: hidden;
                }
            }
        `;
    }
    render() {
        const noPoster = "data:image/svg+xml,%3Csvg%20width%3D%22344%22%20height" +
            "%3D%22194%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%" +
            "22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22" +
            "%3E%3Cdefs%3E%3Cpath%20id%3D%22a%22%20d%3D%22M-1%200h344v194H-1z%" +
            "22%2F%3E%3C%2Fdefs%3E%3Cg%20transform%3D%22translate(1)%22%20fill" +
            "%3D%22none%22%20fill-rule%3D%22evenodd%22%3E%3Cmask%20id%3D%22b%22" +
            "%20fill%3D%22%23fff%22%3E%3Cuse%20xlink%3Ahref%3D%22%23a%22%2F%3E%3" +
            "C%2Fmask%3E%3Cuse%20fill%3D%22%23BDBDBD%22%20xlink%3Ahref%3D%22%23a" +
            "%22%2F%3E%3Cg%20mask%3D%22url(%23b)%22%3E%3Cpath%20d%3D%22M173.65%206" +
            "9.238L198.138%2027%20248%20112.878h-49.3c.008.348.011.697.011%201.0" +
            "46%200%2028.915-23.44%2052.356-52.355%2052.356C117.44%20166.28%2094%" +
            "20142.84%2094%20113.924c0-28.915%2023.44-52.355%2052.356-52.355%2010%" +
            "200%2019.347%202.804%2027.294%207.669zm0%200l-25.3%2043.64h50.35c-" +
            ".361-18.478-10.296-34.61-25.05-43.64z%22%20fill%3D%22%23757575%22%2" +
            "F%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E";

        // if (this.service.availability == undefined) { return; }
        return html`
            <div class="card">
                <div class="left">
                <div class="top">
                    <div class="title" qtitle="${this.service['name'] == null ?
                    `No name` : this.service.displayName}"><h2 class="title" @click="${this._serviceDetailsDispatch}">
                        ${this.service['name'] == null ?
                `No name` : this.service.displayName}
                    </h2></div>
                    ${(this.service.availability != undefined) ?
                        this.service.availability.status == 'Available' ?
                html`<span class="greendot" title="Status: ${this.service.availability.status}\nLast checked: ${this.service.availability.lastChecked}"/>` :
                html`<span class="greydot" title="Status: ${this.service.availability.status}\nError: ${this.service.availability.reason}\nLast checked: ${this.service.availability.lastChecked}"/>`: ""}
                </div>
                <div class="center">
                    <div class="software" @click="${this._softwareDispatch}">
                        <img src="data:image/svg+xml;base64,${this.service.img}"/>
                        <span>${this._returnString(this.service.displaySoftware)}<span>
                    </div>
                    <div class="description">
                        ${this.service.summary ?
                           this._returnString(this.service.summary) :
                           this._returnString(this.service.description)}
                    </div>
                </div>
                <div class="bottom">
                    <span>by:</span>
                    <img @click="${this._providerDispatch}" src="data:image/svg+xml;base64,${this.service.providerImg}"/>
                    <div class="use-button">
                        <a href="${this.service.entryPoint}"><mwc-icon>arrow_forward</mwc-icon> Go to service</a>
                    </div>
                    <mwc-icon-button icon="more_vert" id="opendetails" @click="${this._serviceDetailsDispatch}"></mwc-icon-button>
                </div>
                </div>
                <div class="right">
                    <div class="tags">
                        ${this.service.tags.map(tag =>
                            html`<div class="whitebox" @click=${this._keywordDispatch}>${tag}</div>`)}
                    </div>
                    <div class="buttons">
                        <div class="use-button">
                            <a id="opendetailsBig" @click="${this._serviceDetailsDispatch}"><mwc-icon>more_vert</mwc-icon> Description</a>
                        </div>
                        <div class="use-button">
                            <a href="${this.service.entryPoint}"><mwc-icon>arrow_forward</mwc-icon> Go to service</a>
                        </div>
                    </div>
                </div>
            </div>
        `;
    }
    firstUpdated() {
        super.firstUpdated();
        if (this.shadowRoot.host.hasAttribute("highlighted")) {
            this.scrollIntoView({
                behavior: "smooth",
                block: "start",
                inline: "nearest"
              });
        }
        if (this.service.foundDescription) {
            this.shadowRoot.getElementById('opendetails').classList.add('pulsating');
            this.shadowRoot.getElementById('opendetailsBig').classList.add('pulsating');
        }
    }

    _serviceDetailsDispatch() {
        this.dispatchEvent(new CustomEvent('helmholtz-cloud-service-metadata', {
            detail: { message: this.service }, bubbles: true, composed: true
        }))
    }
    _softwareDispatch(e) {
        this.dispatchEvent(new CustomEvent('helmholtz-cloud-service-filter-software', {
            detail: { message: this.service.softwareList }, bubbles: true, composed: true
        }))
    }
    _providerDispatch(e) {
        this.dispatchEvent(new CustomEvent('helmholtz-cloud-service-filter-provider', {
            detail: { message: this.service.serviceProvider.abbreviation }, bubbles: true, composed: true
        }))
    }
    _keywordDispatch(e) {
        var originalElement = e.srcElement || e.originalTarget;
        var keyword = originalElement.innerText;
        this.dispatchEvent(new CustomEvent('helmholtz-cloud-service-filter-keywords', {
            detail: { message: keyword }, bubbles: true, composed: true
        }))
    }
    _returnString(str) {
        return document.createRange().createContextualFragment(`${str}`);
    }
}

customElements.define('service-card', ServiceCard);