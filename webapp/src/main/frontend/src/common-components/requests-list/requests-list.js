import { LitElement, html, css } from 'lit-element';

import '../../common-components/request-card/request-card';

class RequestsList extends LitElement
{
    constructor()
    {
        super();
        this.requests = [];
    }

    static get properties()
    {
        return {
            requests: {
                type: Array
            }
        };
    }
    static get styles()
    {
        return css`
            :host {
                display: block;
                width: 100%;
                height: 100%;
            }
            * {
                box-sizing: border-box;
            }
            .container {
                display: grid;
                padding-bottom: 20px;
                width: 100%;
                grid-template-columns: repeat(auto-fill, minmax(350px, 1fr));
                grid-gap: 2em;
                gap: 2em;
            }
        `;
    }
    render()
    {
        if (this.requests == null) { return; }
        return html`
            <div class="container">
                ${this.requests.map(request =>
                    html`<request-card  .request=${request}></request-card>`)}
            </div>
        `;
    }
}
customElements.define('requests-list', RequestsList)