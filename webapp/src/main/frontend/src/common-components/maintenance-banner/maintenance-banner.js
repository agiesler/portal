import { LitElement, html, css } from 'lit';

import '@material/mwc-button';

class MaintenanceBanner extends LitElement
{
    constructor()
    {
        super();
    }
    static get properties()
    {
        return {
            message: {
                type: String,
            },
            page: {
                type: String,
            }
        };
    }
    static get styles()
    {
        return css`
            :host {
                width: 100%;
                display: block;
                box-sizing: border-box;
            }
            * {
                box-sizing: border-box;
            }
            .container {
                display: flex;
            }
            .message {
                text-align: center;
                width: 100%;
                padding-top: 12px;
            }
            .button {
                margin-left: auto;
            }
            mwc-button {
                --mdc-theme-primary: #005aa0;
            }

        `;
    }
    render()
    {
        return html`
            <div class="container">
                <div class="message">
                ${this._returnString(this.message)}
                </div>
                <div class="button">
                <mwc-icon-button icon="close" id="opendetails" @click="${this._dismiss}"></mwc-icon-button>
                </div>
            </div>
        `;
    }
    _dismiss()
    {
        this.dispatchEvent(new CustomEvent(`helmholtz-cloud-dismiss-maintenance-${this.page}`, {
            detail: {dismiss: true}, bubbles: true, composed: true}));
    }
    _returnString(str)
    {
        return document.createRange().createContextualFragment(`${ str }`);
    }
}
customElements.define('maintenance-banner', MaintenanceBanner);