import { LitElement, html, css } from 'lit';

import '../../common-components/service-card/service-card';

class ServicesList extends LitElement
{
    constructor()
    {
        super();
        this.services = [];
    }

    static get properties()
    {
        return {
            services: {
                type: Array
            }
        };
    }
    static get styles()
    {
        return css`
            :host {
                display: block;
                width: 100%;
                height: 100%;
            }
            * {
                box-sizing: border-box;
            }
            .container {
                display: grid;
                padding-bottom: 20px;
                width: 100%;
                grid-template-columns: repeat(auto-fill, minmax(min(400px, 100%), 1fr));
                grid-gap: 2em;
                gap: 2em;
            }
            service-card {
                max-width: 600px;
            }
        `;
    }
    render()
    {
        if (this.services == null) { return; }
        return html`
            <div class="container">
                ${this.services.map(service =>
                    service['highlighted'] != undefined ?
                    html`<service-card highlighted .service=${service}></service-card>` :
                    html`<service-card  .service=${service}></service-card>`)}
            </div>
        `;
    }
}
customElements.define('services-list', ServicesList)