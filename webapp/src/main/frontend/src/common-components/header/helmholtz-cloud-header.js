/* eslint class-methods-use-this: ["error", { "exceptMethods": ["_logout", "_goToHomepage"] }] */
import { LitElement, html, css } from 'lit';

import '@material/mwc-icon-button';
import 'js-cookie';

class HelmholtzCloudHeader extends LitElement
{
    static get properties()
    {
        return {
            backgroundColor: {
                type: Boolean,
                attribute: true
            },
            principal: {
                type: Object
            }
        };
    }

    static get styles()
    {
        return css`
            :host {
                display: flex;
                width: 100%;
                align-items: center;
                box-sizing: border-box;
                color: #005aa0;
                background-color: #fff;
                height: 50px;
            }
            :host([backgroundcolor]) {
                background-color: #005aa0;
                color: #fff;
            }
            .logo {
                display: flex;
                justify-content: center;
                flex: 1 1 auto;
            }
            .logo .project-name {
                display: none;
            }
            .logo img {
                height: 24px;
                width: 250px;
                cursor: pointer;
            }
            .top-nav {
                display: none;
            }
            .dropdown {
                float: right;
                overflow: hidden;
                padding-left: 1.5em;
                padding-right: 1.5em;
            }
            .dropdown-top {
                display: flex;
                flex-direction: row;
                justify-content: center;
                align-items: center;
                padding-top: 5px;
                padding-bottom: 8px;
            }
            .dropdown .dropbtn {
                font-size: 16px;
                border: none;
                outline: none;
                color: #005aa0;
                background-color: inherit;
                font-family: inherit;
                margin: 0;
            }
            .dropdown-content {
                display: none;
                position: absolute;
                background-color: #fff;
                min-width: 160px;
                box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
                z-index: 1;
                right: 0;
            }
            .dropdown-content a {
                float: none;
                color: black;
                padding: 12px 16px;
                text-decoration: none;
                display: block;
                text-align: right;
            }
            .dropdown:hover {
                background-color: #eee;
                color: white;
            }
            .dropdown-content a:hover {
                background-color: #ddd;
                color: #005aa0;
            }
            .dropdown:hover .dropdown-content {
                display: block;
            }
            .arrow-down:hover .dropdown-content {
                display: block;
            }
            .circle {
                width: 36px;
                line-height: 0;
                position: relative;
                border-radius: 50%;
                color: #fff;
                background: #005aa0;
            }
            .circle::after {
                content: "";
                display: block;
                padding-bottom: 100%;
              }
            .circle-text {
                position: absolute;
                bottom: 45%;
                width: 100%;
                text-align: center;
            }
            .arrow-down {
                width: 0;
                height: 0;
                border-left: 5px solid transparent;
                border-right: 5px solid transparent;
                border-top: 5px solid #005aa0;
            }
            :host([backgroundcolor]) .circle {
                color: #005aa0;
                background: #fff;
            }
            :host([backgroundcolor]) .dropdown:hover .circle {
                color: #fff;
                background: #005aa0;
            }
            :host([backgroundcolor]) .dropdown:hover .arrow-down {
                border-top: 5px solid #005aa0;
            }
            :host([backgroundcolor]) .dropdown-content a{
                background-color: white;
                color:  #005aa0;
            }
            :host([backgroundcolor]) .dropdown-content a:hover{
                background-color: #ddd;
                color:  #005aa0;
            }
            :host([backgroundcolor]) .arrow-down {
                border-top: 5px solid #fff;
            }
            @media screen and (min-width: 700px) {
                :host([backgroundcolor]) a {
                    color: #fff;
                }
                :host a {
                    color: #005aa0;
                }
                .menu {
                    display: none;
                }
                .top-nav {
                    display: flex;
                    height: 50px;
                    box-sizing: border-box;
                    font-weight: 300;
                }
                .top-nav a {
                    display: flex;
                    text-decoration: none;
                    padding: 1.5em;
                    align-items: center;
                }
                .logo {
                    display: block;
                    padding-left: 2%;
                }
                .logo img {
                    height: 24px;
                }
            }
        `;
    }

    render()
    {
        return html`
            <div class="menu">
                <mwc-icon-button
                    icon="menu"
                    slot="navigationIcon"
                    @click="${this._toggleDrawer}"></mwc-icon-button>
            </div>
            <div class="logo">
                <img @click="${this._goToHomepage}"
                     src=${this.backgroundColor ? 
                        `media/i/helmholtz-cloud_white_font.svg` : 
                        `media/i/helmholtz-cloud_blue_font.svg`}>
                <div class="project-name">Helmholtz Cloud</div>
            </div>
            <nav class="top-nav">
                <a href="https://hifis.net/team">Team</a>
                <a href="https://hifis.net/news">News</a>
                <a href="https://hifis.net/contact">Helpdesk</a>
                <a href="https://hifis.net/services/cloud/Helmholtz_cloud">About</a>
                ${sessionStorage.getItem('auth_status') === 'authenticated' ?
                    this.principal ?
                    html`<div class="dropdown">
                    <div class="dropdown-top">
                    <div class="arrow-down"></div>
                    <button class="dropbtn"><div class="circle">
                    <div class="circle-text">${this.principal.attributes.given_name.charAt(0)}${this.principal.attributes.family_name.charAt(0)}</div></div>
                    <i class="fa fa-caret-down"></i>
                    </button>
                    </div>
                    <div class="dropdown-content">
                    <a href="/aboutme">${this.principal.attributes.name}</a>
                    <a href="/request_resource">Request Resources</a>
                    <a href="/resources">My Resources</a>
                    <a href="javascript:void(0);" @click=${this._logout}>Sign out</a>
                    </div>
                </div>`: "" :
                    html`<a href="${window.location.origin}${cp_prefix}oauth2/authorization/unity">Sign in</a>`
                }
            </nav>
        `;
    }

    firstUpdated() {
        if (sessionStorage.getItem('auth_status') === 'authenticated') {
            const headers = new Headers({
                "Accept": "application/json"
            });
            var url = `${window.location.origin}${cp_prefix}principal`
            fetch(url, { headers: headers })
            .then((response) => {
                if (response.status !== 200) {
                    throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
                }
                return response.json();
            })
            .then((principal) => {
                this.principal = principal;
            });
        }
    }
    _toggleDrawer()
    {
        this.dispatchEvent(
            new CustomEvent('helmholtz-main-menu-toggle', {
                bubbles: true,
                composed: true,
            })
        );
    }

    _logout()
    {
        fetch(`${window.location.origin}${cp_prefix}logout`, {
            method: 'POST',
            headers: {
                'X-XSRF-TOKEN': window.Cookies.get('XSRF-TOKEN'),
            },
        })
            .then(response => {
                if (response.status !== 200) {
                    throw new Error(
                        `Looks like there was a problem. Status Code: ${response.status}`
                    );
                }
                sessionStorage.clear();
                if (window.location.hash === '' && window.location.path === cp_prefix) {
                    window.location.reload();
                } else {
                    window.location.replace(cp_prefix);
                }
            })
            .catch(error => console.log(error));
    }

    _goToHomepage()
    {
        window.location.href = cp_prefix;
    }
}
customElements.define('helmholtz-cloud-header', HelmholtzCloudHeader);
