import { LitElement, html, css } from 'lit';

import '@material/mwc-button';

class FeedbackBanner extends LitElement {
    constructor() {
        super();
        this.message = "This is a preliminary version of the cloud portal. " +
            "We're working on making it even better. Come back soon to see what's new!";
        this.page = "landing";
    }
    static get properties() {
        return {
            message: {
                type: String,
            },
            page: {
                type: String,
            }
        };
    }
    static get styles() {
        return css`
            :host {
                width: 100%;
                display: block;
                padding: 10px;
                box-sizing: border-box;
            }
            * {
                box-sizing: border-box;
            }
            .container {
                display: grid;
                align-items: center;
            }
            #feedbackemail {
                width: 100%;
            }
            #feedbackbutton {
                width: 50%;
            }
            #feedbackcomment {
                width: 100%;
            }
            .information {
                flex: 1 1 auto;
            }
            .button {
                display: flex;
                width: 100%
            }
            .flex {
                flex: 1 1 auto;
            }
            input, textarea {
                font-size: 0.85em;
                font-family: inherit;
            }
            mwc-button {
                --mdc-theme-primary: #005aa0;
            }
            @media screen and (min-width: 700px) {
                .container {
                    width: 500px;
                    margin: auto;
                }
            }
            .submit {
                display: flex;
                height: 100%;
                padding-top: 10px;
            }
            #msgfield {
                width: 100%;
                padding-top: 10px;
                height: 20px;
            }
            #spincontainer {
                width: 50%;
                height: 1px;
                padding-left: 10px;
            }
            .spin {
                display: inline-block;
                width: 20px;
                height: 20px;
                border: 3px solid rgba(255, 255, 255, .3);
                border-radius: 50%;
                border-top-color: #005aa0;
                animation: spin 1s ease-in-out infinite;
                -webkit-animation: spin 1s ease-in-out infinite;
            }
            @keyframes spin {
                to {
                  -webkit-transform: rotate(360deg);
                }
            }
            @-webkit-keyframes spin {
                to {
                  -webkit-transform: rotate(360deg);
                }
            }
        `;
    }
    render() {
        var placeholderMsg = "You couldn't find what you were looking for? Or something didn't work the way you expected? " +
            " - Please let us now so we can improve.";
        var placeholderEmail = "Email (optional): If you provide your email, we will get in touch with you.";
        return html`
            <div class="container">
                <h3>Feedback</h3>
                <form id="feedbackform">
                <textarea name="comment" rows="4" form="feedbackform" id="feedbackcomment" placeholder=${placeholderMsg}></textarea>
                <input type="email" placeholder=${placeholderEmail} id="feedbackemail">
                </form>
                <div class="submit">
                <button type="button" form="feedbackform" id="feedbackbutton" @click=${this._handleFeedback}>Send feedback</button>
                <div id="spincontainer"></div>
                </div>
                <div id="msgfield"></div>
                <div class="button">
                    <div class="flex"></div><mwc-button @click="${this._dismiss}" label="Close"></mwc-button>
                </div>
            </div>
        `;
    }
    _validateEmail(email) {
        const res = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return res.test(String(email).toLowerCase());
      }
    _handleFeedback(e) {
        this.shadowRoot.getElementById("msgfield").innerHTML = "";
        var comment = this.shadowRoot.getElementById("feedbackcomment").value;
        var email = this.shadowRoot.getElementById("feedbackemail").value;

        var body = {'comment': comment};
        if (comment == "") {
            this.shadowRoot.getElementById("msgfield").innerHTML = "<span style='color:red'>Please enter a comment</span>";
            return;
        }
        if (email != "") {
            if (!this._validateEmail(email)) {
                this.shadowRoot.getElementById("msgfield").innerHTML = "<span style='color:red'>Please enter a valid email address</span>";
                return;
            }
            body['fromAddress'] = email;
        }
        this.shadowRoot.getElementById("feedbackbutton").disabled = true;
        this.shadowRoot.getElementById("spincontainer").innerHTML = "<div class='spin'></div>";

        const xsrfCookie = document.cookie
            .split('; ')
            .find(row => row.startsWith('XSRF-TOKEN='))
            .split('=')[1];
        const options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-XSRF-TOKEN': xsrfCookie
            },
            body: JSON.stringify(body)
        }
        fetch(`${window.location.origin}${cp_prefix}sendemail`, options)
            .then((response) => {
                console.log(response);
                this.shadowRoot.getElementById("spincontainer").innerHTML = "";
                if (response.status !== 200) {
                    this.shadowRoot.getElementById("msgfield").innerHTML = "<span style='color:red'>An error occured. Your feedback cannot be sent.</span>";
                    throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
                }
                this.shadowRoot.getElementById("msgfield").innerHTML = "<span style='color:green'>The message has been sent. Thanks for your feedback!</span>";
            });

    }
    _dismiss() {
        this.dispatchEvent(new CustomEvent('helmholtz-cloud-dismiss-feedback', {
            detail: { dismiss: true }, bubbles: true, composed: true
        }));
    }
    _returnString(str) {
        return document.createRange().createContextualFragment(`${str}`);
    }
}
customElements.define('feedback-banner', FeedbackBanner);