import { LitElement, html, css } from 'lit-element';

import '@material/mwc-icon';
import '@material/mwc-icon-button';

class RequestCard extends LitElement
{
    constructor()
    {
        super();
    }
    static get properties()
    {
        return {
            request: {
                type: Object
            }
        };
    }
    static get styles()
    {
        return css`
            :host {
                display: inline-block;
            }
            * {
                box-sizing: border-box;
            }
            .card {
                width: 100%;
                height: 229px;
                box-shadow: none;
                transition: .3s transform cubic-bezier(.155,1.105,.295,1.12),
                            .3s box-shadow,
                            .3s -webkit-transform cubic-bezier(.155,1.105,.295,1.12);
                border-radius: 4px;
                border: 1px solid #ccc;
                background-color: #fff;
            }
            .card .top {
                width: 100%;
                height: 180px;
                padding: 14px 80px 10px 36px;
                background-repeat: no-repeat;
                background-position: right;
                background-size: 80px;
            }
            .card:hover {
                box-shadow: 0 10px 20px rgba(0,0,0,.12), 0 4px 8px rgba(0,0,0,.06);
            }
            .top img {
              top: 20px;
              right: 15px;
              max-height: 120px;
            }
            h2 {
                font-weight: 600;
                margin: 0;
                color: #035ba0;
                cursor: pointer;
            }
            .specification {
                display: flex;
                align-items: center;
                margin-top: 5px;
            }
            .software img {
                max-width: 80px;
                max-height: 40px;
            }
            .software span {
                padding-left: 10px;
            }
            .description {
                display: -webkit-box;
                max-height: 70px;
                max-width: 330px;
                width: auto;
                overflow: hidden;
                text-overflow: ellipsis;
                font-size: 0.8em;
                padding: 8px 5px 0px 0px;
                -webkit-line-clamp: 4;
                -webkit-box-orient: vertical;
            }
            .card .bottom {
                display: flex;
                align-items: center;
                padding-left: 20px;
                padding-right: 0;
                height: 44px;
                width: 100%;
                border-top: 1px solid #ccc;
            }
            .bottom span {
                font-weight: 400;
                margin-right: 4px;
                font-size: 0.8em;
                font-style: italic;
            }
            .bottom img {
                max-width: 70px;
                max-height: 35px;
            }
            .bottom .use-button {
                flex: 1 1 auto;
                text-align: end;
            }
            .bottom a {
                text-decoration: none;
                color: #009688;
                padding: 5px;
                border-radius: 4px;
                font-size: 0.82em;
                font-weight: 200;
            }
            a mwc-icon {
                --mdc-icon-size: 0.82em;
            }
            .bottom a:hover {
                background-color: #e5f4f3;
            }
            .bottom mwc-icon-button {
                color: #333;
            }
        `;
    }
    render()
    {
        this.request.createdDate = new Date(this.request.createdDate).toLocaleDateString('de-de', { weekday:"long", year:"numeric", month:"short", day:"numeric"});
        return html`
            <div class="card">
                <div class="top">
                    <h2 class="title">
                        ${this.request['serviceName']}
                    </h2>
                    <div>
                        <div><b>Created:</b> ${this.request.createdDate}</div>
                        <div><b>Status:</b> ${this.request.status}</div>
                        ${this.request.errorDescription != null ?
                            html`<div><b>Error Message:</b> ${this.request.errorDescription}</div>` : ``}
                    </div>
                </div>
            </div>
        `;
    }

    _returnString(str)
    {
        return document.createRange().createContextualFragment(`${ str }`);
    }
}
customElements.define('request-card', RequestCard);