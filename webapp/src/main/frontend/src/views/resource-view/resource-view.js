import { LitElement, html, css } from 'lit-element';

import '@material/mwc-icon-button';
import '@material/mwc-icon';
import '@material/mwc-button';

import '../../common-components/header/helmholtz-cloud-header.js';
import '../../common-components/footer/helmholtz-cloud-footer.js';

class ResourceView extends LitElement {
    constructor() {
        super();
        const headers = new Headers({
            "Accept": "application/json"
        });
        this.resourceTypeMapping = {
            "GroupStorageResourceSpecV1": "Group Folder",
            "ComputeResourceSpecV1": "Computing Resource"
        }

        this.targetEntityMapping = {
            "UserSpecV1": "User",
            "VOListSpecV1": "VO List"
        }
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        /* check if any of the search/filter/sort options has been provided in the url params and apply them */
        const id = urlParams.get("id");
        fetch(`${window.location.origin}${cp_prefix}api/v0/resource/${id}`, { headers: headers })
            .then((response) => {
                if (response.status !== 200) {
                    throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
                }
                return response.json();
            })
            .then(resource => {
                this.resource = resource;
            })
            .catch(e => console.log(e));
        fetch(`${window.location.origin}${cp_prefix}api/v0/services/idMapping`, { headers: headers })
            .then((response) => {
                if (response.status !== 200) {
                    throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
                }
                return response.json();
            })
            .then((mapping) => {
                this.serviceMapping = {};
                mapping.content.forEach(element => {
                    this.serviceMapping[element['uuid']] = element['displayName'];
                });
            })
    }
    static get properties() {
        return {
            resource: {
                type: Object
            },
            serviceMapping: {
                type: Object
            },
            resourceTypeMapping: {
                type: Object
            },
            targetEntityMapping: {
                type: Object
            }
        };
    }
    static get styles() {
        return css`
            :host {
                width: 100%;
                height: 100%;
                overflow: hidden;
                -ms-overflow-style: none;  /* IE and Edge */
                scrollbar-width: none;  /* Firefox */
            }
            header,
            main,
            footer {
                width: 100%;
            }
            header {
                position: fixed;
                top: 0;
                z-index: 100;
                box-sizing: border-box;
                box-shadow: 0 -2px 8px rgb(0 0 0 / 9%),
                    0 4px 8px rgb(0 0 0 / 6%),
                    0 1px 2px rgb(0 0 0 / 30%),
                    0 2px 6px rgb(0 0 0 / 15%)
            }
            .header-border {
                border-bottom: 1px solid #274C69;
            }
            span {
                padding-bottom: 10px;
            }
            footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
            }
            * {
                box-sizing: border-box;
            }
            notification-banner {
                background-color: #e3e3e3;
            }
            main {
                display: flex;
                flex-direction: row;
                padding-top: 50px; /*must be equal to the height of the header*/
                width: 100%;
                height: 100vh;
                overflow: hidden;
                padding-bottom: 30px;
            }
            .content::-webkit-scrollbar {
                display: none;
            }
            .buttons {
                padding: 20px;
            }
            .content {
                display: flex;
                flex-direction: column;
                width: 100%;
                padding-left: 20px;
                padding-right: 20px;
                overflow-y: scroll;
                -ms-overflow-style: none;  /* IE and Edge */
                scrollbar-width: none;  /* Firefox */
                align-items: center;
            }
            table, th, td  {
                border: 1px solid #ddd;
                border-collapse: collapse;
                white-space: nowrap;
                width: 30%;
                padding: 15px;
            }
            tr:hover {background-color: rgba(140,180,35,.2);}
        `;
    }

    render() {
        if (this.resource == undefined) { return; };
        if (this.serviceMapping == undefined) { return; };
        return html`
        <header class="header-border">
        <helmholtz-cloud-header backgroundcolor></helmholtz-cloud-header>
        </header>
        <main>
            <div class="content">
                <h2>Resource Details</h2>
                ${this.resource ?
                html`
                <table>
                    <tr>
                        <td class="tg-0lax">Requester Id</td>
                        <td class="tg-0lax" colspan="2">${this.resource.requesterId}</td>
                    </tr>
                    <tr>
                        <td class="tg-0lax">Service</td>
                        <td class="tg-0lax" colspan="2">${this.serviceMapping[this.resource.serviceId]}</td>
                    </tr>
                    <tr>
                        <td class="tg-0lax">Status</td>
                        <td class="tg-0lax" colspan="2">${this.resource.status}</td>
                    </tr>
                    ${this.resource.errorMessage != undefined ?
                    html`
                    <tr>
                        <td class="tg-0lax">Error Message<br></td>
                        <td class="tg-0lax" colspan="2">${this.resource.errorMessage}</td>
                    </tr>
                    `: ''}
                    <tr>
                        <td class="tg-0lax">Resource Type</td>
                        <td class="tg-0lax" colspan="2">${this.resourceTypeMapping[this.resource.resourceType]}</td>
                    </tr>
                    <tr>
                        <td class="tg-0lax">External Resource Id</td>
                        <td class="tg-0lax" colspan="2">${this.resource.resourceId}</td>
                    </tr>
                    <tr>
                        <td class="tg-0lax">Creation Date</td>
                        <td class="tg-0lax" colspan="2">${new Date(this.resource.createdDate).toISOString()}</td>
                    </tr>
                    <tr>
                        <td class="tg-0lax">Last Updated</td>
                        <td class="tg-0lax" colspan="2">${new Date(this.resource.lastModifiedDate).toISOString()}</td>
                    </tr>
                    <tr>
                        <td class="tg-0lax" rowspan="2">Target Entity</td>
                        <td class="tg-0lax">Type</td>
                        <td class="tg-0lax">${this.targetEntityMapping[this.resource.targetEntity.type]}</td>
                    </tr>
                    ${this.resource.targetEntity.type == "VOListSpecV1" ?
                    html`
                    <tr>
                        <td class="tg-0lax">VOs</td>
                        <td class="tg-0lax">${this.resource.targetEntity.specification.vos.join(",")}</td>
                    </tr>` : ''}
                    ${this.resource.targetEntity.type == "UserSpecV1" ?
                    html`
                    <tr>
                        <td class="tg-0lax">User Id</td>
                        <td class="tg-0lax">${this.resource.targetEntity.specification.userId}</td>
                    </tr>` : ''}
                    ${this.resource.resourceType == "GroupStorageResourceSpecV1" ?
                    html`
                    <tr>
                        <td class="tg-0lax" rowspan="2">Spec</td>
                        <td class="tg-0lax">Name<br></td>
                        <td class="tg-0lax">${this.resource.spec.desiredName}</td>
                    </tr>
                    <tr>
                        <td class="tg-0lax">Quota</td>
                        <td class="tg-0lax">${this.resource.spec.quota.value}${this.resource.spec.quota.unit}</td>
                    </tr>
                    `: ''}
                    ${this.resource.resourceType == "ComputeResourceSpecV1" ?
                    html`
                    <tr>
                        <td class="tg-0lax" rowspan="3">Spec</td>
                        <td class="tg-0lax">CPU</td>
                        <td class="tg-0lax">${this.resource.spec.cpu.value} ${this.resource.spec.cpu.unit}</td>
                    </tr>
                    <tr>
                        <td class="tg-0lax">RAM</td>
                        <td class="tg-0lax">${this.resource.spec.ram.value} ${this.resource.spec.ram.unit}</td>
                    </tr>
                    <tr>
                        <td class="tg-0lax">Storage</td>
                        <td class="tg-0lax">${this.resource.spec.storage.value} ${this.resource.spec.storage.unit}</td>
                    </tr>
                    `: ''}
                </table>` : ''}
                <div class="buttons">
                    <mwc-button @click="${this._updateResource}" label="Update"></mwc-button>
                    <mwc-button @click="${this._deleteResource}" label="Delete"></mwc-button>
                </div>
            </div>
        </main>
        <footer>
            <helmholtz-cloud-footer minima></helmholtz-cloud-footer>
        </footer>
        `;
    }
}
customElements.define('resource-view', ResourceView);