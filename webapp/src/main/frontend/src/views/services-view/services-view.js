//import { LitElement, html, css } from 'lit-element';
import { LitElement, html, css } from 'lit';

import '@material/mwc-icon-button';
import '@material/mwc-icon';

import '../../common-components/services-list/services-list.js';
import '../../common-components/service-profile/service-profile.js';
import '../../common-components/notification-banner/notification-banner.js';
import '../../common-components/maintenance-banner/maintenance-banner.js';
import '../../common-components/feedback-banner/feedback-banner.js';
import '../../common-components/header/helmholtz-cloud-header.js';
import '../../common-components/footer/helmholtz-cloud-footer.js';
import '../../common-components/filter-bar/filter-big.js';
import '../../common-components/filter-bar/filter-small.js';

class ServicesView extends LitElement {
    constructor() {
        super();
        this.apiEndpoint = `http://localhost:8090/api/v0`;
        if (typeof CONFIG != 'undefined') {
            if ("api.endpoint" in CONFIG) {
                this.apiEndpoint = CONFIG['api.endpoint'];
            }
        }
        this.addEventListener('helmholtz-cloud-service-metadata', this._showDetailsPanel);
        this.addEventListener('helmholtz-cloud-service-filter-keywords', this._filterKeywords);
        this.addEventListener('helmholtz-cloud-service-filter-software', this._filterSoftware);
        this.addEventListener('helmholtz-cloud-service-filter-provider', this._filterProvider);
        this.addEventListener('helmholtz-open-feedback-banner', this._openfeedback);
        this.addEventListener(
            'helmholtz-cloud-dismiss-notification-services',
            this._notificationDismissalListener, true);
        this.addEventListener(
            'helmholtz-cloud-dismiss-maintenance-services',
            this._maintenanceDismissalListener, true);
        this.addEventListener(
            'helmholtz-cloud-dismiss-feedback',
            this._feedbackDismissalListener, true);
        this.addEventListener('helmholtz-cloud-apply-filters', this._receiveFilters);
        this.addEventListener('helmholtz-cloud-reset-filters', this._resetFilters);
        this.addEventListener('helmholtz-cloud-close-filters', this._closeFilters);
        const headers = new Headers({
            "Accept": "application/json"
        });
        this.serviceDetails = "";
        this.selectedProvider = null;

        this.searchInput = "";
        this.softwareList = [];
        this.keywordList = [];
        this.providerList = [];
        this.selectedKeywords = [];
        this.selectedProviders = [];
        this.selectedSoftware = [];
        this.defaultSorting = 'software';
        this.serviceSort = this.defaultSorting;

        var url = `${this.apiEndpoint}/services?sort=software.asc&size=100`

        fetch(url, { headers: headers })
            .then((response) => {
                if (response.status !== 200) {
                    throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
                }
                return response.json();
            })
            .then(this._setup.bind(this))
            .catch(e => console.log(e));

        var maintenanceUrl = `${this.apiEndpoint}/maintenanceMessage`

        fetch(maintenanceUrl, { headers: headers })
            .then((response) => {
                if (response.status !== 200) {
                    throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
                }
                return response.json();
            })
            .then(data => {
                this.maintenanceMessage = data.message;
            })
            .catch(e => console.log("no maintenance message available"));
    }
    static get properties() {
        return {
            services: {
                type: Array
            },
            serviceName: {
                type: String
            },
            serviceContact: {
                type: String
            },
            maintenanceMessage: {
                type: String
            }
        };
    }
    static get styles() {
        return css`
            :host {
                width: 100%;
                height: 100%;
                overflow: hidden;
            }
            header,
            main,
            footer {
                width: 100%;
            }
            header {
                position: fixed;
                top: 0;
                z-index: 100;
                box-sizing: border-box;
                box-shadow: 0 -2px 8px rgb(0 0 0 / 9%),
                    0 4px 8px rgb(0 0 0 / 6%),
                    0 1px 2px rgb(0 0 0 / 30%),
                    0 2px 6px rgb(0 0 0 / 15%)
            }
            input {
                color: black;
                font-family: inherit;
            }
            .header-border {
                border-bottom: 1px solid #274C69;
            }
            * {
                box-sizing: border-box;
            }
            notification-banner {
                background-color: #e3e3e3;
            }
            maintenance-banner {
                background-color: #ffcb0d;
            }
            feedback-banner {
                background-color: #e3e3e3;
            }
            .list, .details {
                height: 100%;
            }
            main {
                display: flex;
                flex-direction: row;
                padding-top: 50px; /*must be equal to the height of the header*/
                width: 100%;
                height: 100vh;
                overflow: hidden;
                padding-bottom: 30px;
            }
            /* Hide scrollbar for Chrome, Safari and Opera */
            .search::-webkit-scrollbar {
                display: none;
            }
            .topbar {
                display: flex;
                flex-direction: column;
                border-bottom: 1px solid #CCC;
            }
            #taglist {
                padding-left: 20px;
            }
            .feedback {
                padding-right: 10px;
                padding-top: 5px;
            }
            .content {
                display: flex;
                flex-direction: column;
                width: 100%;
            }
            /* Hide scrollbar for Chrome, Safari and Opera */
            .services::-webkit-scrollbar {
                display: none;
            }
            .services {
                display: flex;
                padding-top: 10px;
                padding-left: 20px;
                padding-right: 20px;
                flex-direction: row;
                width: 100%;
                height: 100%;
                overflow-y: scroll;
                -ms-overflow-style: none;  /* IE and Edge */
                scrollbar-width: none;  /* Firefox */
            }
            .details {
                overflow: hidden;
                background-color: #efefef;
                width: 0;
            }
            #details.show {
                width: 600px;
            }
            #content.show {
                width: calc(100% - 600px);
            }
            .details .buttons {
                max-height: 50px;
                height: 5%;
                display: flex;
            }
            .buttons div {
                flex: 1 1 auto;
                display: flex;
                align-items: center;
                padding: 20px;
                color: #ff5722;
            }
            .buttons div a {
                text-decoration: none;
                font-size: 0.85em;
                padding-right: 5px;
                color: #ff5722;
                font-weight: 300;
            }
            .buttons div mwc-icon {
                font-size: 1.21em;
            }
            #feedbackbutton {
                vertical-align: middle;
                color: #8CB423;
                --mdc-icon-size: 32px;
            }
            .details .details-content {
                background-color: white;
                margin: 20px;
                padding: 20px;
                border-radius: 3px;
                height: 90%;
                overflow-y: scroll;
            }
            footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
            }
            .hide {
                display: none;
            }
            @media screen and (max-width: 771px) {
                #content.show {
                    display: none;
                }
                #details.show {
                    width: 100vw;
                }
                filter-big {
                    display: none;
                }
            }
            @media screen and (min-width: 772px) {
                #content.show {
                    width: calc(100% - 390px);
                }
                #details.show {
                    width: 390px;
                }
                filter-small {
                    display: none;
                }
            }
            @media screen and (min-width: 1100px) {
                #content.show {
                    width: calc(100% - 600px);
                }
                #details.show {
                    width: 600px;
                }
            }
            .bluebox {
                margin: 2px 2px 2px 2px;
                padding-left: 5px;
                padding-right: 5px;
                background: #035ba0;
                border: 1px solid #035ba0;
                font-size: 0.82em;
                float: left;
                color: white;
                border-radius: 10px;
                height: 20px;
            }
            .whitebox {
                margin: 2px 2px 2px 2px;
                padding-left: 5px;
                padding-right: 5px;
                background: white;
                border: 1px solid #035ba0;
                font-size: 0.82em;
                float: left;
                color: #035ba0;
                border-radius: 10px;
                height: 20px;
            }
            .redbox {
                margin: 2px 2px 2px 2px;
                padding-left: 5px;
                padding-right: 5px;
                background: red;
                border: 2px solid red;
                font-size: 0.82em;
                float: left;
                color: white;
                border-radius: 10px;
                height: 20px;
            }
            #taglist mwc-icon {
                vertical-align: middle;
                padding-left: 6px;
                cursor: pointer;
                --mdc-icon-size: 0.82em;
            }
        `;
    }
    render() {
        const notification = window.localStorage.getItem('notification-services');
        const msg = "Disclaimer: All Service information provided here is for your information and convenience. " +
            "The respective service provider is responsible for providing the service and all required information " +
            "on usage conditions and regulations. <br>" +
            "<p><b>Not all services are accessible to all users.</b></p>";
        return html`
                ${notification === "dismiss" ?
                html`
                        <header class="header-border">
                            <helmholtz-cloud-header backgroundcolor></helmholtz-cloud-header>
                        </header>`:
                html`
                        <header>
                            <helmholtz-cloud-header backgroundcolor></helmholtz-cloud-header>
                            <notification-banner message=${msg} page="services"></notification-banner>
                        </header>`}
            </header>
            <main>
                <div id="content" class="content">
                    ${this.maintenanceMessage ?
                    html`
                    <maintenance-banner message=${this.maintenanceMessage} page="services"></maintenance-banner>` : ''}
                    <div class="topbar" id="topbar">
                        <!--<div class="feedback">
                            <mwc-icon-button icon="sms" @click="${this._openfeedback}" label="Feedback" id="feedbackbutton"></mwc-icon-button>
                        </div>-->
                    </div>
                    <div id="services" class="services">
                        <services-list id="serviceslist" .services=${this.services}></services-list>
                    </div>
                </div>
                <div id="details" class="details">
                    <div class=buttons>
                        <div>
                            <a href="mailto:${this.serviceContact}">
                                need help with ${this.serviceName}</a><mwc-icon>help</mwc-icon>
                        </div>
                        <mwc-icon-button icon="close" @click="${this._closeDetailsPanel}"></mwc-icon-button>
                    </div>
                    <div id="panel" class="details-content"></div>
                </div>
            </main>
            <footer>
                <helmholtz-cloud-footer minima></helmholtz-cloud-footer>
            </footer>
        `;
    }
    /* runs once after the first render and sets up the listeners for the different search/filter/sort inputs */
    firstUpdated() {
        super.firstUpdated();

        var feedbackBanner = document.createElement('feedback-banner');
        feedbackBanner.classList.add('hide');
        var header = this.shadowRoot.querySelector("header");

        var dialog = this.shadowRoot.getElementById("dialog");
        header.appendChild(feedbackBanner);
    }
    _setup(data) {
        this.services = data.content;

        /* converts the list of services to a comma-separated string
           that is displayed on the service card. */
        this.services.forEach(service => {
            service.providerName = service.serviceProvider.nameDE;
            var softwares = [];
            service.softwareList.forEach(software => {
                softwares.push(software.name);
            });
            service.displaySoftware = softwares.join(", ");
        });
        this.dispatchEvent(
            new CustomEvent('helmholtz-cloud-custom-element-status', {
                detail: { customElementName: 'service-list' },
                bubbles: true,
                composed: true
            }));

        this._loadDropdownOptions();

        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        /* check if any of the search/filter/sort options has been provided in the url params and apply them */
        const name = urlParams.get("search");
        const sort = urlParams.get("sort");
        const keywords = urlParams.get("keywords");
        const software = urlParams.get("software");
        const provider = urlParams.get("provider");

        this.newServices = this._deepCopy(this.services);
        if (name != null) {
            this.searchInput = name;
        }
        if (keywords != null) {
            this.selectedKeywords = keywords.split(',');
        }
        if (software != null) {
            this.selectedSoftware = software.split(',');
        }
        if (provider != null) {
            this.selectedProviders = provider.split(',');
        }
        if (sort != null) {
            this.serviceSort = sort;
        }

        /* create the filter-bars filled with the selected parameters
           from the url parameters */
        this._updateFilterBar();

        var imageurl = `${this.apiEndpoint}/images?size=100`;
        fetch(imageurl)
            .then((response) => {
                if (response.status !== 200) {
                    throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
                }
                return response.json();
            })
            .then(this._resolveImages.bind(this));

        var availurl = `${this.apiEndpoint}/availabilities?size=100`;
        fetch(availurl)
            .then((response) => {
                if (response.status !== 200) {
                    throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
                }
                return response.json();
            })
            .then(this._handleAvailability.bind(this));

        var serviceDetails = urlParams.get('serviceDetails');
        this.newServices.forEach(service => {
            if (service.name == serviceDetails || (service.altName != undefined && service.altName == serviceDetails)) {
                service['highlighted'] = true;
                this.dispatchEvent(new CustomEvent('helmholtz-cloud-service-metadata', {
                    detail: { message: service }, bubbles: true, composed: true
                }));
            }
        });
        var closeDetails = true;
        if (this.serviceDetails != null) {
            closeDetails = false;
        }

        var newList = document.createElement("services-list");
        newList.services = this.newServices;
        var servicesElement = this.shadowRoot.getElementById("services");
        servicesElement.innerHTML = "";
        servicesElement.appendChild(newList);
        this._applyFilters(closeDetails);
    }
    /* called when a keyword is selected from the service-card sidebar. Updates the
       selectedKeywords list, recreated the filter-bars and applies the filters. */
    _filterKeywords(e) {
        var keyword = e.detail.message;
        if (this.selectedKeywords.includes(keyword)) {
            return;
        }
        this.selectedKeywords.push(keyword);
        this._updateFilterBar();
        this._applyFilters();
    }
    /* called when a software is selected from the service-card. Updates the
       selectedSoftware list, recreated the filter-bars and applies the filters. */
    _filterSoftware(e) {
        var softwareList = e.detail.message;
        softwareList.forEach(software => {
            if (this.selectedSoftware.includes(software.name)) {
                return;
            }
            this.selectedSoftware.push(software.name);
        });
        this._updateFilterBar();
        this._applyFilters();
    }
    /* called when a provider is selected from the service-card. Updates the
       selectedProviders list, recreated the filter-bars and applies the filters. */
    _filterProvider(e) {
        var provider = e.detail.message;
        if (this.selectedProviders.includes(provider)) {
            return;
        }
        this.selectedProviders.push(provider);
        this._updateFilterBar();
        this._applyFilters();
    }
    /* remove a keyword bubble from the tag list and update the selectedKeywords list */
    _removeKeyword(e) {
        e.target.parentNode.parentNode.removeChild(e.target.parentNode);
        var keyword = e.target.attributes.tag.value;
        var index = this.selectedKeywords.indexOf(keyword);
        this.selectedKeywords.splice(index, 1);
        this._updateFilterBar();
        this._applyFilters();
    }
    /* remove a provider bubble from the tag list and update the selectedProviders list */
    _removeProvider(e) {
        e.target.parentNode.parentNode.removeChild(e.target.parentNode);
        var keyword = e.target.attributes.tag.value;
        var index = this.selectedProviders.indexOf(keyword);
        this.selectedProviders.splice(index, 1);
        this._updateFilterBar();
        this._applyFilters();
    }
    /* remove a software bubble from the tag list and update the selectedSoftware list */
    _removeSoftware(e) {
        e.target.parentNode.parentNode.removeChild(e.target.parentNode);
        var keyword = e.target.attributes.tag.value;
        var index = this.selectedSoftware.indexOf(keyword);
        this.selectedSoftware.splice(index, 1);
        this._updateFilterBar();
        this._applyFilters();
    }
    _removeSorting(e) {
        e.target.parentNode.parentNode.removeChild(e.target.parentNode);
        this.serviceSort = this.defaultSorting;
        this._updateFilterBar();
        this._applyFilters();
    }
    _resolveImages(data) {
        let imageMap = {};
        for (const image of data.content) {
            const key = image.uuid;
            if (!(key in imageMap)) {
                imageMap[key] = image.image;
            }
        }
        this.services.forEach(service => {
            service.img = imageMap[service.logoId];
            service.providerImg = imageMap[service.serviceProvider.logoId];
        });

        this.newServices.forEach(service => {
            service.img = imageMap[service.logoId];
            service.providerImg = imageMap[service.serviceProvider.logoId];
        });
        var newList = document.createElement("services-list");
        newList.services = this.newServices;

        var servicesElement = this.shadowRoot.getElementById("services");
        servicesElement.innerHTML = "";
        servicesElement.appendChild(newList);
    }
    /* matches the availability data to list of services adds a corresponding availability field to the service */
    _handleAvailability(data) {
        let availMap = {};
        for (const avail of data.content) {
            const key = avail.id;
            if (!(key in availMap)) {
                availMap[key] = avail;
            }
        }
        this.services.forEach(service => {
            if (!(service.uuid in availMap)) {
                return;
            }
            var avail = availMap[service.uuid];
            service.availability = {};
            var d = new Date(avail['lastUpdated']);
            service.availability['lastChecked'] = `${d.getDate()}.${d.getMonth() + 1}.${d.getFullYear()} ${d.getHours()}:${(d.getMinutes() < 10 ? '0' : '') + d.getMinutes()}`;
            if (avail['status'] == 200) {
                service.availability['status'] = "Available";
            } else if (avail['status'] < 0) {
                service.availability['status'] = "Not available";
                service.availability['reason'] = avail['errorMsg'];
            } else {
                service.availability['status'] = "Not available";
                service.availability['reason'] = HTTP_STATUS_CODES[`CODE_${avail['status']}`];
            }
        });
        this.newServices.forEach(service => {
            if (!(service.uuid in availMap)) {
                return;
            }
            var avail = availMap[service.uuid];
            service.availability = {};
            var d = new Date(avail['lastUpdated']);
            service.availability['lastChecked'] = `${d.getDate()}.${d.getMonth() + 1}.${d.getFullYear()} ${d.getHours()}:${(d.getMinutes() < 10 ? '0' : '') + d.getMinutes()}`;
            if (avail['status'] == 200) {
                service.availability['status'] = "Available";
            } else if (avail['status'] < 0) {
                service.availability['status'] = "Not available";
                service.availability['reason'] = avail['errorMsg'];
            } else {
                service.availability['status'] = "Not available";
                service.availability['reason'] = HTTP_STATUS_CODES[`CODE_${avail['status']}`];
            }
        });
        var newList = document.createElement("services-list");
        newList.services = this.newServices;

        var servicesElement = this.shadowRoot.getElementById("services");
        servicesElement.innerHTML = "";
        servicesElement.appendChild(newList);
    }
    _deepCopy(array) {
        var newArray = []
        array.forEach(item => {
            newArray.push(JSON.parse(JSON.stringify(item)));
        })
        return newArray;
    }
    /* checks if the different inputs are different from default and updates the url query accordingly */
    _updateSearchParams() {
        var newQuery = "";
        if (this.searchInput.length != 0) {
            newQuery += `&search=${this.searchInput}`;
        }
        if (this.selectedSoftware.length != 0) {
            newQuery += `&software=${this.selectedSoftware.join(',')}`;
        }
        if (this.selectedProviders.length != 0) {
            newQuery += `&provider=${this.selectedProviders.join(',')}`;
        }
        if (this.selectedKeywords.length != 0) {
            newQuery += `&keywords=${this.selectedKeywords.join(',')}`;
        }
        if (this.serviceSort != this.defaultSorting) {
            newQuery += `&sort=${this.serviceSort}`;
        }
        if (this.serviceDetails.length != 0) {
            newQuery += `&serviceDetails=${encodeURIComponent(this.serviceDetails)}`;
        }
        newQuery = "?" + newQuery.substring(1);

        if (newQuery == '?') newQuery = '';
        if (history.pushState) {
            var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + newQuery;
            window.history.pushState({ path: newurl }, '', newurl);
        }
    }
    /* helper methods to sort services */
    _compareObjects(object1, object2, key) {
        const obj1 = object1[key].toUpperCase()
        const obj2 = object2[key].toUpperCase()

        if (obj1 < obj2) {
            return -1
        }
        if (obj1 > obj2) {
            return 1
        }
        return 0
    }
    _highlightText(text, pattern) {
        const regex = new RegExp(pattern, 'gi');
        return text.replace(regex, "<span style='background-color: rgba(140,180,35,.2)'>$&</span>");
    }
    /* takes an array of services and checks the service name against the provided pattern */
    _findServicesByPattern(services, pattern) {
        var matchedServices = [];
        services.forEach(service => {
            var found = false;
            if (service.name.includes(pattern)) {
                found = true;
            }
            if (service.displaySoftware.toLowerCase().includes(pattern)) {
                found = true;
            }
            if (service.summary.toLowerCase().includes(pattern)) {
                found = true;
                service.summary = this._highlightText(service.summary, pattern);
            }
            if (service.serviceProvider.abbreviation.toLowerCase().includes(pattern)) {
                found = true;
            }
            if (service.serviceProvider.nameDE.toLowerCase().includes(pattern)) {
                found = true;
            }
            if (service.description.toLowerCase().includes(pattern)) {
                found = true;
                service.foundDescription = true;
            }
            if (found) {
                matchedServices.push(service);
            }
        });

        return matchedServices;
    }
    /* takes an array of services and checks the keywords against the given input.
       a service has to match all keywords to be selected */
    _findServicesByKeywords(services, keywords) {
        var matchedServices = [];
        services.forEach(service => {
            var matchedKeywords = [];
            keywords.forEach(keyword => {
                if (service.tags.includes(keyword)) {
                    matchedKeywords.push(keyword);
                }
            });
            if (keywords.length == matchedKeywords.length) {
                matchedServices.push(service);
            }
        });
        return matchedServices;
    }

    /* takes an array of services and checks the service provider against the given input.
       a service has to match any provider to be selected */
    _selectServicesByProvider(services, providers) {
        var matchedServices = [];
        services.forEach(service => {
            var found = false;
            providers.forEach(provider => {
                if (service.serviceProvider.abbreviation == provider) {
                    found = true;
                }
            });
            if (found) {
                matchedServices.push(service);
            }
        });
        return matchedServices;
    }
    /* takes an array of services and checks the service software against the given input.
       a service has to match any software to be selected */
    _selectServicesBySoftware(services, softwares) {
        var matchedServices = [];
        services.forEach(service => {
            var found = false;
            softwares.forEach(software => {
                service.softwareList.forEach(sft => {
                    if (sft.name == software) {
                        found = true;
                    }
                });
            });
            if (found) {
                matchedServices.push(service);
            }
        });
        return matchedServices;
    }
    /* goes through the service list add creates list of distinct elements for certain fields that are used
       in the dropdown menus */
    _loadDropdownOptions() {
        var softwareList = [];
        var providerList = [];
        var keywordList = [];

        this.services.forEach(service => {
            service.softwareList.forEach(software => {
                softwareList.push(software['name']);
            });
            providerList.push(service.serviceProvider.abbreviation);
            Array.prototype.push.apply(keywordList, service['tags']);
        });

        this.softwareList = new Set(softwareList.sort(function (a, b) {
            return a.toLowerCase().localeCompare(b.toLowerCase());
        }));
        this.providerList = new Set(providerList.sort());
        this.keywordList = new Set(keywordList.sort());
    }
    /* callback used to receive the filter values from the filter-bar components */
    _receiveFilters(e) {
        var filters = e.detail.filters;
        this.searchInput = filters.searchInput;
        this.selectedKeywords = filters.selectedKeywords;
        this.selectedProviders = filters.selectedProviders;
        this.selectedSoftware = filters.selectedSoftware;
        this.serviceSort = filters.serviceSort;
        this._applyFilters();
    }
    /* processes the received filter value and selects the matching services */
    _applyFilters(closeDetails=true) {
        if (closeDetails) {
            this._closeDetailsPanel();
        }
        this.newServices = this._deepCopy(this.services);

        this._updateSearchParams();
        var taglist = this.shadowRoot.getElementById("taglist");
        this._removeAllChildNodes(taglist);
        if (this.searchInput.length != 0) {
            this.newServices = this._findServicesByPattern(this.newServices, this.searchInput.toLowerCase());
        }
        if (this.selectedProviders.length != 0) {
            this.newServices = this._selectServicesByProvider(this.newServices, this.selectedProviders);
            // add a bubble with the selected value in the tag list.
            this.selectedProviders.forEach(keyword => {
                var taglist = this.shadowRoot.getElementById("taglist");
                var tagfilter = document.createElement("div");
                var closeButton = document.createElement("mwc-icon");
                closeButton.innerHTML = "close";
                closeButton.setAttribute("tag", keyword);
                closeButton.addEventListener("click", this._removeProvider.bind(this), true);
                tagfilter.className = "whitebox";
                tagfilter.innerHTML = "Provider: " + keyword;
                tagfilter.appendChild(closeButton);
                taglist.appendChild(tagfilter);
            })
        }
        if (this.selectedSoftware.length != 0) {
            this.newServices = this._selectServicesBySoftware(this.newServices, this.selectedSoftware);
            // add a bubble with the selected value in the tag list.
            this.selectedSoftware.forEach(keyword => {
                var taglist = this.shadowRoot.getElementById("taglist");
                var tagfilter = document.createElement("div");
                var closeButton = document.createElement("mwc-icon");
                closeButton.innerHTML = "close";
                closeButton.setAttribute("tag", keyword);
                closeButton.addEventListener("click", this._removeSoftware.bind(this), true);
                tagfilter.className = "whitebox";
                tagfilter.innerHTML = "Software: " + keyword;
                tagfilter.appendChild(closeButton);
                taglist.appendChild(tagfilter);
            })
        }

        if (this.selectedKeywords.length != 0) {
            this.newServices = this._findServicesByKeywords(this.newServices, this.selectedKeywords);
            // add a bubble with the selected value in the tag list.
            this.selectedKeywords.forEach(keyword => {
                var taglist = this.shadowRoot.getElementById("taglist");
                var tagfilter = document.createElement("div");
                var closeButton = document.createElement("mwc-icon");
                closeButton.innerHTML = "close";
                closeButton.setAttribute("tag", keyword);
                closeButton.addEventListener("click", this._removeKeyword.bind(this), true);
                tagfilter.className = "whitebox";
                tagfilter.innerHTML = keyword;
                tagfilter.appendChild(closeButton);
                taglist.appendChild(tagfilter);
            })
        }
        // add a bubble with the selected value in the tag list.
        if (this.serviceSort != this.defaultSorting) {
            var taglist = this.shadowRoot.getElementById("taglist");
            var tagfilter = document.createElement("div");
            var closeButton = document.createElement("mwc-icon");
            closeButton.innerHTML = "close";
            closeButton.setAttribute("tag", this.serviceSort);
            closeButton.addEventListener("click", this._removeSorting.bind(this), true);
            tagfilter.className = "whitebox";
            if (this.serviceSort == 'providerName') {
                tagfilter.innerHTML = "Sorting: Provider Name";
            }
            if (this.serviceSort == 'name') {
                tagfilter.innerHTML = "Sorting: Service Name";
            }
            tagfilter.appendChild(closeButton);
            taglist.appendChild(tagfilter);
        }

        // if at least one filter has been selected display a "reset all" button.
        if (this.searchInput.length != 0 || this.selectedKeywords.length != 0 || this.selectedProviders.length != 0 || this.selectedSoftware != 0 || this.serviceSort != this.defaultSorting) {
            var taglist = this.shadowRoot.getElementById("taglist");
            var tagfilter = document.createElement("div");
            var closeButton = document.createElement("mwc-icon");
            closeButton.innerHTML = "close";
            closeButton.addEventListener("click", this._resetFilters.bind(this), true);
            tagfilter.className = "bluebox";
            tagfilter.innerHTML = "Reset all filters";
            tagfilter.appendChild(closeButton);
            taglist.appendChild(tagfilter);
        }
        /* sort the services according to the selected parameter */
        this.newServices.sort((svc1, svc2) => {
            return this._compareObjects(svc1, svc2, this.serviceSort);
        })

        /* if the details panel for a service has been opened add the "highlighted" attribute
           to the service to that the corresponding service-card will be highlighted */
        this.newServices.forEach(service => {
            if (service.name == this.serviceDetails) {
                service['highlighted'] = true;
                this.dispatchEvent(new CustomEvent('helmholtz-cloud-service-metadata', {
                    detail: { message: service }, bubbles: true, composed: true
                }));
            }
        });
        var newList = document.createElement("services-list");
        newList.services = this.newServices;

        var servicesElement = this.shadowRoot.getElementById("services");
        servicesElement.innerHTML = "";
        servicesElement.appendChild(newList);

    }
    /* recreates the filter-bars with the updated selected parameters */
    _updateFilterBar() {
        var newFiltersBig = document.createElement("filter-big");
        var newFiltersSmall = document.createElement("filter-small");
        var topbar = this.shadowRoot.getElementById("topbar");
        newFiltersBig.keywordList = this.keywordList;
        newFiltersBig.selectedKeywords = this.selectedKeywords;
        newFiltersBig.softwareList = this.softwareList;
        newFiltersBig.selectedSoftware = this.selectedSoftware;
        newFiltersBig.providerList = this.providerList;
        newFiltersBig.selectedProviders = this.selectedProviders;
        newFiltersBig.serviceSort = this.serviceSort;
        newFiltersBig.searchInput = this.searchInput;
        newFiltersSmall.keywordList = this.keywordList;
        newFiltersSmall.selectedKeywords = this.selectedKeywords;
        newFiltersSmall.softwareList = this.softwareList;
        newFiltersSmall.selectedSoftware = this.selectedSoftware;
        newFiltersSmall.providerList = this.providerList;
        newFiltersSmall.selectedProviders = this.selectedProviders;
        newFiltersSmall.serviceSort = this.serviceSort;
        newFiltersSmall.searchInput = this.searchInput;
        topbar.innerHTML = "";
        var taglist = document.createElement("div");
        taglist.id = "taglist";
        topbar.appendChild(newFiltersBig);
        topbar.appendChild(newFiltersSmall);
        topbar.appendChild(taglist);
    }
    /* reset all filters and recreate the filter-bars without any selected options */
    _resetFilters(e) {
        this.searchInput = "";
        this.selectedKeywords = [];
        this.selectedProviders = [];
        this.selectedSoftware = [];
        this.serviceSort = this.defaultSorting;
        this._updateFilterBar();
        // apply filters again to update the displayed services
        this._applyFilters();
    }
    _openfeedback(e) {
        this.shadowRoot.querySelector('feedback-banner').classList.remove('hide');
        this.shadowRoot.querySelector('header').classList.remove('header-border');
    }

    /* opens the details panel for a given service */
    _showDetailsPanel(e) {
        const el = this.shadowRoot.getElementById("panel");
        const newService = e.detail.message;
        const oldService = el.querySelector('service-profile') ?
            el.querySelector('service-profile').service : null;
        if (!oldService || (newService["uuid"] !== oldService["uuid"])) {
            this.serviceName = newService['displayName'];
            this.serviceContact = newService['email'];
            const serviceProfile = document.createElement('service-profile');
            serviceProfile.uuid = newService["uuid"];
            serviceProfile.service = newService["service"];
            this._removeAllChildNodes(el);
            el.appendChild(serviceProfile);
        }
        this.shadowRoot.getElementById('content').classList.add('show');
        this.shadowRoot.getElementById('details').classList.add('show');
        this.serviceDetails = newService.name;
        this._updateSearchParams();

        this.newServices.forEach(service => {
            if (oldService && service['name'] == oldService['name']) {
                delete service['highlighted'];
            }
            if (service['name'] == newService['name']) {
                service['highlighted'] = true;
            }
        });
        var newList = document.createElement("services-list");
        newList.services = this.newServices;
        var servicesElement = this.shadowRoot.getElementById("services");
        servicesElement.innerHTML = "";
        servicesElement.appendChild(newList);
    }
    /* closed the details panel */
    _closeDetailsPanel() {
        this.shadowRoot.getElementById('content').classList.remove('show');
        this.shadowRoot.getElementById('details').classList.remove('show');
        const el = this.shadowRoot.getElementById("panel");
        this._removeAllChildNodes(el);
        this.serviceDetails = "";
        this.newServices.forEach(service => {
            delete service['highlighted'];
        });
        var newList = document.createElement("services-list");
        newList.services = this.newServices;
        var servicesElement = this.shadowRoot.getElementById("services");
        servicesElement.innerHTML = "";
        servicesElement.appendChild(newList);
        this._updateSearchParams();
    }
    /* helper method to remove all child nodes for a given parent */
    _removeAllChildNodes(parent) {
        while (parent.firstChild) {
            parent.removeChild(parent.firstChild);
        }
    }
    /* hides the notification banner if the user already dismissed it previously */
    _notificationDismissalListener() {
        this.shadowRoot.querySelector('notification-banner').classList.add('hide');
        this.shadowRoot.querySelector('header').classList.add('header-border');
    }
    /* hides the notification banner if the user already dismissed it previously */
    _maintenanceDismissalListener() {
        this.shadowRoot.querySelector('maintenance-banner').classList.add('hide');
    }
    _feedbackDismissalListener() {
        this.shadowRoot.querySelector('feedback-banner').classList.add('hide');
        this.shadowRoot.querySelector('header').classList.add('header-border');
    }
}
customElements.define('services-view', ServicesView);
