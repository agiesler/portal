import { css, html, LitElement } from 'lit-element';

import '@material/mwc-button';
import '@material/mwc-textfield';
import '@material/mwc-switch';
import '@material/mwc-formfield';
import '@material/mwc-select';

import '../../../common-components/header/helmholtz-cloud-header.js';
import '../../../common-components/footer/helmholtz-cloud-footer.js';


class RequestForm extends LitElement {
    constructor() {
        super();
        this.serviceMapping = {
            "svc-01eb3268-853d-1326-bf76-ea0d786b0753": "Nubes"
        }
        this.typeMapping = {
            "ComputeResourceSpecV1": "Computing Resource",
            "GroupStorageResourceSpecV1": "Group Folder"
        }
        var headers = new Headers({
            "Accept": "application/json"
        });
        this.alreadySubmitted = false;
        fetch(`${window.location.origin}${cp_prefix}principal`, { headers: headers })
            .then((response) => {
                if (response.status !== 200) {
                    throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
                }
                return response.json();
            })
            .then(principal => {
                this.principal = principal;
            })
            .catch(e => console.log(e));

        fetch(`${window.location.origin}${cp_prefix}permissions`, { headers: headers })
            .then((response) => {
                if (response.status !== 200) {
                    throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
                }
                return response.json();
            })
            .then(permissions => {
                this._setup(permissions.results);
            })
            .catch(e => console.log(e));
    }

    static get properties() {
        return {
            principal: {
                type: Array
            },
            errorMessage: {
                type: String
            }
        }
    }
    static get styles() {
        return css`
        :host {
            width: 100%;
            height: 100%;
            overflow: hidden;
        }
        main {
            height: 100vh;
        }
        .container {
            display: flex;
            flex-direction: column;
            align-items: center;
            margin-top: 20px;
        }
        .specs {
            display: flex;
            flex-direction: column;
            align-items: center;
        }
        label, input {
            margin-bottom: 20px;
        }
        mwc-textfield {
            min-width: 400px;
            margin-top: 30px;
            --mdc-theme-primary: rgb(0,90,160);
            --mdc-theme-on-primary: white;
        }
        mwc-select {
            min-width: 400px;
            margin-top: 30px;
            --mdc-theme-primary: rgb(0,90,160);
            --mdc-theme-on-primary: white;
        }
        mwc-formfield {
            margin-top: 30px;
            --mdc-theme-primary: rgb(0,90,160);
            --mdc-theme-on-primary: white;
        }
        mwc-button {
            --mdc-theme-primary: rgb(0,90,160);
            --mdc-theme-on-primary: white;
        }
        mwc-slider {

            min-width: 400px;
            margin-top: 30px;
            --mdc-theme-primary: rgb(0,90,160);
            --mdc-theme-on-primary: white;
        }
        .hide {
            display: none;
        }
        .result {
            padding-top: 50px;
            display: block;
        }
        .error {
            display: block;
        }
        .form {
        }
        .slidecontainer {
            width: 100%; /* Width of the outside container */
        }
        .slider {
            -webkit-appearance: none;
            width: 100%;
            height: 15px;
            border-radius: 5px;
            background: #d3d3d3;
            outline: none;
            opacity: 0.7;
            -webkit-transition: .2s;
            transition: opacity .2s;
        }
        .slider::-webkit-slider-thumb {
            -webkit-appearance: none;
            appearance: none;
            width: 25px;
            height: 25px;
            border-radius: 50%;
            background: rgb(0,90,160);
            cursor: pointer;
        }
        .slider::-moz-range-thumb {
            width: 25px;
            height: 25px;
            border-radius: 50%;
            background: rgb(0,90,160);
            cursor: pointer;
        }
        .spin {
            display: inline-block;
            width: 50px;
            height: 50px;
            border: 3px solid rgba(255, 255, 255, .3);
            border-radius: 50%;
            border-top-color: #005aa0;
            animation: spin 1s ease-in-out infinite;
            -webkit-animation: spin 1s ease-in-out infinite;
        }
        @keyframes spin {
            to {
              -webkit-transform: rotate(360deg);
            }
        }
        @-webkit-keyframes spin {
            to {
              -webkit-transform: rotate(360deg);
            }
        }
        `;

    }
    render() {
        return html`
        <header class="header-border">
            <helmholtz-cloud-header backgroundcolor></helmholtz-cloud-header>
        </header>
        <main>
            <div class="container">
                <h2>Request a new resource</h2>
                <div class="hide" id="error">${this.errorMessage}</div>
                <mwc-select class="hide" label="Service Name" id="serviceName">
                    <mwc-list-item></mwc-list-item>
                    <!--<mwc-list-item selected value="svc-01eb3268-853d-1326-bf76-ea0d786b0753">Nubes</mwc-list-item>
                    <mwc-list-item value="svc-01eb1182-767c-1244-b23d-d702acab8cb5">Openstack</mwc-list-item>-->
                </mwc-select>
                <mwc-select class="hide" label="Resource Type" id="resourceType">
                    <mwc-list-item></mwc-list-item>
                    <!--<mwc-list-item selected value="Group Folder">Group Folder</mwc-list-item>
                    <mwc-list-item value="Computing Resource">Computing Resource</mwc-list-item>-->
                </mwc-select>

                <div class="hide specs" id="groupSpecs">
                    <mwc-textfield label="Desired Name" id="desiredName" name="desiredName" helper="Name of new folder"></mwc-textfield>
                    <div class="slidecontainer">
                        <input type="range" min="1" max="100" value="1" class="slider" id="quotaRange">
                    </div>
                    <div id="quotaOutput"></div>
                    <!--<mwc-formfield label="The folder is for myself">
                    <mwc-switch id="userSwitchGroup"></mwc-switch>-->
                    </mwc-formfield>
                    <mwc-textfield label="VOs" id="vosGroup" name="vosGroup" helper="List of VOs that should have access to the folder." ></mwc-textfield>
                </div>

                <div class="hide specs" id="computeSpecs">
                    <mwc-textfield label="CPU" id="cpu" name="cpu" helper="Number of CPUs"></mwc-textfield>
                    <mwc-textfield label="RAM" id="ram" name="ram" helper="Size of RAM in GB"></mwc-textfield>
                    <mwc-textfield label="Storage" id="storage" name="storage" helper="Size of storage in GB"></mwc-textfield>
                    <mwc-formfield label="The compute resource is for myself">
                    <mwc-switch id="userSwitchCompute"></mwc-switch>
                    </mwc-formfield>
                    <mwc-textfield label="VO" id="vosCompute" name="vosCompute" helper="List of VOs that should have access to the resource." ></mwc-textfield>
                </div>

                <div class="hide" id="submitbutton"><mwc-button @click="${this._sendRequest}" label="Submit"></mwc-button></div>
                <div class="hide" id="result">
                    <div class="spin"></div>
                </div>
            </div>
        </main>
        <footer>
            <helmholtz-cloud-footer minima></helmholtz-cloud-footer>
        </footer>
        `;
    }
    _setup(permissions) {
        if (permissions.length == 0) {
            var error = this.shadowRoot.getElementById("error");
            error.classList.remove("hide");
            error.innerHTML = '<span style="color:red">You do not have authorization to book or upgrade resources.</span>';
            return;
        }
        var serviceNameSelect = this.shadowRoot.getElementById("serviceName");
        serviceNameSelect.classList.remove("hide");
        this.permissions = {};
        permissions.forEach(permission => {
            const key = permission['_id']['serviceId'];
            if (!(key in this.permissions)) {
                this.permissions[key] = [];
                var item = document.createElement("mwc-list-item");
                item.value = permission['_id']['serviceId'];
                item.innerHTML = this.serviceMapping[permission['_id']['serviceId']];
                serviceNameSelect.appendChild(item);
            }
            this.permissions[key].push(permission['_id']['type']);
        });
    }
    firstUpdated() {
        this.shadowRoot.getElementById("userSwitchCompute").addEventListener("change", function (e) {
            if (e.target.__checked) {
                this.shadowRoot.getElementById("vosCompute").disabled = true;
            } else {
                this.shadowRoot.getElementById("vosCompute").disabled = false;
            }
        }.bind(this));
        this.shadowRoot.getElementById("serviceName").addEventListener("change", function (e) {
            this.shadowRoot.getElementById("resourceType").classList.remove('hide');
            this.shadowRoot.getElementById("groupSpecs").classList.add('hide');
            this.shadowRoot.getElementById("submitbutton").classList.add('hide');
            this.shadowRoot.getElementById("computeSpecs").classList.add('hide');
            this.shadowRoot.getElementById('result').classList.remove('result');
            this.shadowRoot.getElementById('result').classList.add('hide');
            this.shadowRoot.getElementById('error').classList.remove('error');
            this.shadowRoot.getElementById('error').classList.add('hide');
            var permissions = this.permissions[e.target.value];

            var resourceTypeSelector = this.shadowRoot.getElementById("resourceType");
            resourceTypeSelector.disabled = false;
            resourceTypeSelector.innerHTML = "";
            permissions.forEach(permission => {
                var item = document.createElement("mwc-list-item");
                item.value = permission;
                item.innerHTML = this.typeMapping[permission];
                resourceTypeSelector.appendChild(item);
            })
        }.bind(this));
        this.shadowRoot.getElementById("resourceType").addEventListener("change", function (e) {
            this.shadowRoot.getElementById('result').classList.remove('result');
            this.shadowRoot.getElementById('result').classList.add('hide');
            this.shadowRoot.getElementById('error').classList.remove('error');
            this.shadowRoot.getElementById('error').classList.add('hide');
            if (e.target.value == 'GroupStorageResourceSpecV1') {
                this.shadowRoot.getElementById("groupSpecs").classList.remove('hide');
                this.shadowRoot.getElementById("submitbutton").classList.remove('hide');
                this.shadowRoot.getElementById("computeSpecs").classList.add('hide');
            }
            else if (e.target.value == 'ComputeResourceSpecV1') {
                this.shadowRoot.getElementById("computeSpecs").classList.remove('hide');
                this.shadowRoot.getElementById("submitbutton").classList.remove('hide');
                this.shadowRoot.getElementById("groupSpecs").classList.add('hide');
            }
        }.bind(this));
        var slider = this.shadowRoot.getElementById("quotaRange");
        var output = this.shadowRoot.getElementById("quotaOutput");
        output.innerHTML = "Quota: " + slider.value + "GB";

        slider.oninput = function () {
            output.innerHTML = "Quota: " + this.value + "GB";
        }
    }
    checkNumber(x) {
        if (isNaN(x) || x < 1) {
            return false;
        }
        return true;
    }
    _sendRequest(e) {
        e.preventDefault()
        if (this.alreadySubmitted) {
            return;
        }
        this.shadowRoot.querySelector('#result').classList.replace('hide', 'result');

        const CSRF_TOKEN = document.cookie.match(new RegExp(`XSRF-TOKEN=([^;]+)`))[1];
        const headers = new Headers({
            "X-XSRF-TOKEN": CSRF_TOKEN,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        });

        var service = this.shadowRoot.getElementById("serviceName").value;
        var resourceType = this.shadowRoot.getElementById("resourceType").value;

        var data = {};
        var errors = [];
        var error = this.shadowRoot.querySelector('#error')
        if (resourceType == 'ComputeResourceSpecV1') {
            var cpu = this.shadowRoot.getElementById("cpu").value;
            const br = document.createElement("br");
            if (!this.checkNumber(cpu)) {
                const message = document.createElement("span");
                message.style = "color:red";
                message.innerHTML = "CPU must be a number";
                errors.push(message);
            }
            var ram = this.shadowRoot.getElementById("ram").value;
            if (!this.checkNumber(ram)) {
                const message = document.createElement("span");
                message.style = "color:red";
                message.innerHTML = "RAM must be a number";
                errors.push(message);
            }
            var storage = this.shadowRoot.getElementById("storage").value;
            if (!this.checkNumber(storage)) {
                const message = document.createElement("span");
                message.style = "color:red";
                message.innerHTML = "Storage must be a number";
                errors.push(message);
            }
            var vos = this.shadowRoot.getElementById("vosCompute").value;
            var userSwitchCompute = this.shadowRoot.getElementById("userSwitchCompute")
            if (userSwitchCompute.__checked == false && vos == "") {
                const message = document.createElement("span");
                message.style = "color:red";
                message.innerHTML = "VOs have to be set";
                errors.push(message);
            }
            data['type'] = "ComputeResourceSpecV1";
            data['specification'] = {
                "ram": {
                    "unit": "GB",
                    "value": ram
                },
                "cpu": {
                    "unit": "cores",
                    "value": cpu
                },
                "storage": {
                    "unit": "GB",
                    "value": storage
                }
            }

            var userFolder = this.shadowRoot.getElementById("userSwitchCompute");

            if (userFolder.__checked) {
                data['targetEntity'] = {
                    "type": "UserSpecV1",
                    "specification": {
                        "userId": this.principal.attributes.eduperson_unique_id
                    }
                };
            } else {
                data['targetEntity'] = {
                    "type": "VOListSpecV1",
                    "specification": {
                        "vos": vos.split(",")
                    }
                };
            }
        }

        if (resourceType == 'GroupStorageResourceSpecV1') {
            var name = this.shadowRoot.getElementById("desiredName").value;

            if (name == "") {
                const message = document.createElement("span");
                message.style = "color:red";
                message.innerHTML = "Name has to be set";
                errors.push(message);
            }
            var quota = this.shadowRoot.getElementById("quotaRange").value;

            var vos = this.shadowRoot.getElementById("vosGroup").value;
            if (vos == "") {
                const message = document.createElement("span");
                message.style = "color:red";
                message.innerHTML = "VOs have to be set";
                errors.push(message);
            }
            data['type'] = "GroupStorageResourceSpecV1";
            data['specification'] = {
                "desiredName": name,
                "quota": {
                    "unit": "GB",
                    "value": quota
                }
            }

            data['targetEntity'] = {
                "type": "VOListSpecV1",
                "specification": {
                    "vos": vos.split(",")
                }
            };
        }

        if (errors.length != 0) {
            error.classList.replace('hide', 'error');
            error.innerHTML = "";
            var list = document.createElement("ul");
            errors.forEach(error => {
                var item = document.createElement("li");
                item.appendChild(error);
                list.appendChild(item);
            });
            error.appendChild(list);
            return;
        }
        const options = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: headers
        }

        var allocateUrl = `${window.location.origin}${cp_prefix}api/v0/services/${service}/allocate`
        fetch(allocateUrl, options)
            .then((response) => {
                return response.json()
            })
            .then(json => {
                this.shadowRoot.querySelector('#error').classList.replace('form', 'hide');
                this.shadowRoot.querySelector('#error').classList.replace('error', 'hide');
                this.shadowRoot.querySelector('#result').innerHTML = `
                <div>
                    Request submitted!
                </div>
                <div>You can check the status of your resources <a href="/resources">here</a>.</div>
                `
            }).catch(e => {
                this.errorMessage = e.message;
                this.shadowRoot.querySelector('#result').classList.replace('result', 'hide');
                this.shadowRoot.querySelector('#error').classList.replace('hide', 'error');
            })
        this.alreadySubmitted = true;
        return false;
    }
}
customElements.define('request-form', RequestForm);