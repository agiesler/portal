import {css, html, LitElement} from 'lit-element';

import '@material/mwc-select';
import '@material/mwc-list/mwc-list-item';
import '@material/mwc-textfield';
import '@material/mwc-switch';
import '@material/mwc-icon-button';
import '@material/mwc-textarea';
import '@material/mwc-button';

import '../../common-components/header/helmholtz-cloud-header.js';
import '../../views/forms/image-form/image-form';
import '../../views/forms/service-form/service-form';

class FormsView extends LitElement
{
    constructor()
    {
        super();
    }
    connectedCallback()
    {
        super.connectedCallback();
    }
    static get properties()
    {
        return {
        };
    }
    static get styles()
    {
        return css`
            :host {
                width: 100%;
                height: 100%;
                overflow: hidden;
            }
            header,
            main,
            footer {
                width: 100%;
            }
            header {
                position: fixed;
                top: 0;
                z-index: 100;
                box-sizing: border-box;
                border-bottom: 1px solid #274C69;
                box-shadow: 0 -2px 8px rgb(0 0 0 / 9%),
                    0 4px 8px rgb(0 0 0 / 6%),
                    0 1px 2px rgb(0 0 0 / 30%),
                    0 2px 6px rgb(0 0 0 / 15%)
            }
            * {
                box-sizing: border-box;
            }
            main {
                display: flex;
                flex-direction: column;
                align-items: center;
                padding-top: 50px; /*must be equal to the height of the header*/
                width: 100%;
                height: 100%;
                overflow: hidden;
            }
            .container {
                /*display: flex;
                flex-direction: column;
                justify-content: center;*/
            }
        `;
    }
    render()
    {
        return html`
            <header class="header-shadow">
                <helmholtz-cloud-header backgroundcolor></helmholtz-cloud-header>
            </header>
            <main>
                <div class="side-nav">
                    <nav>
                        <mwc-button @click="${this._serviceForm}" label="service form"></mwc-button>
                        <mwc-button @click="${this._imageForm}" label="image form"></mwc-button>
                    </nav>
                </div>
                <div class="container">
                    <div id="display" class="default-display">
                        <image-form></image-form>
                    </div>
                </div>
            </main>
        `;
    }
    _serviceForm()
    {
        this.removeAllNode(this.shadowRoot.querySelector('#display'))
        this.shadowRoot.querySelector('#display')
            .appendChild(document.createElement('service-form'))
    }
    _imageForm()
    {
        this.removeAllNode(this.shadowRoot.querySelector('#display'))
        this.shadowRoot.querySelector('#display')
            .appendChild(document.createElement('image-form'))
    }
    removeAllNode(parent)
    {
        while (parent.firstChild) {
            parent.removeChild(parent.firstChild);
        }
    }
}
customElements.define('forms-view', FormsView);