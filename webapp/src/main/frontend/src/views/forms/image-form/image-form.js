import {css, html, LitElement} from 'lit-element';

import '@material/mwc-button';

class ImageForm extends LitElement
{
    constructor()
    {
        super();
        this.uploadUrl = `${window.CONFIG['cerebrum.endpoint']}images`
    }

    static get properties()
    {
        return {
            uploadUrl: {
                type: String
            }
        }
    }
    static get styles()
    {
        return css`
            .hide {
                display: none;
            }
            .result {
                display: block;
            }
            .error {
                display: block;
            }
            .form {
            }
        `;

    }
    render()
    {
        return html`
            <span>Add new image</span>
            <div class="hide" id="error">${this.errorMessage}</div>
            <form class="form" action="${this.uploadUrl}"
                  enctype="multipart/form-data"
                  id="form" @submit="${this._upload}">
                <div>
                    <input required id="name" name="name" @input="${this._checkValidity}">
                </div>
                <div>
                    <input class="" type="file" name="image" id="image"  accept="image/*" @change="${this._checkValidity}">
                </div>
                <div><input type="submit" value="Upload" id="button" disabled></div>
            </form>
            <div class="hide" id="result">
                <p>Cerebrum is happy with your upload. Here are the details</p>
                <div>
                    <span>metadata</span>
                    <div>
                        <code id="metadata"></code>
                    </div>
                    <img id="img" width="100px" height="auto">
                </div>
                <div><button>do you want to add another one</button></div>
            </div>
        `;
    }
    _upload(e)
    {
        e.preventDefault()
        console.log(e);
        const form = this.shadowRoot.querySelector("#form");
        const fileName = form.querySelector("#image").value;
        const extention = fileName.split(".").pop()

        if (extention !== "svg") {
            this.errorMessage = "Only svg file can be uploaded"
            this.shadowRoot.querySelector("#error").append("");
            return ;
        }
        const options = {
            method: 'POST',
            body: new FormData(form),
            headers: {
                'Authorization': `Bearer ${sessionStorage.getItem("access_token")}`
            }
        }
        fetch(form.action, options)
            .then((response) => {
                return response.json()
            })
            .then(json => {
                this.shadowRoot.querySelector('#img').src = `data:image/svg+xml;base64,${json["image"]}`;
                const newMet = JSON.parse(JSON.stringify(json));
                delete newMet["image"];
                delete newMet["createdDate"];
                delete newMet["lastModifiedDate"];
                delete newMet["foreignKeys"];
                this.shadowRoot.querySelector('#metadata').append(JSON.stringify(newMet));
                this.shadowRoot.querySelector('#error').classList.replace('form', 'hide');
                this.shadowRoot.querySelector('#error').classList.replace('error', 'hide')
                this.shadowRoot.querySelector('#result').classList.replace('hide', 'result')
            }).catch( e=> {
                this.errorMessage = e.message;
                this.shadowRoot.querySelector('#result').classList.replace('result', 'hide');
                this.shadowRoot.querySelector('#error').classList.replace('hide', 'error')
            })
        return false;
    }
    _checkValidity(e)
    {
        console.log(e);
        const form = this.shadowRoot.querySelector("#form");
        const fileName = form.querySelector("#image").value;
        const extension = fileName.split(".").pop()

        const name = form.querySelector("#name").value;

        if (extension === "svg" &&  (name !== "" || name !== null)) {
            this.shadowRoot.querySelector('#error').classList.replace('error', 'hide')
            this.shadowRoot.querySelector('#button').removeAttribute("disabled");
        }

        if (extension !== "svg" && e.type === "change") {
            console.log("extension problem")
            this.shadowRoot.querySelector('#button').setAttribute("disabled", true);
        }
        if ((name === "" || name === null) && e.type === "input") {
            console.log("name is empty");
            this.shadowRoot.querySelector('#button').setAttribute("disabled", true);
        }
    }
}
customElements.define('image-form', ImageForm);