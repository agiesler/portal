import { LitElement, html, css } from 'lit-element';

import '@material/mwc-icon-button';
import '@material/mwc-icon';

import '../../common-components/requests-list/requests-list.js';
import '../../common-components/header/helmholtz-cloud-header.js';
import '../../common-components/footer/helmholtz-cloud-footer.js';

class ResourcesView extends LitElement {
    constructor() {
        super();
        const headers = new Headers({
            "Accept": "application/json"
        });
        this.resourceTypeMapping = {
            "GroupStorageResourceSpecV1": "Group Folder",
            "ComputeResourceSpecV1": "Computing Resource"
        }

        fetch(`${window.location.origin}${cp_prefix}api/v0/services/idMapping`, { headers: headers })
            .then((response) => {
                if (response.status !== 200) {
                    throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
                }
                return response.json();
            })
            .then((mapping) => {
                this.serviceMapping = {};
                mapping.content.forEach(element => {
                    this.serviceMapping[element['uuid']] = element['displayName'];
                });
            })
        fetch(`${window.location.origin}${cp_prefix}principal`, { headers: headers })
            .then((response) => {
                if (response.status !== 200) {
                    throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
                }
                return response.json();
            })
            .then(principal => {
                this.principal = principal;
                fetch(`${window.location.origin}${cp_prefix}api/v0/users/${principal.name}/resources?sort=LastModifiedDate.desc`, { headers: headers })
                    .then((response) => {
                        if (response.status !== 200) {
                            throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
                        }
                        return response.json();
                    })
                    .then(data => {
                        this.resources = data.content;
                    })
                    .catch(e => console.log(e));

            })
            .catch(e => console.log(e));

    }
    static get properties() {
        return {
            resources: {
                type: Array
            },
            principal: {
                type: Array
            },
            serviceMapping: {
                type: Object
            },
            resourceTypeMapping: {
                type: Object
            }
        };
    }
    static get styles() {
        return css`
            :host {
                width: 100%;
                height: 100%;
                overflow: hidden;
            }
            header,
            main,
            footer {
                width: 100%;
            }
            header {
                position: fixed;
                top: 0;
                z-index: 100;
                box-sizing: border-box;
                box-shadow: 0 -2px 8px rgb(0 0 0 / 9%),
                    0 4px 8px rgb(0 0 0 / 6%),
                    0 1px 2px rgb(0 0 0 / 30%),
                    0 2px 6px rgb(0 0 0 / 15%)
            }
            .header-border {
                border-bottom: 1px solid #274C69;
            }
            * {
                box-sizing: border-box;
            }
            main {
                display: flex;
                padding-top: 50px; /*must be equal to the height of the header*/
                width: 100%;
                height: 100vh;
                overflow: hidden;
                padding-bottom: 30px;
            }
            .container {
                display: flex;
                flex-direction: column;
                align-items: center;
                margin-top: 20px;
                width: 100%;
            }
            .list, .details {
                height: 100%;
            }
            .list {
                flex: 1 1 auto;
                padding: 20px;
                overflow-y: scroll;
                max-width: 100%;
            }
            .details {
                overflow: hidden;
                background-color: #efefef;
                width: 0;
                transition: width 0.3s linear;
            }
            #details.show {
                width: 600px;
            }
            #list.show {
                width: calc(100% - 600px);
            }
            .details .buttons {
                max-height: 50px;
                height: 5%;
                display: flex;
            }
            .buttons div {
                flex: 1 1 auto;
                display: flex;
                align-items: center;
                padding: 20px;
                color: #ff5722;
            }
            .buttons div a {
                text-decoration: none;
                font-size: 0.85em;
                padding-right: 5px;
                color: #ff5722;
                font-weight: 300;
            }
            .buttons div mwc-icon {
                font-size: 1.21em;
            }
            .details .details-content {
                background-color: white;
                margin: 20px;
                padding: 20px;
                border-radius: 3px;
                height: 90%;
                overflow-y: scroll;
            }
            footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
            }
            .hide {
                display: none;
            }
            @media screen and (max-width: 771px) {
                #list.show {
                    display: none;
                }
                #details.show {
                    width: 100vw;
                }
            }
            @media screen and (min-width: 772px) {
                #list.show {
                    width: calc(100% - 390px);
                }
                #details.show {
                    width: 390px;
                }
            }
            @media screen and (min-width: 1100px) {
                #list.show {
                    width: calc(100% - 600px);
                }
                #details.show {
                    width: 600px;
                }
            }
            .resourceTable {
                display: table;
                width: 100%;
                padding: 30px;
            }
            table, th, td {
                border:1px solid black;
                border-collapse: collapse;
              }
            table {
                border-spacing: 0;
                width: 100%;
                border: 1px solid #ddd;
            }
            tr {
                height: 20px;
                padding: 20px;
            }
            th {
                cursor: pointer;
            }
            th, td {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
            }
            tr:nth-child(even) {
                background-color: #f2f2f2
            }
        `;
    }
    sortTable(e) {
        if (e.originalTarget) {
            var n = e.originalTarget.cellIndex;
        } else {
            var n = e.path[0].cellIndex;
        }
        var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
        table = this.shadowRoot.getElementById("resourceTable");
        switching = true;
        // Set the sorting direction to ascending:
        dir = "asc";
        /* Make a loop that will continue until
        no switching has been done: */
        while (switching) {
            // Start by saying: no switching is done:
            switching = false;
            rows = table.rows;
            /* Loop through all table rows (except the
            first, which contains table headers): */
            for (i = 1; i < (rows.length - 1); i++) {
                // Start by saying there should be no switching:
                shouldSwitch = false;
                /* Get the two elements you want to compare,
                one from current row and one from the next: */
                x = rows[i].getElementsByTagName("TD")[n];
                y = rows[i + 1].getElementsByTagName("TD")[n];
                /* Check if the two rows should switch place,
                based on the direction, asc or desc: */
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        // If so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        // If so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                /* If a switch has been marked, make the switch
                and mark that a switch has been done: */
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
                // Each time a switch is done, increase this count by 1:
                switchcount++;
            } else {
                /* If no switching has been done AND the direction is "asc",
                set the direction to "desc" and run the while loop again. */
                if (switchcount == 0 && dir == "asc") {
                    dir = "desc";
                    switching = true;
                }
            }
        }
    }

    render() {
        if (this.resources == undefined) { return; };
        if (this.serviceMapping == undefined) { return; };
        return html`
        <header class="header-border">
        <helmholtz-cloud-header backgroundcolor></helmholtz-cloud-header>
        </header>
        <main>
            <div class="container">
                ${this.resources.length == 0 ?
                    html`
                        <div>You have no resources. You can book them <a href="/request_resource">here<a>.</div>` :
                    html`
                        <div class="resourceTable" id="resourceTable">
                            <table>
                                <tr>
                                    <th @click=${this.sortTable}>Id</th>
                                    <th @click=${this.sortTable}>Resource Type</th>
                                    <th @click=${this.sortTable}>State</th>
                                    <th @click=${this.sortTable}>Service Name</th>
                                    <th @click=${this.sortTable}>Created</th>
                                    <th @click=${this.sortTable}>Updated</th>
                                </tr>
                                ${this.resources.map(resource =>
                                html`
                                    <tr>
                                        <td><a href="/resource?id=${resource.uuid}">${resource.uuid}</a></td>
                                        <td>${this.resourceTypeMapping[resource.resourceType]}</td>
                                        <td>${resource.status}</td>
                                        <td>${this.serviceMapping[resource.serviceId]}</td>
                                        <td>${new Date(resource.createdDate).toISOString()}</td>
                                        <td>${new Date(resource.lastModifiedDate).toISOString()}</td>
                                    </tr>`)
                                }
                            </table>
                        </div>`
            }
            </div>
        </main>
        <footer>
            <helmholtz-cloud-footer minima></helmholtz-cloud-footer>
        </footer>
        `;
    }
    _removeAllChildNodes(parent) {
        while (parent.firstChild) {
            parent.removeChild(parent.firstChild);
        }
    }

}
customElements.define('resources-view', ResourcesView);
