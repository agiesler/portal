import { LitElement, html, css } from 'lit';

import '@material/mwc-icon-button';
import '@material/mwc-icon';

import '../../common-components/header/helmholtz-cloud-header.js';
import '../../common-components/footer/helmholtz-cloud-footer.js';

class AboutmeView extends LitElement {
    constructor() {
        super();
        const headers = new Headers({
            "Accept": "application/json"
        });
        fetch(`${window.location.origin}${cp_prefix}principal`, { headers: headers })
            .then((response) => {
                if (response.status !== 200) {
                    throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
                }
                return response.json();
            })
            .then(principal => {
                this.principal = principal;
            })
            .catch(e => console.log(e));
    }
    static get properties() {
        return {
            principal: {
                type: Array
            }
        };
    }
    static get styles() {
        return css`
            :host {
                width: 100%;
                height: 100%;
                overflow: hidden;
                -ms-overflow-style: none;  /* IE and Edge */
                scrollbar-width: none;  /* Firefox */
            }
            header,
            main,
            footer {
                width: 100%;
            }
            header {
                position: fixed;
                top: 0;
                z-index: 100;
                box-sizing: border-box;
                box-shadow: 0 -2px 8px rgb(0 0 0 / 9%),
                    0 4px 8px rgb(0 0 0 / 6%),
                    0 1px 2px rgb(0 0 0 / 30%),
                    0 2px 6px rgb(0 0 0 / 15%)
            }
            .header-border {
                border-bottom: 1px solid #274C69;
            }
            span {
                padding-bottom: 10px;
            }
            footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
            }
            * {
                box-sizing: border-box;
            }
            notification-banner {
                background-color: #e3e3e3;
            }
            main {
                display: flex;
                flex-direction: column;
                padding-top: 50px; /*must be equal to the height of the header*/
                width: 100%;
                height: 100vh;
                overflow: hidden;
                padding-bottom: 30px;
                align-items: center;
            }
            .content::-webkit-scrollbar {
                display: none;
            }
            .content {
                display: flex;
                flex-direction: column;
                width: 30%;
                padding-left: 20px;
                padding-right: 20px;
                overflow-y: scroll;
                -ms-overflow-style: none;  /* IE and Edge */
                scrollbar-width: none;  /* Firefox */
                align-items: center;
            }
            table, th, td  {
                border: 1px solid #000;
                border-collapse: collapse;
                white-space: nowrap;
                width: 100%;
                padding: 15px;
            }
            tr:hover {background-color: rgba(140,180,35,.2);}
        `;
    }

    render() {
        return html`
        <header class="header-border">
        <helmholtz-cloud-header backgroundcolor></helmholtz-cloud-header>
        </header>
        <main>
            <div class="content">
                <h2>User Information</h2>
                ${this.principal ?
                    this.principal.attributes != null ?
                    html`
                        <span>This page shows information about the currently logged-in user:</span>
                        <table>
                        ${this.principal.attributes.name != null ?
                        html`
                            <tr>
                                <td>Name</td>
                                <td><b>${this.principal.attributes.name}</b></td>
                            </tr>` : ''}
                        ${this.principal.attributes.email != null ?
                        html`
                            <tr>
                                <td>Email</td>
                                <td><b>${this.principal.attributes.email}</b></td>
                            </tr>` : ''}
                        </table>`
                    : ''
                : ''}
            </div>
        </main>
        <footer>
            <helmholtz-cloud-footer minima></helmholtz-cloud-footer>
        </footer>
        `;
    }
}
customElements.define('aboutme-view', AboutmeView);