/* eslint class-methods-use-this: ["error", { "exceptMethods": ["_goToServices"] }] */
import { LitElement, html, css } from 'lit-element';

import '@material/mwc-button';

import './sections/mission-section.js';
import './sections/feature-section.js';
import '../../common-components/header/helmholtz-cloud-header.js';
import '../../common-components/footer/helmholtz-cloud-footer.js';
import '../../common-components/maintenance-banner/maintenance-banner.js';

class LandingView extends LitElement
{
    constructor()
    {
        super();
        this.addEventListener('wheel', this._scrollListener, true);

        this.addEventListener(
            'helmholtz-cloud-dismiss-maintenance-services',
            this._maintenanceDismissalListener, true);

        var maintenanceUrl = `${window.location.origin}${cp_prefix}api/v0/maintenanceMessage`

        const headers = new Headers({
            "Accept": "application/json"
        });
        fetch(maintenanceUrl, { headers: headers })
        .then((response) => {
            if (response.status !== 200) {
                throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
            }
            return response.json();
        })
        .then(data => {
            this.maintenanceMessage = data.message;
        })
        .catch(e => console.log("no maintenance message available"));
    }
    static get properties() {
        return {
            maintenanceMessage: {
                type: String
            }
        };
    }
    static get styles()
    {
        return css`
            :host {
                width: 100%;
                height: 100%;
                min-width: 320px;
            }
            header,
            main,
            footer {
                width: 100%;
                min-width: 320px;
            }
            header {
                position: fixed;
                top: 0;
            }
            maintenance-banner {
                background-color: #ffcb0d;
            }
            .header-shadow {
                box-sizing: border-box;
                box-shadow: 0px 0px 20px rgb(0 0 0 / 50%);
                border-bottom: 1px solid #c5c5c5;
            }
            main {
                display: flex;
                flex-direction: column;
                margin-top: 50px; /*must be equal to the height of the header*/
            }
            .hero {
                height: 100vh;
                min-height: 345px;
                background-color: #005aa0;
                color: #fff;
                display: flex;
                flex-direction: column;
                place-content: center;
                align-items: center;
                text-align: center;
                padding: 5%;
                box-sizing: border-box;
                background-attachment: fixed;
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
                background-image: url('./media/i/blue-background_16_9.jpg');
            }
            .hero h1 {
                font-size: 4em;
                margin: 0;
                font-family: 'Hermann';
            }
            .hero mwc-button {
                --mdc-theme-primary: #8cb422;
                --mdc-theme-on-primary: white;
                width: 200px;
                transform: skew(-20deg);
                border-radius: 4px;
            }
            .definition {
                font-size: 1.2em;
                font-weight: 300;
            }
            .hide {
                display: none;
            }
            .arrow {
                position: absolute;
                top: 90%;
                left: 50%;
                transform: translate(-50%, -50%);
            }
            .arrow span {
                display: block;
                width: 30px;
                height: 30px;
                border-bottom: 5px solid #dedede;
                border-right: 5px solid #dedede;
                transform: rotate(45deg);
                margin: -10px;
                animation: animate 2s infinite;
            }
            .arrow span:nth-child(2) {
                animation-delay: -0.2s;
            }
            .arrow span:nth-child(3) {
                animation-delay: -0.4s;
            }
            @keyframes animate {
                0% {
                    opacity: 0;
                    transform: rotate(45deg) translate(-20px, -20px);
                }
                50% {
                    opacity: 1;
                }
                100% {
                    opacity: 0;
                    transform: rotate(45deg) translate(20px, 20px);
                }
            }
            /* mission */
            .mission {
                background-color: #e4e4e4;
                width: 100%;
            }

            /* explore services */
            .explore {
                color: #5a5a5a;
                background-color: white;
                background-image: repeating-linear-gradient(45deg, white, transparent 500px);
                background-attachment: fixed;
            }
            @media screen and (min-width: 700px) {
                .hero {
                    height: 95vh;
                }
                .hero p {
                    font-size: 1.2em;
                    line-height: 1.5em;
                }
            }
        `;
    }

    render()
    {
        const maintenancemsg = "Maintenance announcement: On Tuesday 8th & Wednesday 9th February, the DESY cloud will undergo maintenance. We expect only short sporadic outages for all AAI connected services";
        return html`
            <header class="header-shadow">
                <helmholtz-cloud-header></helmholtz-cloud-header>
            </header>
            <main>
            ${this.maintenanceMessage ?
            html`
            <maintenance-banner message=${this.maintenanceMessage} page="services"></maintenance-banner>` : ''}
                <section class="hero">
                    <div class="inner-section">
                        <h1>Helmholtz Cloud</h1>
                        <div class="definition">
                            <div>In the Helmholtz Cloud, members of the Helmholtz Association of German research centers provide selected IT-Services for joint use.</div>
                            <div>The Service Portfolio covers the whole scientific process and offers Helmholtz employees and their project partners a federated community cloud with uniform access for them to conduct and support excellent science.</div>
                        </div>
                        <p>Note: Helmholtz Cloud is currently in pilot phase and will go in production during 2022.</p>
                        <div>
                            <div @click="${this._goToServices}">
                                <mwc-button raised>Show Services</mwc-button>
                            </div>
                        </div>
                    </div>
                    <div class="arrow">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </section>
                <section class="explore">
                    <feature-section></feature-section>
                </section>
                <section class="mission">
                    <mission-section></mission-section>
                </section>
            </main>
            <footer>
                <helmholtz-cloud-footer backgroundcolor></helmholtz-cloud-footer>
            </footer>
        `;
    }

    _goToServices()
    {
        window.location.href = `${cp_prefix}services`;
    }

    _scrollListener()
    {
        this.removeEventListener('wheel', this._scrollListener, true);
        this.shadowRoot.querySelector('.arrow').classList.add('hide');
    }

    _maintenanceDismissalListener() {
        this.shadowRoot.querySelector('maintenance-banner').classList.add('hide');
    }
}
customElements.define('landing-view', LandingView);
