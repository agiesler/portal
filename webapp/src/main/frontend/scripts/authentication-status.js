(()=>{
    function triggerEvent() {
        const aa = setInterval(() => {
            if (document.readyState === "complete") {
                clearInterval(aa);
                window.dispatchEvent(new CustomEvent('authentication-status-checker', { detail: {isLoading: false}}));
            }
        }, 100)
    }

    fetch(`${window.location.origin}${cp_prefix}auth_status`, {
    }).then(response => {
        if (response.status !== 200) {
            throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
        }
        return response.json();
    }).then(authenticated => {
        if (authenticated) {
            sessionStorage.setItem("auth_status", "authenticated");
        } else {
            sessionStorage.removeItem("auth_status");
        }
        triggerEvent();
    }).catch(e => {
        console.log(e);
        if (e.message.includes("401") && sessionStorage.getItem("auth_status") === "authenticated") {
            sessionStorage.clear();
        }
        triggerEvent();
    })
})()
