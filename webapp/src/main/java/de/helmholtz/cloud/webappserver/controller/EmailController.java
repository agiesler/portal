package de.helmholtz.cloud.webappserver.controller;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import de.helmholtz.cloud.webappserver.controller.Email.EmailData;

@RestController
public class EmailController {

    @Value("${webapp.email.fromAddress}")
    String fromAddress;

    @Value("${webapp.email.toAddress}")
    String toAddress;

    @Value("${webapp.email.subject}")
    String subject;

    @PostMapping(value = "/sendemail")
    public String sendEmail(@RequestBody EmailData data) {
        try {
            sendmail(data);
        } catch (AddressException e) {
            return "Cannot send message";
        } catch (MessagingException e) {
            return "Cannot send message";
        } catch (IOException e) {
            return "Cannot send message";
        }
        return "Email sent successfully";
    }

    private void sendmail(EmailData data) throws AddressException, MessagingException, IOException {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.desy.de");
        props.put("mail.smtp.port", "25");

        Session session = Session.getInstance(props);
        Message msg = new MimeMessage(session);
        if (data.getFromAddress() != null) {
            String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
            if (data.getFromAddress().matches(regex)) {
                msg.setFrom(new InternetAddress(data.getFromAddress(), false));
            }
        } else {
            msg.setFrom(new InternetAddress(fromAddress, false));
        }

        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toAddress));
        msg.setSubject(subject);
        msg.setContent(data.getComment(), "text/plain; charset=UTF-8");

        Transport.send(msg);
    }
}