package de.helmholtz.cloud.webappserver.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class HelmholtzMarketServerController {
    @Value("${cerebrum.endpoint}")
    String cerebrumUrl;

    private final OAuth2AuthorizedClientService authorizedClientService;

    public HelmholtzMarketServerController(
            OAuth2AuthorizedClientService authorizedClientService) {
        this.authorizedClientService = authorizedClientService;
    }

    @GetMapping(path = "/auth_status", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean getAuthStatus(OAuth2AuthenticationToken authentication,
            HttpServletResponse response) throws IOException {
        if (authentication == null) {
            return false;
        }
        return true;
    }

    @GetMapping(path = "/principal", produces = MediaType.APPLICATION_JSON_VALUE)
    public JsonNode getPrincipal(OAuth2AuthenticationToken authentication,
            HttpServletResponse response) throws IOException {
        if (authentication == null) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "OAuth2AuthenticationToken is null");
            return null;
        }

        final ObjectMapper mapper = new ObjectMapper();

        return mapper.valueToTree(authentication.getPrincipal());
    }

    @GetMapping("/csrf")
    public CsrfToken csrf(CsrfToken token) {
        return token;
    }

    @GetMapping(path = "/scripts/config.js", produces = "text/javascript")
    public String getWebAppConfig()
    {
        return "var CONFIG = {\"api.endpoint\": `${window.location.origin}${cp_prefix}api/v0`}";
    }
}
