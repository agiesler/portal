package de.helmholtz.cloud.webappserver.controller;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.Builder;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.reactive.function.client.WebClientResponseException;

import de.helmholtz.cloud.hca.message.ResourceAllocateV1;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
public class BackendServiceController {
    @Value("${cerebrum.endpoint}")
    String cerebrumUrl;

    private final OAuth2AuthorizedClientService authorizedClientService;

    public BackendServiceController(OAuth2AuthorizedClientService authorizedClientService) {
        this.authorizedClientService = authorizedClientService;
    }

    @GetMapping(value = "/api/v0/**", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JsonNode> getServices(OAuth2AuthenticationToken authentication, HttpServletRequest request) {
        String pattern = (String) request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE);
        String path = new AntPathMatcher().extractPathWithinPattern(pattern, request.getServletPath());
        String query = request.getQueryString();

        if (query != null) {
            path += "?" + query;
        }

        Builder client_builder = WebClient.builder().baseUrl(cerebrumUrl);

        if (authentication != null) {
            OAuth2AuthorizedClient authclient = authorizedClientService
                    .loadAuthorizedClient(authentication.getAuthorizedClientRegistrationId(), authentication.getName());
            String token = authclient.getAccessToken().getTokenValue();
            client_builder = client_builder.defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token);
        }
        WebClient client = client_builder
                .exchangeStrategies(ExchangeStrategies.builder()
                        .codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(16 * 1024 * 1024)).build())
                .build();
        try {
            JsonNode response = client.get().uri(path).accept(MediaType.APPLICATION_JSON).retrieve()
                    .bodyToMono(JsonNode.class).block();

            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (WebClientResponseException e) {
            return new ResponseEntity<JsonNode>(e.getStatusCode());
        }
    }

    private boolean checkPermission(OAuth2AuthenticationToken authentication, String serviceId, String type) {
        String path = "permissions";

        Builder client_builder = WebClient.builder().baseUrl(cerebrumUrl);

        OAuth2AuthorizedClient authclient = authorizedClientService
                .loadAuthorizedClient(authentication.getAuthorizedClientRegistrationId(), authentication.getName());
        String token = authclient.getAccessToken().getTokenValue();
        client_builder = client_builder.defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token);

        Object body = authentication.getPrincipal().getAttributes().get("eduperson_entitlement");

        WebClient client = client_builder
        .exchangeStrategies(ExchangeStrategies.builder()
                .codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(16 * 1024 * 1024)).build())
        .build();
        try {
            JsonNode response = client.post().uri(path).bodyValue(body).retrieve()
                    .bodyToMono(JsonNode.class).block();
            ArrayNode results = (ArrayNode)response.get("results");
            Iterator<JsonNode> iter = results.iterator();
            while (iter.hasNext()) {
                JsonNode node = iter.next();
                String permServiceId = node.get("_id").get("serviceId").asText();
                String permType = node.get("_id").get("type").asText();
                if (permServiceId.equals(serviceId) && permType.equals(type)) {
                    return true;
                }
            }

        } catch (WebClientResponseException e) {
            return false;
        }
        return false;
    }
    @PostMapping(value = "/api/v0/hca/request/allocate/{serviceId}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JsonNode> allocateResource(OAuth2AuthenticationToken authentication,
            HttpServletRequest request,
            @PathVariable(name = "serviceId") String serviceId,
            @RequestBody ResourceAllocateV1 body) {

        if (!this.checkPermission(authentication, serviceId, body.getType())) {
            return new ResponseEntity<JsonNode>(HttpStatus.UNAUTHORIZED);
        }
        String path = "hca/request/allocate/" + serviceId;

        Builder client_builder = WebClient.builder().baseUrl(cerebrumUrl);

        if (authentication != null) {
            OAuth2AuthorizedClient authclient = authorizedClientService
                    .loadAuthorizedClient(authentication.getAuthorizedClientRegistrationId(), authentication.getName());
            String token = authclient.getAccessToken().getTokenValue();
            client_builder = client_builder.defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token);
        }
        WebClient client = client_builder
                .exchangeStrategies(ExchangeStrategies.builder()
                        .codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(16 * 1024 * 1024)).build())
                .build();
        try {
            JsonNode response = client.post().uri(path).bodyValue(body).retrieve()
                    .bodyToMono(JsonNode.class).block();

            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        } catch (WebClientResponseException e) {
            return new ResponseEntity<JsonNode>(e.getStatusCode());
        }
    }

    @PostMapping(value = "/api/v0/**", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JsonNode> postData(OAuth2AuthenticationToken authentication,
            HttpServletRequest request,
            @RequestBody Object body) {
        String pattern = (String) request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE);
        String path = new AntPathMatcher().extractPathWithinPattern(pattern, request.getServletPath());
        String query = request.getQueryString();
        if (query != null) {
            path += "?" + query;
        }

        Builder client_builder = WebClient.builder().baseUrl(cerebrumUrl);

        if (authentication != null) {
            OAuth2AuthorizedClient authclient = authorizedClientService
                    .loadAuthorizedClient(authentication.getAuthorizedClientRegistrationId(), authentication.getName());
            String token = authclient.getAccessToken().getTokenValue();
            client_builder = client_builder.defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token);
        }
        WebClient client = client_builder
                .exchangeStrategies(ExchangeStrategies.builder()
                        .codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(16 * 1024 * 1024)).build())
                .build();
        try {
            JsonNode response = client.post().uri(path).bodyValue(body).retrieve()
                    .bodyToMono(JsonNode.class).block();

            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        } catch (WebClientResponseException e) {
            return new ResponseEntity<JsonNode>(e.getStatusCode());
        }
    }
}