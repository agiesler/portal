package de.helmholtz.cloud.webappserver.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class SinglePageApplicationErrorController implements ErrorController {
    @RequestMapping("/error")
    public Object error(HttpServletRequest request, HttpServletResponse response) {
        // FIXME - place the logging code here to log the cause of the error
        String uri = (String) request.getAttribute("javax.servlet.error.request_uri");
        if (uri.equals("/tokens")) {
            return new ResponseEntity<>(
                    request.getAttribute("javax.servlet.error.message"),
                    HttpStatus.valueOf((Integer) request.getAttribute("javax.servlet.error.status_code")));
        }
        if (request.getMethod().equalsIgnoreCase(HttpMethod.GET.name())) {
            response.setStatus(HttpStatus.OK.value());
            return "forward:/index.html";
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    public String getErrorPath() {
        return "/error";
    }
}
