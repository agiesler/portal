package de.helmholtz.cloud.webappserver.controller.Email;

import lombok.Getter;
import lombok.Setter;

@Getter()
@Setter()
public class EmailData {
    public EmailData() {
    }

    public EmailData(String fromAddress, String comment) {
        this.fromAddress = fromAddress;
        this.comment = comment;
    }

    private String fromAddress;
    private String comment;
}
