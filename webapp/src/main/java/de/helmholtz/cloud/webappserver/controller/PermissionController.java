package de.helmholtz.cloud.webappserver.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.databind.JsonNode;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.Builder;
import org.springframework.web.servlet.HandlerMapping;

import org.springframework.web.reactive.function.client.WebClientResponseException;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
public class PermissionController {
    @Value("${cerebrum.endpoint}")
    String cerebrumUrl;

    private final OAuth2AuthorizedClientService authorizedClientService;

    public PermissionController(OAuth2AuthorizedClientService authorizedClientService) {
        this.authorizedClientService = authorizedClientService;
    }

    @GetMapping(value = "/permissions", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JsonNode> getServices(OAuth2AuthenticationToken authentication, HttpServletRequest request) {
        String path = "permissions";

        Builder client_builder = WebClient.builder().baseUrl(cerebrumUrl);

        OAuth2AuthorizedClient authclient = authorizedClientService
                .loadAuthorizedClient(authentication.getAuthorizedClientRegistrationId(), authentication.getName());
        String token = authclient.getAccessToken().getTokenValue();
        client_builder = client_builder.defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token);

        Object body = authentication.getPrincipal().getAttributes().get("eduperson_entitlement");

        WebClient client = client_builder
        .exchangeStrategies(ExchangeStrategies.builder()
                .codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(16 * 1024 * 1024)).build())
        .build();
        try {
            JsonNode response = client.post().uri(path).bodyValue(body).retrieve()
                    .bodyToMono(JsonNode.class).block();

            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (WebClientResponseException e) {
            return new ResponseEntity<JsonNode>(e.getStatusCode());
        }
    }

}