<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.7.1</version>
		<relativePath/>
	</parent>
	<groupId>de.helmholtz.cloud</groupId>
	<artifactId>webapp</artifactId>
	<version>0.0.2-SNAPSHOT</version>

	<name>Helmholtz Cloud Portal Frontend</name>
	<url>https://cloud.helmholtz.de</url>
	<description>The frontend application for the HIFIS Cloud Portal</description>
	<organization>
		<name>Helmholtz Association</name>
		<url>https://www.helmholtz.de</url>
	</organization>

	<licenses>
		<license>
			<name>GNU Affero General Public License Version 3</name>
			<url>http://www.gnu.org/licenses/agpl-3.0.txt</url>
			<distribution>manual</distribution>
		</license>
	</licenses>

	<properties>
		<java.version>11</java.version>
		<dependency-check-maven.version>7.1.0</dependency-check-maven.version>
		<dependency-check-maven.cvss-threshold>8</dependency-check-maven.cvss-threshold>
		<version.helmholtz-marketplace-webapp>0.0.1</version.helmholtz-marketplace-webapp>
		<version.jacoco-maven-plugin>0.8.7</version.jacoco-maven-plugin>
		<version.maven-assembly-plugin>3.3.0</version.maven-assembly-plugin>
		<version.node>v14.17.1</version.node>
		<version.npm>7.19.1</version.npm>
		<version.frontend-maven>1.9.1</version.frontend-maven>
		<sonar.token>${env.SONAR_AUTH_TOKEN}</sonar.token>
	</properties>

	<dependencies>
		<dependency>
			<groupId>de.helmholtz.cloud</groupId>
			<artifactId>hca-messages</artifactId>
			<version>0.0.1-SNAPSHOT</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-webflux</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>
		<dependency>
			<groupId>io.micrometer</groupId>
			<artifactId>micrometer-registry-prometheus</artifactId>
			<scope>runtime</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-mail</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-oauth2-client</artifactId>
		</dependency>
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<optional>true</optional>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-tomcat</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-jetty</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
			<exclusions>
				<exclusion>
					<groupId>org.junit.vintage</groupId>
					<artifactId>junit-vintage-engine</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>com.github.eirslett</groupId>
				<artifactId>frontend-maven-plugin</artifactId>
				<version>${version.frontend-maven}</version>
				<configuration>
					<workingDirectory>src/main/frontend</workingDirectory>
				</configuration>
				<executions>
					<execution>
						<id>install node and npm</id>
						<goals>
							<goal>install-node-and-npm</goal>
						</goals>
						<configuration>
							<nodeVersion>${version.node}</nodeVersion>
							<npmVersion>${version.npm}</npmVersion>
						</configuration>
						<phase>generate-resources</phase>
					</execution>

					<execution>
						<id>npm install</id>
						<goals>
							<goal>npm</goal>
						</goals>
						<phase>generate-resources</phase>
						<configuration>
							<arguments>install</arguments>
						</configuration>
					</execution>

					<execution>
						<id>npm run build</id>
						<goals>
							<goal>npm</goal>
						</goals>
						<configuration>
							<arguments>run build</arguments>
						</configuration>
						<phase>process-resources</phase>
					</execution>

					<execution>
						<id>unit and integration tests</id>
						<goals>
							<goal>karma</goal>
						</goals>
						<phase>test</phase>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<version>${project.parent.version}</version>
			</plugin>
			<plugin>
				<groupId>org.owasp</groupId>
				<artifactId>dependency-check-maven</artifactId>
				<version>${dependency-check-maven.version}</version>
				<configuration>
					<failBuildOnCVSS>${dependency-check-maven.cvss-threshold}</failBuildOnCVSS>
					<suppressionFile>webapp/dependency-check-suppressions.xml</suppressionFile>
				</configuration>
				<executions>
					<execution>
						<goals>
							<goal>check</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-assembly-plugin</artifactId>
				<version>${version.maven-assembly-plugin}</version>
				<configuration>
					<descriptors>
						src/main/resources/distribution/distribution.xml
					</descriptors>
					<finalName>hm-server</finalName>
				</configuration>
				<executions>
					<execution>
						<phase>package</phase>
						<goals>
							<goal>single</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
</project>
