package de.helmholtz.cloud.cerebrum.service;

import java.util.NoSuchElementException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import de.helmholtz.cloud.cerebrum.entity.MaintenanceMessage;
import de.helmholtz.cloud.cerebrum.repository.MaintenanceMessageRepository;
import de.helmholtz.cloud.cerebrum.service.common.CerebrumServiceBase;
import de.helmholtz.cloud.cerebrum.service.common.ForeignKeyExecutorService;

@Service
public class MaintenanceMessageService
        extends CerebrumServiceBase<MaintenanceMessage, MaintenanceMessageRepository, ForeignKeyExecutorService> {
    private final MaintenanceMessageRepository repository;
    private final ForeignKeyExecutorService foreignKeyExecutorService;

    protected MaintenanceMessageService(MaintenanceMessageRepository repository,
            ForeignKeyExecutorService foreignKeyExecutorService) {
        super(MaintenanceMessage.class, MaintenanceMessageRepository.class, ForeignKeyExecutorService.class);
        this.repository = repository;
        this.foreignKeyExecutorService = foreignKeyExecutorService;
    }

    public ResponseEntity<MaintenanceMessage> getMessage() {

        try {
            MaintenanceMessage msg = repository.findById("1").get();
            return ResponseEntity.ok().body(msg);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
