package de.helmholtz.cloud.cerebrum.utils;

import java.security.SecureRandom;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Objects;
import java.util.UUID;

import de.helmholtz.cloud.cerebrum.entity.MarketService;
import de.helmholtz.cloud.cerebrum.entity.MarketUser;
import de.helmholtz.cloud.cerebrum.entity.Organization;
import de.helmholtz.cloud.cerebrum.entity.Person;
import de.helmholtz.cloud.cerebrum.entity.Permission;
import de.helmholtz.cloud.cerebrum.entity.HcaResource;
import de.helmholtz.cloud.cerebrum.entity.HcaRequest;
import de.helmholtz.cloud.cerebrum.entity.Image;
import de.helmholtz.cloud.cerebrum.entity.Software;
import de.helmholtz.cloud.cerebrum.errorhandling.exception.CerebrumInvalidUuidException;

public class CerebrumEntityUuidGenerator {
    public enum PrefixEnum {
        ORGANIZATION("org", "organization", Organization.class),
        HELMHOLTZMARKETUSER("usr", "marketuser", MarketUser.class),
        HCARESOURCE("res", "hcaresource", HcaResource.class),
        HCAREQUEST("req", "hcarequest", HcaRequest.class),
        PERSON("prn", "person", Person.class),
        PERMISSION("per", "permission", Permission.class),
        MARKETSERVICE("svc", "marketservice", MarketService.class),
        IMAGE("img", "image", Image.class),
        SOFTWARE("sfw", "software", Software.class);

        private final String prefix;
        private final String className;
        private final Class<?> clazz;

        PrefixEnum(String prefix, String className, Class<?> clazz) {
            this.prefix = prefix;
            this.className = className;
            this.clazz = clazz;
        }

        public static String getEntityPrefix(String className) {
            for (PrefixEnum p : PrefixEnum.values()) {
                if (Objects.equals(p.className.toLowerCase(),
                        className.toLowerCase())) {
                    return p.prefix;
                }
            }
            throw new IllegalArgumentException(
                    "Unknown entity with class name " + className);
        }

        public static void checkPrefixValidity(String prefix) {
            boolean valid = Arrays.stream(
                    PrefixEnum.values()).anyMatch(p -> p.prefix.equals(prefix));
            if (!valid)
                throw new IllegalArgumentException(
                        "Prefix: '" + prefix + "' is unknown to cerebrum.");
        }
    }

    public static synchronized String generate(String prefix) {
        PrefixEnum.checkPrefixValidity(prefix);
        return prefix + "-" + generateType1UUID();
    }

    public static Boolean isValid(String id) {
        try {
            PrefixEnum.checkPrefixValidity(id.split("-")[0]);
            UUID uuid = UUID.fromString(id.substring(id.indexOf('-') + 1));
            return uuid.version() > 0;
        } catch (IllegalArgumentException | NullPointerException ex) {
            return false;
        }
    }

    public static void checkUuidValidity(String uuid) {
        if (Boolean.FALSE.equals(isValid(uuid)))
            throw new CerebrumInvalidUuidException(uuid);
    }

    public static boolean matchUuidWithClass(String uuid, Class<?> clazz) {
        checkUuidValidity(uuid);
        return getClass(uuid).isAssignableFrom(clazz);
    }

    public static Class<?> getClass(String uuid) {
        String prefix = uuid.split("-")[0];
        for (PrefixEnum p : PrefixEnum.values()) {
            if (p.prefix.equals(prefix)) {
                return p.clazz;
            }
        }
        throw new IllegalArgumentException(
                "uuid: '" + prefix + "' is unknown to cerebrum.");
    }

    /**
     * Type 1 UUID Generation
     * source:
     * https://github.com/eugenp/tutorials/blob/master/core-java-modules/core-java/src/main/java/com/baeldung/uuid/UUIDGenerator.java
     */
    public static UUID generateType1UUID() {

        long most64SigBits = get64MostSignificantBitsForVersion1();
        long least64SigBits = get64LeastSignificantBitsForVersion1();

        return new UUID(most64SigBits, least64SigBits);
    }

    private static long get64LeastSignificantBitsForVersion1() {
        SecureRandom random = new SecureRandom();
        long random63BitLong = random.nextLong() & 0x3FFFFFFFFFFFFFFFL;
        long variant3BitFlag = 0x8000000000000000L;
        return random63BitLong + variant3BitFlag;
    }

    private static long get64MostSignificantBitsForVersion1() {
        LocalDateTime start = LocalDateTime.of(1582, 10, 15, 0, 0, 0);
        Duration duration = Duration.between(start, LocalDateTime.now());
        long seconds = duration.getSeconds();
        long nanos = duration.getNano();
        long timeForUuidIn100Nanos = seconds * 10000000 + nanos * 100;
        long least12SignificatBitOfTime = (timeForUuidIn100Nanos & 0x000000000000FFFFL) >> 4;
        long version = 1 << 12;
        return (timeForUuidIn100Nanos & 0xFFFFFFFFFFFF0000L) + version + least12SignificatBitOfTime;
    }
}
