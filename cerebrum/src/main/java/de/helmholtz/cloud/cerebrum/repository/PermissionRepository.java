package de.helmholtz.cloud.cerebrum.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;

import de.helmholtz.cloud.cerebrum.entity.Permission;
import de.helmholtz.cloud.cerebrum.repository.fragment.CerebrumRepository;

public interface PermissionRepository extends MongoRepository<Permission, String>, CerebrumRepository<Permission> {
    Page<Permission> findByServiceId(String serviceId, PageRequest page);

    Page<Permission> findByType(String type, PageRequest page);

    Page<Permission> findByServiceIdAndType(String serviceId, String type, PageRequest page);

    Page<Permission> findByEntitlement(String entitlement, PageRequest page);
    Page<Permission> findByEntitlementIn(List<String> entitlement, PageRequest page);
}
