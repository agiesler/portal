package de.helmholtz.cloud.cerebrum.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.lang.NonNull;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Document
public class MaintenanceMessage extends AuditMetadata
{
    @Setter(AccessLevel.NONE)
    @Id
    private int uuid = 1;

    @NonNull
    private String message;
}
