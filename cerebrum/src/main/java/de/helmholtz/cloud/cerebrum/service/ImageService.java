package de.helmholtz.cloud.cerebrum.service;

import lombok.SneakyThrows;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import de.helmholtz.cloud.cerebrum.entity.Image;
import de.helmholtz.cloud.cerebrum.repository.ImageRepository;
import de.helmholtz.cloud.cerebrum.service.common.CerebrumServiceBase;
import de.helmholtz.cloud.cerebrum.service.common.ForeignKeyExecutorService;

@Service
public class ImageService extends CerebrumServiceBase<Image, ImageRepository, ForeignKeyExecutorService> {
    private final ImageRepository repository;
    private final ForeignKeyExecutorService foreignKeyExecutorService;

    protected ImageService(ImageRepository repository, ForeignKeyExecutorService foreignKeyExecutorService) {
        super(Image.class, ImageRepository.class, ForeignKeyExecutorService.class);
        this.repository = repository;
        this.foreignKeyExecutorService = foreignKeyExecutorService;
    }

    public Page<Image> getImages(PageRequest page) {
        return getAllEntities(page, repository);
    }

    public Page<Image> getImages(String name, PageRequest page) {
        return repository.findByName(name, page);
    }

    public Image getImage(String uuid) {
        return getEntity(uuid, repository);
    }

    @SneakyThrows
    public ResponseEntity<Image> createImage(Image image, UriComponentsBuilder uriComponentsBuilder) {
        return createEntity(image, repository, foreignKeyExecutorService, uriComponentsBuilder);
    }

    @SneakyThrows
    public ResponseEntity<Image> updateImage(String uuid, Image entity,
            UriComponentsBuilder uriComponentsBuilder) {
        return updateEntity(uuid, entity, repository, foreignKeyExecutorService, uriComponentsBuilder);
    }

    public ResponseEntity<Image> deleteImage(String uuid) {
        return deleteEntity(uuid, repository, foreignKeyExecutorService);
    }
}
