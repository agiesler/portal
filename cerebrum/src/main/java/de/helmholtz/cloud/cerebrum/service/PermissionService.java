package de.helmholtz.cloud.cerebrum.service;

import java.util.List;

import org.bson.Document;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import de.helmholtz.cloud.cerebrum.entity.Permission;
import de.helmholtz.cloud.cerebrum.repository.PermissionRepository;
import de.helmholtz.cloud.cerebrum.service.common.CerebrumServiceBase;
import de.helmholtz.cloud.cerebrum.service.common.ForeignKeyExecutorService;
import lombok.SneakyThrows;

@Service
public class PermissionService
        extends CerebrumServiceBase<Permission, PermissionRepository, ForeignKeyExecutorService> {
    private final PermissionRepository repository;
    private final ForeignKeyExecutorService foreignKeyExecutorService;
    private final MongoTemplate template;

    protected PermissionService(PermissionRepository repository, ForeignKeyExecutorService foreignKeyExecutorService, MongoTemplate template) {
        super(Permission.class, PermissionRepository.class, ForeignKeyExecutorService.class);
        this.repository = repository;
        this.template = template;
        this.foreignKeyExecutorService = foreignKeyExecutorService;
    }

    public Page<Permission> getPermissions(PageRequest page) {
        return getAllEntities(page, repository);
    }

    public Page<Permission> findByServiceId(String serviceId, PageRequest page) {
        return repository.findByServiceId(serviceId, page);
    }

    public Page<Permission> findByType(String type, PageRequest page) {
        return repository.findByType(type, page);
    }

    public Page<Permission> findByServiceIdAndType(String serviceId, String type, PageRequest page) {
        return repository.findByServiceIdAndType(serviceId, type, page);
    }

    public Page<Permission> findByEntitlement(String entitlement, PageRequest page) {
        return repository.findByEntitlement(entitlement, page);
    }

    public Page<Permission> findByEntitlementIn(List<String> entitlements, PageRequest page) {
        return repository.findByEntitlementIn(entitlements, page);
    }

    public Document aggregateByEntitlement(List<String> entitlements) {
        AggregationOperation match = Aggregation.match(Criteria.where("entitlement").in(entitlements));
        AggregationOperation group = Aggregation.group("$serviceId", "$type");

        Aggregation aggregation = Aggregation.newAggregation(match, group);

        Document doc = template.aggregate(aggregation, template.getCollectionName(Permission.class), Permission.class).getRawResults();
        return doc;
    }

    public Permission getPermission(String uuid) {
        return getEntity(uuid, repository);
    }

    @SneakyThrows
    public ResponseEntity<Permission> createPermission(String serviceId , String type, String entitlement,
            UriComponentsBuilder uriComponentsBuilder) {
        Permission entity = new Permission(serviceId, type, entitlement);

        return createEntity(entity, repository, foreignKeyExecutorService, uriComponentsBuilder);
    }

    @SneakyThrows
    public ResponseEntity<Permission> updatePermission(String uuid, Permission entity,
            UriComponentsBuilder uriComponentsBuilder) {
        return updateEntity(uuid, entity, repository, foreignKeyExecutorService, uriComponentsBuilder);
    }

    public ResponseEntity<Permission> deletePermission(String uuid) {
        return deleteEntity(uuid, repository, foreignKeyExecutorService);
    }
}
