package de.helmholtz.cloud.cerebrum.controller;

import java.security.Principal;
import java.util.List;

import javax.validation.constraints.Min;

import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import de.helmholtz.cloud.cerebrum.service.PermissionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, path = "${spring.data.rest.base-path}/permissions")
@Tag(name = "Permissions", description = "API to permission for a user")
public class PermissionController {

        private final PermissionService PermissionService;

        public PermissionController(PermissionService PermissionService) {
                this.PermissionService = PermissionService;
        }

        /* list requests for user */
        @PreAuthorize("isAuthenticated()")
        @Operation(summary = "get all requests for a user")
        @PostMapping(path = "")
        public String getPermissionByEntitlement(
                        @Parameter(description = "specify the page number") @RequestParam(value = "page", defaultValue = "0") @Min(0) Integer page,
                        @Parameter(description = "limit the number of records returned in one page") @RequestParam(value = "size", defaultValue = "20") @Min(1) Integer size,
                        @Parameter(description = "sort the fetched data in either ascending (asc) "
                                        + "or descending (desc) according to one or more of the images "
                                        + "properties. Eg. to sort the list in ascending order base on the "
                                        + "name property; the value will be set to name.asc") @RequestParam(value = "sort", defaultValue = "createdDate.desc") List<String> sorts,
                                        @RequestBody List<String> entitlements,
                                        Principal user) {
                return PermissionService.aggregateByEntitlement(entitlements).toJson();
        }
}
