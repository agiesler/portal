package de.helmholtz.cloud.cerebrum.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import de.helmholtz.cloud.cerebrum.utils.CerebrumEntityUuidGenerator;
import de.helmholtz.cloud.hca.message.AllocateResourceSpecification;
import de.helmholtz.cloud.hca.message.TargetEntityV1;
import static de.helmholtz.cloud.cerebrum.utils.CerebrumEntityUuidGenerator.generate;

import javax.validation.constraints.NotNull;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Document
public class HcaResource extends AuditMetadata {
    @Schema(description = "Unique identifier of the HCA request.", example = "hca-01eac6d7-0d35-1812-a3ed-24aec4231940", required = true)
    @Setter(AccessLevel.NONE)
    @Id
    private String uuid = generate("res");

    @Version
    private Long version;

    @NonNull
    private TargetEntityV1 targetEntity;

    @NonNull
    private Object spec;

    @NonNull
    private String status;

    private String errorMessage;

    @NotNull
    private String requesterId;

    @NotNull
    private String serviceId;

    @NotNull
    private String resourceType;

    @Nullable
    private String resourceId;

    public void setUuid(@Nullable String uuid) {
        this.uuid = Boolean.TRUE.equals(CerebrumEntityUuidGenerator.isValid(uuid)) ? uuid : generate("hca");
    }

    public HcaResource(TargetEntityV1 targetEntity, Object spec, String serviceId, String requesterId, String resourceType) {
        this.requesterId = requesterId;
        this.targetEntity = targetEntity;
        this.spec = spec;
        this.status = "requested";
        this.serviceId = serviceId;
        this.resourceType = resourceType;
    }
}
