package de.helmholtz.cloud.cerebrum.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.Date;

import javax.validation.constraints.NotNull;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Document
public class Availability {
    @Setter(AccessLevel.NONE)
    @Id
    private String id;

    @NonNull
    private Integer status;

    @Nullable
    private String errorMsg;

    @NotNull
    private Date lastUpdated;
}
