package de.helmholtz.cloud.cerebrum.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

import de.helmholtz.cloud.cerebrum.entity.Image;
import de.helmholtz.cloud.cerebrum.repository.fragment.CerebrumRepository;

public interface ImageRepository extends MongoRepository<Image, String>, CerebrumRepository<Image> {
    Optional<Image> findByUuid(String uuid);

    Optional<Image> deleteByUuid(String uuid);

    Page<Image> findByName(String name, PageRequest page);
}
