package de.helmholtz.cloud.cerebrum.service.common;

import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.Optional;
import java.util.Set;

import de.helmholtz.cloud.cerebrum.annotation.ForeignKey;
import de.helmholtz.cloud.cerebrum.errorhandling.exception.CerebrumEntityNotFoundException;
import de.helmholtz.cloud.cerebrum.repository.MarketServiceRepository;
import de.helmholtz.cloud.cerebrum.repository.MarketUserRepository;
import de.helmholtz.cloud.cerebrum.repository.PersonRepository;
import de.helmholtz.cloud.cerebrum.repository.ImageRepository;
import de.helmholtz.cloud.cerebrum.utils.CerebrumEntityUuidGenerator;

@Service
public class ForeignKeyExecutorService {
    private final MarketServiceRepository marketServiceRepository;
    private final MarketUserRepository marketUserRepository;
    private final PersonRepository personRepository;
    private final ImageRepository imageRepository;

    public ForeignKeyExecutorService(MarketServiceRepository marketServiceRepository,
            MarketUserRepository marketUserRepository,
            PersonRepository personRepository,
            ImageRepository imageRepository) {
        this.marketServiceRepository = marketServiceRepository;
        this.marketUserRepository = marketUserRepository;
        this.personRepository = personRepository;
        this.imageRepository = imageRepository;
    }

    @SneakyThrows
    private <E, R> boolean entityExist(String uuid, R repository) {
        // noinspection unchecked
        return ((Optional<E>) repository.getClass()
                .getMethod("findByUuid", String.class).invoke(repository, uuid)).isPresent();
    }

    @SneakyThrows
    private <E> void linkTwoEntities(Object repository, Class<E> entityClass, String primaryUuid, String foreignUuid) {
        if (entityExist(primaryUuid, repository))
            repository.getClass().getMethod("addForeignKey", String.class, String.class)
                    .invoke(repository, primaryUuid, foreignUuid);
        else
            throw new CerebrumEntityNotFoundException(entityClass.getName(), primaryUuid);
    }

    @SneakyThrows
    private void unLinkTwoEntities(Object repository, String primaryUuid, String foreignUuid) {
        if (entityExist(primaryUuid, repository))
            repository.getClass().getMethod("removeForeignKey", String.class, String.class)
                    .invoke(repository, primaryUuid, foreignUuid);
    }

    private Object repository(String prefix) {
        switch (prefix) {
            case "usr":
                return marketUserRepository;
            case "prn":
                return personRepository;
            case "svc":
                return marketServiceRepository;
            case "img":
                return imageRepository;
            default:
                throw new IllegalArgumentException("The repository for the entity is " +
                        "not yet registered inside ForeignKeyExecutorService");
        }
    }

    private void start(String primaryUuid, String foreignUuid, boolean link) {
        if (link)
            linkTwoEntities(
                    repository(primaryUuid.split("-")[0]),
                    CerebrumEntityUuidGenerator.getClass(primaryUuid), primaryUuid, foreignUuid);
        else
            unLinkTwoEntities(repository(primaryUuid.split("-")[0]), primaryUuid, foreignUuid);
    }

    /**
     *
     * FIXME: This should be simplify
     */
    @SneakyThrows
    private void triggerCascading(String newEntityUuid, Field field, Object object, Object object2, boolean link) {
        String type = field.getGenericType().getTypeName().toLowerCase();
        switch (type) {
            case "java.lang.string":
                String primaryUuid = (String) field.get(object);
                if (object2 == null) {
                    if (primaryUuid != null)
                        start(primaryUuid, newEntityUuid, link);
                } else {
                    String sPrimaryUuid = (String) field.get(object2);
                    if (sPrimaryUuid == null || sPrimaryUuid.isEmpty()) {
                        start(newEntityUuid, primaryUuid, false);
                    } else {
                        if (!sPrimaryUuid.equals(primaryUuid)) {
                            start(newEntityUuid, sPrimaryUuid, true);
                            start(newEntityUuid, primaryUuid, false);
                        }
                    }
                }
                break;
            case "java.util.set<java.lang.string>":
                Set<String> primaryUuids = (Set<String>) field.get(object);
                if (object2 == null) {
                    for (String uuid : primaryUuids) {
                        if (uuid != null)
                            start(uuid, newEntityUuid, link);
                    }
                } else {
                    Set<String> sPrimaryUuids = (Set<String>) field.get(object2);
                    if (sPrimaryUuids.isEmpty() || (sPrimaryUuids.contains(null) && sPrimaryUuids.size() == 1)) {
                        for (String uuid : primaryUuids) {
                            start(newEntityUuid, uuid, false);
                        }
                    } else {
                        for (String uuid : sPrimaryUuids) {
                            if (!(uuid == null || uuid.isEmpty()) || !primaryUuids.contains(uuid))
                                start(newEntityUuid, uuid, true);
                        }
                        for (String uuid : primaryUuids) {
                            if (!(uuid == null || uuid.isEmpty()) || !sPrimaryUuids.contains(uuid))
                                start(newEntityUuid, uuid, false);
                        }
                    }
                }
                break;
            default:
                throw new IllegalArgumentException("Annotated Foreignkey field must " +
                        "be of either String or  Set<String> type, where the String has " +
                        "to be a valid cerebrum uuid");
        }
    }

    @SneakyThrows
    public void executeBeforeCreate(Object object) {
        Class<?> clazz = object.getClass();
        Field f = clazz.getDeclaredField("uuid");
        f.setAccessible(true);
        String newEntityUuid = (String) f.get(object);

        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(ForeignKey.class)) {
                triggerCascading(newEntityUuid, field, object, null, true);
            }
        }
    }

    @SneakyThrows
    public void executeBeforeUpdate(Object retrieved, Object submitted) {
        Class<?> clazz = submitted.getClass();
        Field f = clazz.getDeclaredField("uuid");
        f.setAccessible(true);
        String updatedEntityUuid = (String) f.get(submitted);

        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(ForeignKey.class)) {
                triggerCascading(updatedEntityUuid, field, retrieved, submitted, true);
            }
        }
    }

    @SneakyThrows
    public void executeAfterDelete(Object deletedEntity) {
        Class<?> clazz = deletedEntity.getClass();
        Field f = clazz.getDeclaredField("uuid");
        f.setAccessible(true);
        String deletedEntityUuid = (String) f.get(deletedEntity);

        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(ForeignKey.class)) {
                triggerCascading(deletedEntityUuid, field, deletedEntity, null, false);
            }
        }
    }
}
