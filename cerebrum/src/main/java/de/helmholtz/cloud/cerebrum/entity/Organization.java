package de.helmholtz.cloud.cerebrum.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.URL;

import de.helmholtz.cloud.cerebrum.annotation.ForeignKey;

import javax.validation.constraints.NotNull;
import java.util.Objects;

@Schema(name = "Organization", description = "POJO that represents a single organization entry.")
@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
public class Organization {
    @Schema(description = "Name of the organisation in full", example = "Deutsches Elektronen-Synchrotron", required = true)
    @NotNull
    private String name;

    @Schema(description = "Name of the organisation in German")
    private String nameDE;

    @Schema(description = "The shortened form of an organisation's name - this " +
            "can be an acronym or initial", example = "DESY")
    @Setter(AccessLevel.NONE)
    private String abbreviation;

    @Schema(description = "Valid web address link to the organisation logo " +
            "or base64 encoded string of the organisation logo", example = "https://www.desy.de/++resource++desy/images/desy_logo_3c_web.svg")
    @ForeignKey
    private String logoId;

    @Schema(description = "The organisation web address", example = "https://www.desy.de/", required = true)
    @URL(message = "Web address")
    @NotNull
    private String url;

    @Schema(description = "", example = "HELMHOLTZ_CENTRE")
    private Type type;

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation.toUpperCase();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Organization that = (Organization) o;
        return name.equals(that.name) &&
                Objects.equals(abbreviation, that.abbreviation) &&
                Objects.equals(logoId, that.logoId) &&
                url.equals(that.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, abbreviation, logoId, url);
    }
}

enum Type {
    HELMHOLTZ_CENTRE,
    DEPARTMENT,
    GROUP,
    UNIT,
    OTHERS
}
