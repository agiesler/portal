package de.helmholtz.cloud.cerebrum.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import de.helmholtz.cloud.hca.message.RequestV1;
import de.helmholtz.cloud.cerebrum.entity.HcaRequest;
import de.helmholtz.cloud.cerebrum.repository.HcaRequestRepository;
import de.helmholtz.cloud.cerebrum.service.common.CerebrumServiceBase;
import de.helmholtz.cloud.cerebrum.service.common.ForeignKeyExecutorService;
import lombok.SneakyThrows;

@Service
public class HcaRequestService
        extends CerebrumServiceBase<HcaRequest, HcaRequestRepository, ForeignKeyExecutorService> {
    private final HcaRequestRepository repository;
    private final ForeignKeyExecutorService foreignKeyExecutorService;

    protected HcaRequestService(HcaRequestRepository repository, ForeignKeyExecutorService foreignKeyExecutorService) {
        super(HcaRequest.class, HcaRequestRepository.class, ForeignKeyExecutorService.class);
        this.repository = repository;
        this.foreignKeyExecutorService = foreignKeyExecutorService;
    }

    public Page<HcaRequest> getHcaRequests(PageRequest page) {
        return getAllEntities(page, repository);
    }

    public Page<HcaRequest> getHcaRequestsByRequesterId(String requesterId, PageRequest page) {
        return repository.findByRequesterId(requesterId, page);
    }

    public Page<HcaRequest> getHcaRequestsByRequesterIdAndServiceName(String requesterId, String serviceId,
            PageRequest page) {
        return repository.findByRequesterIdAndServiceId(requesterId, serviceId, page);
    }

    public Page<HcaRequest> getHcaRequestsByServiceId(String serviceId, PageRequest page) {
        return repository.findByServiceId(serviceId, page);
    }

    public HcaRequest getHcaRequest(String uuid) {
        return getEntity(uuid, repository);
    }

    @SneakyThrows
    public ResponseEntity<HcaRequest> createHcaRequest(String serviceId, RequestV1 request, String requesterId,
            UriComponentsBuilder uriComponentsBuilder) {
        HcaRequest entity = new HcaRequest(request, serviceId, requesterId);

        return createEntity(entity, repository, foreignKeyExecutorService, uriComponentsBuilder);
    }

    @SneakyThrows
    public ResponseEntity<HcaRequest> updateHcaRequest(String uuid, HcaRequest entity,
            UriComponentsBuilder uriComponentsBuilder) {
        return updateEntity(uuid, entity, repository, foreignKeyExecutorService, uriComponentsBuilder);
    }

    public ResponseEntity<HcaRequest> deleteHcaRequest(String uuid) {
        return deleteEntity(uuid, repository, foreignKeyExecutorService);
    }
}
