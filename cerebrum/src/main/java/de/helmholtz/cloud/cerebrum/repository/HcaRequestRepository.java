package de.helmholtz.cloud.cerebrum.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

import de.helmholtz.cloud.cerebrum.entity.HcaRequest;
import de.helmholtz.cloud.cerebrum.repository.fragment.CerebrumRepository;

public interface HcaRequestRepository extends MongoRepository<HcaRequest, String>, CerebrumRepository<HcaRequest> {
    Optional<HcaRequest> findByUuid(String uuid);

    Optional<HcaRequest> deleteByUuid(String uuid);

    Page<HcaRequest> findByRequesterId(String requesterId, PageRequest page);

    Page<HcaRequest> findByRequesterIdAndServiceId(String requesterId, String serviceId, PageRequest page);

    Page<HcaRequest> findByServiceId(String serviceId, PageRequest page);
}
