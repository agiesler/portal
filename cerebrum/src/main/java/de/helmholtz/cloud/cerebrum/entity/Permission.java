package de.helmholtz.cloud.cerebrum.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import de.helmholtz.cloud.cerebrum.utils.CerebrumEntityUuidGenerator;
import static de.helmholtz.cloud.cerebrum.utils.CerebrumEntityUuidGenerator.generate;

import javax.validation.constraints.NotNull;

@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Document
public class Permission extends AuditMetadata {
    @Schema(description = "Unique identifier of the permission.", example = "per-01eac6d7-0d35-1812-a3ed-24aec4231940", required = true)
    @Setter(AccessLevel.NONE)
    @Id
    private String uuid = generate("per");

    @Version
    private Long version;
    @NonNull
    private String serviceId;

    @NotNull
    private String type;

    @NotNull
    private String entitlement;

    public void setUuid(@Nullable String uuid) {
        this.uuid = Boolean.TRUE.equals(CerebrumEntityUuidGenerator.isValid(uuid)) ? uuid : generate("hca");
    }

    public Permission(String serviceId, String type, String entitlement) {
        this.serviceId = serviceId;
        this.type = type;
        this.entitlement = entitlement;
    }
}
