package de.helmholtz.cloud.cerebrum.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Min;
import java.util.List;

import de.helmholtz.cloud.cerebrum.entity.Availability;
import de.helmholtz.cloud.cerebrum.errorhandling.CerebrumApiError;
import de.helmholtz.cloud.cerebrum.service.AvailabilityService;
import de.helmholtz.cloud.cerebrum.utils.CerebrumControllerUtilities;

@RestController
@Validated
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, path = "${spring.data.rest.base-path}/availabilities")
@Tag(name = "availabilities", description = "The Image API")
public class AvailabilityController {
        private final AvailabilityService availabilityService;

        public AvailabilityController(AvailabilityService availabilityService) {
                this.availabilityService = availabilityService;
        }

        /* get Images */
        @Operation(summary = "get array list of all images")
        @ApiResponses(value = {
                        @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(array = @ArraySchema(schema = @Schema(implementation = Availability.class)))),
                        @ApiResponse(responseCode = "400", description = "invalid request", content = @Content(array = @ArraySchema(schema = @Schema(implementation = CerebrumApiError.class)))) })
        @GetMapping(path = "")
        public Iterable<Availability> getAvailabilities(
                        @Parameter(description = "specify the page number") @RequestParam(value = "page", defaultValue = "0") @Min(0) Integer page,
                        @Parameter(description = "limit the number of records returned in one page") @RequestParam(value = "size", defaultValue = "20") @Min(1) Integer size,
                        @Parameter(description = "sort the fetched data in either ascending (asc) "
                                        + "or descending (desc) according to one or more of the images "
                                        + "properties. Eg. to sort the list in ascending order base on the "
                                        + "name property; the value will be set to name.asc") @RequestParam(value = "sort", defaultValue = "name.asc") List<String> sorts) {
                return availabilityService.getAvailabilities(
                                PageRequest.of(page, size, Sort.by(CerebrumControllerUtilities.getOrders(sorts))));
        }
}
