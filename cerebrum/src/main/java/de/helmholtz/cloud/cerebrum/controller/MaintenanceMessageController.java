package de.helmholtz.cloud.cerebrum.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import de.helmholtz.cloud.cerebrum.entity.MaintenanceMessage;
import de.helmholtz.cloud.cerebrum.errorhandling.CerebrumApiError;
import de.helmholtz.cloud.cerebrum.service.MaintenanceMessageService;

@RestController
@Validated
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, path = "${spring.data.rest.base-path}/maintenanceMessage")
@Tag(name = "images", description = "The Image API")
public class MaintenanceMessageController {
        private final MaintenanceMessageService maintenanceMessageService;

        public MaintenanceMessageController(MaintenanceMessageService maintenanceMessageService) {
                this.maintenanceMessageService = maintenanceMessageService;
        }

        /* get Images */
        @Operation(summary = "get array list of all images")
        @ApiResponses(value = {
                        @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(array = @ArraySchema(schema = @Schema(implementation = MaintenanceMessage.class)))),
                        @ApiResponse(responseCode = "400", description = "invalid request", content = @Content(array = @ArraySchema(schema = @Schema(implementation = CerebrumApiError.class))))
        })
        @GetMapping(path = "")
        public ResponseEntity<MaintenanceMessage> getMaintenanceMessage() {
                return maintenanceMessageService.getMessage();
        }

}
