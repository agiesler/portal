package de.helmholtz.cloud.cerebrum.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import de.helmholtz.cloud.hca.message.AllocateResourceSpecification;
import de.helmholtz.cloud.hca.message.TargetEntityV1;
import de.helmholtz.cloud.cerebrum.entity.HcaResource;
import de.helmholtz.cloud.cerebrum.repository.HcaResourceRepository;
import de.helmholtz.cloud.cerebrum.service.common.CerebrumServiceBase;
import de.helmholtz.cloud.cerebrum.service.common.ForeignKeyExecutorService;
import lombok.SneakyThrows;

@Service
public class HcaResourceService
        extends CerebrumServiceBase<HcaResource, HcaResourceRepository, ForeignKeyExecutorService> {
    private final HcaResourceRepository repository;
    private final ForeignKeyExecutorService foreignKeyExecutorService;

    protected HcaResourceService(HcaResourceRepository repository,
            ForeignKeyExecutorService foreignKeyExecutorService) {
        super(HcaResource.class, HcaResourceRepository.class, ForeignKeyExecutorService.class);
        this.repository = repository;
        this.foreignKeyExecutorService = foreignKeyExecutorService;
    }

    public Page<HcaResource> getHcaResources(PageRequest page) {
        return getAllEntities(page, repository);
    }

    public Page<HcaResource> getHcaResourcesByRequesterId(String requesterId, PageRequest page) {
        return repository.findByRequesterId(requesterId, page);
    }

    public Page<HcaResource> getHcaResourcesByRequesterIdAndServiceName(String requesterId, String serviceId,
            PageRequest page) {
        return repository.findByRequesterIdAndServiceId(requesterId, serviceId, page);
    }

    public Page<HcaResource> getHcaResourceByResourceId(String resourceId, PageRequest page) {
        return repository.findByResourceId(resourceId, page);
    }

    public HcaResource getHcaResource(String uuid) {
        return getEntity(uuid, repository);
    }

    @SneakyThrows
    public ResponseEntity<HcaResource> createHcaResource(String serviceId, TargetEntityV1 targetEntity, Object spec, String userId, String resourceType,
            UriComponentsBuilder uriComponentsBuilder) {
        HcaResource entity = new HcaResource(targetEntity, spec, serviceId, userId, resourceType);

        return createEntity(entity, repository, foreignKeyExecutorService, uriComponentsBuilder);
    }

    @SneakyThrows
    public ResponseEntity<HcaResource> updateHcaRequest(String uuid, HcaResource entity,
            UriComponentsBuilder uriComponentsBuilder) {
        return updateEntity(uuid, entity, repository, foreignKeyExecutorService, uriComponentsBuilder);
    }

    public ResponseEntity<HcaResource> deleteHcaRequest(String uuid) {
        return deleteEntity(uuid, repository, foreignKeyExecutorService);
    }
}
