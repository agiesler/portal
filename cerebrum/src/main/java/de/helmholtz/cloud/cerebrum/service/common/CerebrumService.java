package de.helmholtz.cloud.cerebrum.service.common;

import com.github.fge.jsonpatch.JsonPatch;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

public interface CerebrumService<T, R, F> {
    Page<T> getAllEntities(PageRequest request, R repository);

    T getEntity(String uuid, R repository);

    T getEntity(String key, String value, R repository);

    Page<T> getEntities(PageRequest page, String attribute, String value, R repository);

    Page<T> searchEntities(PageRequest page, String text, R repository);

    T createEntity(T entity, R repository, F foreignKeyExecutorService);

    ResponseEntity<T> createEntity(T entity, R repository, F foreignKeyExecutorService,
            UriComponentsBuilder uriComponentsBuilder);

    ResponseEntity<T> updateEntity(String uuid, T entity, R repository, F foreignKeyExecutorService,
            UriComponentsBuilder uriComponentsBuilder);

    ResponseEntity<T> partiallyUpdateEntity(String uuid, R repository, F foreignKeyExecutorService, JsonPatch patch);

    ResponseEntity<T> deleteEntity(String uuid, R repository, F foreignKeyExecutorService);
}
