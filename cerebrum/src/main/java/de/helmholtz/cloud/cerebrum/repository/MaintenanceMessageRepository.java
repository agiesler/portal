package de.helmholtz.cloud.cerebrum.repository;

import org.springframework.data.mongodb.repository.MongoRepository;


import de.helmholtz.cloud.cerebrum.entity.MaintenanceMessage;
import de.helmholtz.cloud.cerebrum.repository.fragment.CerebrumRepository;

public interface MaintenanceMessageRepository extends MongoRepository<MaintenanceMessage, String>, CerebrumRepository<MaintenanceMessage>
{
}
