package de.helmholtz.cloud.cerebrum.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import javax.validation.constraints.Min;

import java.util.List;

import de.helmholtz.cloud.cerebrum.service.HcaResourceService;
import de.helmholtz.cloud.cerebrum.entity.HcaResource;
import de.helmholtz.cloud.cerebrum.service.MarketUserService;
import de.helmholtz.cloud.cerebrum.service.PersonService;

@RestController
@Validated
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, path = "${spring.data.rest.base-path}/resource")
@Tag(name = "resource", description = "The resources API")
public class HcaResourceController {
        private final HcaResourceService hcaResourceService;

        public HcaResourceController(MarketUserService marketUserService,
                        PersonService personService,
                        HcaResourceService hcaResourceService) {
                this.hcaResourceService = hcaResourceService;
        }

        /* get HCA resource information */
        @PreAuthorize("isAuthenticated()")
        @Operation(summary = "get information for a resource")
        @GetMapping(path = "/{id}")
        public HcaResource getResourcesByUserId(
                        @Parameter(description = "specify the resource ID") @PathVariable(name = "id") String id,
                        @Parameter(description = "specify the page number") @RequestParam(value = "page", defaultValue = "0") @Min(0) Integer page,
                        @Parameter(description = "limit the number of records returned in one page") @RequestParam(value = "size", defaultValue = "20") @Min(1) Integer size,
                        @Parameter(description = "sort the fetched data in either ascending (asc) "
                                        + "or descending (desc) according to one or more of the images "
                                        + "properties. Eg. to sort the list in ascending order base on the "
                                        + "name property; the value will be set to name.asc") @RequestParam(value = "sort", defaultValue = "createdDate.desc") List<String> sorts) {
                return hcaResourceService.getHcaResource(id);
        }
}
