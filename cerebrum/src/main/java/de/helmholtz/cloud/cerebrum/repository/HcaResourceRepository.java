package de.helmholtz.cloud.cerebrum.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

import de.helmholtz.cloud.cerebrum.entity.HcaResource;
import de.helmholtz.cloud.cerebrum.repository.fragment.CerebrumRepository;

public interface HcaResourceRepository extends MongoRepository<HcaResource, String>, CerebrumRepository<HcaResource> {
    Optional<HcaResource> findByUuid(String uuid);

    Optional<HcaResource> deleteByUuid(String uuid);

    Page<HcaResource> findByRequesterId(String requesterId, PageRequest page);

    Page<HcaResource> findByRequesterIdAndServiceId(String requesterId, String serviceId, PageRequest page);

    Page<HcaResource> findByResourceId(String resourceId, PageRequest page);
}
