## Docker-based Development Environment

This folder keeps all configuration files needed for a Docker-based development environment. Getting the environment running is a simple as running:

```
> docker-compose up -d
```

This will start a development container and a MongoDB database. The development container will mount this repository so you can make your changes on your host system and use the container to build, test and run the Cloud Portal front and backend.

Following a couple of useful commands:

```
# Get into the container
> docker exec -it tools_portal-dev_1 /bin/bash

# import the data into the database
ubuntu@portal-dev:/portal$ import_data

# build cerebrum / webapp
ubuntu@portal-dev:/portal$ build_{cerebrum,webapp}

# run all tests (needs to have CLIENT_SECRET and AAI_REFRESH_TOKEN set)
ubuntu@portal-dev:/portal$ run_tests

# run cerebrum / webapp (webapp needs to have CLIENT_SECRET set)
ubuntu@portal-dev:/portal$ run_{cerebrum,webapp}
```
