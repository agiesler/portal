image: registry.hzdr.de/hifis/cloud/access-layer/portal/build:latest

build:
    only:
        - main
        - merge_requests
        - tags
    stage: build
    script: mvn compile

test:
    only:
        - main
        - merge_requests
        - tags
    stage: test
    services:
        - mongo:4.0.25
    script:
        - 'export AAI_TOKEN=$(curl -u "helmholtz-marketplace:${CLIENT_SECRET}" -X POST "https://login.helmholtz.de/oauth2/token" -H "Content-Type: application/x-www-form-urlencoded" -d "grant_type=refresh_token&refresh_token=${AAI_REFRESH_TOKEN}&client_id=helmholtz-marketplace&client_secret=${CLIENT_SECRET}" | sed "s/{.*\"access_token\":\"\([^\"]*\).*}/\1/g")'
        - mvn $MAVEN_CLI_OPTS -Dtoken=$AAI_TOKEN -Dspring.data.mongodb.host=mongo test

package:
    only:
        - main
        - merge_requests
        - tags
    stage: package
    script:
        - mvn $MAVEN_CLI_OPTS -DskipTests clean install
    artifacts:
        paths:
            - "webapp/target/*.tar"
            - "webapp/target/dependency-check-report.html"
            - "cerebrum/target/*.jar"
            - "cerebrum/target/dependency-check-report.html"
        expire_in: 3600 seconds
        when: always
    tags:
        - "docker"

docker_cerebrum:
    only:
        - main
        - tags
        - merge_requests
    stage: docker
    image: docker:20.10.10
    services:
        - name: docker:20.10.10-dind
          alias: docker
    before_script:
        - docker info
    script:
        - docker login -u $GITLAB_DOCKER_USERNAME -p $GITLAB_DOCKER_PASSWORD $DOCKER_REGISTRY
        - |-
            if [[ "$CI_COMMIT_BRANCH" == "main" ]]; then
              docker build -t $DOCKER_IMAGE_BASE/cerebrum:latest -f cerebrum/Dockerfile cerebrum/
              docker push $DOCKER_IMAGE_BASE/cerebrum:latest
            else
              tag=$(echo $CI_COMMIT_REF_NAME | sed 's/^[0-9]*-//' | tr [:upper:] [:lower:] | tr [:punct:] -)
              if [[ -n "$CI_COMMIT_TAG" ]]; then
                tag=$CI_COMMIT_TAG
              fi
              docker build -t $DOCKER_IMAGE_BASE/cerebrum:$tag -f cerebrum/Dockerfile cerebrum/
              docker push $DOCKER_IMAGE_BASE/cerebrum:$tag
            fi
    dependencies:
        - package
    tags:
        - "docker"

docker_webapp:
    only:
        - main
        - tags
        - merge_requests
    stage: docker
    image: docker:20.10.10
    services:
        - name: docker:20.10.10-dind
          alias: docker
    before_script:
        - docker info
    script:
        - docker login -u $GITLAB_DOCKER_USERNAME -p $GITLAB_DOCKER_PASSWORD $DOCKER_REGISTRY
        - |-
            if [[ "$CI_COMMIT_BRANCH" == "main" ]]; then
              docker build -t $DOCKER_IMAGE_BASE/webapp:latest -f webapp/Dockerfile webapp/
              docker push $DOCKER_IMAGE_BASE/webapp:latest
            else
              tag=$(echo $CI_COMMIT_REF_NAME | sed 's/^[0-9]*-//' | tr [:upper:] [:lower:] | tr [:punct:] -)
              if [[ -n "$CI_COMMIT_TAG" ]]; then
                tag=$CI_COMMIT_TAG
              fi
              docker build -t $DOCKER_IMAGE_BASE/webapp:$tag -f webapp/Dockerfile webapp/
              docker push $DOCKER_IMAGE_BASE/webapp:$tag
            fi
    dependencies:
        - package
    tags:
        - "docker"

docker_availability:
    only:
        - main
        - tags
        - merge_requests
    stage: docker
    image: docker:20.10.10
    services:
        - name: docker:20.10.10-dind
          alias: docker
    before_script:
        - docker info
    script:
        - docker login -u $GITLAB_DOCKER_USERNAME -p $GITLAB_DOCKER_PASSWORD $DOCKER_REGISTRY
        - |-
            if [[ "$CI_COMMIT_BRANCH" == "main" ]]; then
              docker build -t $DOCKER_IMAGE_BASE/availability-checker:latest -f availability-checker/Dockerfile availability-checker/
              docker push $DOCKER_IMAGE_BASE/availability-checker:latest
            else
              tag=$(echo $CI_COMMIT_REF_NAME | sed 's/^[0-9]*-//' | tr [:upper:] [:lower:] | tr [:punct:] -)
              if [[ -n "$CI_COMMIT_TAG" ]]; then
                tag=$CI_COMMIT_TAG
              fi
              docker build -t $DOCKER_IMAGE_BASE/availability-checker:$tag -f availability-checker/Dockerfile availability-checker/
              docker push $DOCKER_IMAGE_BASE/availability-checker:$tag
            fi
    dependencies:
        - package
    tags:
        - "docker"

docker_hca_service:
    only:
        - main
        - tags
        - merge_requests
    stage: docker
    image: docker:20.10.10
    services:
        - name: docker:20.10.10-dind
          alias: docker
    before_script:
        - docker info
    script:
        - docker login -u $GITLAB_DOCKER_USERNAME -p $GITLAB_DOCKER_PASSWORD $DOCKER_REGISTRY
        - |-
            if [[ "$CI_COMMIT_BRANCH" == "main" ]]; then
              docker build -t $DOCKER_IMAGE_BASE/hca-service:latest -f hca-service/Dockerfile hca-service/
              docker push $DOCKER_IMAGE_BASE/hca-service:latest
            else
              tag=$(echo $CI_COMMIT_REF_NAME | sed 's/^[0-9]*-//' | tr [:upper:] [:lower:] | tr [:punct:] -)
              if [[ -n "$CI_COMMIT_TAG" ]]; then
                tag=$CI_COMMIT_TAG
              fi
              docker build -t $DOCKER_IMAGE_BASE/hca-service:$tag -f hca-service/Dockerfile hca-service/
              docker push $DOCKER_IMAGE_BASE/hca-service:$tag
            fi
    dependencies:
        - package
    tags:
        - "docker"

trigger_deploy_mr:
    only:
        - merge_requests
    stage: deploy
    image: alpine:3.14.0
    before_script:
        - apk add curl jq
    script:
        - 'curl -X POST -F token=$GITLAB_DESY_TOKEN -F ref=main --form "variables[CI_COMMIT_REF_NAME]=$CI_COMMIT_REF_NAME" --form "variables[CI_MERGE_REQUEST_IID]=$CI_MERGE_REQUEST_IID" --form "variables[CP_BUILD_STAGE]=mr" $GITLAB_DESY_URL > /tmp/out.json'
        - pipelineurl=$(jq .web_url /tmp/out.json)
        - echo "Application will be automatically deployed." && echo "You can check the deployment status here $pipelineurl" && echo "After a couple of minutes you can check the application at http://`echo $CI_COMMIT_REF_NAME | sed 's/^[0-9]*-//' | tr [:upper:] [:lower:] | tr [:punct:] -`.cloud-portal-dev.131.169.234.242.nip.io"

trigger_deploy_master:
    only:
        - main
    stage: deploy
    image: alpine:3.14.0
    before_script:
        - apk add curl jq
    script:
        - 'curl -X POST -F token=$GITLAB_DESY_TOKEN -F ref=main --form "variables[CP_BUILD_STAGE]=int" $GITLAB_DESY_URL > /tmp/out.json'
        - pipelineurl=$(jq .web_url /tmp/out.json)
        - 'echo "Deployment status: $pipelineurl"'
